import React, {Component} from "react";
import {connect} from "react-redux";
import {Menu,Alert} from "antd";
import {Link} from "react-router-dom";
import {Layout,Select, Popover,Input,Avatar,message,Modal} from "antd";
import CustomScrollbars from "util/CustomScrollbars";
import SidebarLogo from "./SidebarLogo";
import axios from 'axios';
import apiCall from '../../apiutil/apicall';
import Auxiliary from "util/Auxiliary";
import UserProfile from "./UserProfile";
import AppsNavigation from "./AppsNavigation";
import {
  NAV_STYLE_NO_HEADER_EXPANDED_SIDEBAR,
  NAV_STYLE_NO_HEADER_MINI_SIDEBAR,
  THEME_TYPE_LITE
} from "../../constants/ThemeSetting";
import IntlMessages from "../../util/IntlMessages";
import { YahooFilled,BankOutlined,CloudSyncOutlined,AuditOutlined,FunnelPlotOutlined,SolutionOutlined,ToolOutlined,SettingOutlined} from "@ant-design/icons";
const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;
class SidebarContent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hideNav:false,
      dkey:'1',
      companyId:'',
      func:this.customfunction
     
    }
  }
  async componentDidMount() {
    window.addEventListener("resize", this.resize.bind(this));
    this.resize();
   // localStorage.setItem("dkey",this.state.dkey);
    var user=localStorage.getItem('user');
  //  localStorage.setItem('com',this.props.sprops.cstate.company);
  if(this.props.sprops.cstate.company&& this.state.hideNav==false)
  {
  localStorage.setItem('com',this.props.sprops.cstate.company);
  }
    var gdisplay='';
    console.log("group",user);
    var res= await apiCall.getApi(`role`)
        //console.log("datafrom Api",res);
        res.data.Items.forEach(function(obj) {
          
          console.log("datafrom2 Api",obj);
       if(user==obj.Name)
       {
gdisplay=obj.DisplayName;
       }
    
       
          
        
        });
     console.log("dname"+gdisplay);
    
       this.setState({Group:gdisplay})
       
       var x=gdisplay.split(' ');
       console.log("length",x);
       if(x.length>1)
       {
       this.setState({fn:x[0].charAt(0)});
       this.setState({ln:x[1].charAt(0)});
       }
       else{
        this.setState({fn:gdisplay.charAt(0)});

       }
     
 
  


 }
 resize() {
  let currentHideNav = (window.innerWidth <= 760);
  if (currentHideNav !== this.state.hideNav) {
      this.setState({hideNav: currentHideNav});
      console.log('1resize',this.state.hideNav);
  }
}
  customfunction()
  {
    alert("this is a test")
  }
  getNoHeaderClass = (navStyle) => {
    if (navStyle === NAV_STYLE_NO_HEADER_MINI_SIDEBAR || navStyle === NAV_STYLE_NO_HEADER_EXPANDED_SIDEBAR) {
      return "gx-no-header-notifications";
    }
    return "";
  };
  getNavStyleSubMenuClass = (navStyle) => {
    if (navStyle === NAV_STYLE_NO_HEADER_MINI_SIDEBAR) {
      return "gx-no-header-submenu-popup";
    }
    return "";
  };
access=event=>{
  event.preventDefault()
  let secondsToGo = 5;
  const modal = Modal.warning({
    title: 'Access Denied',
    content: `You are not authorized to access this page. Please contact your company admin for assistance.`,
  });
  const timer = setInterval(() => {
    secondsToGo -= 1;
    modal.update({
      content: `You are not authorized to access this page. Please contact your company admin for assistance.`,
    });
  }, 1000);
  setTimeout(() => {
    clearInterval(timer);
    modal.destroy();
  }, secondsToGo * 1000);
}
activate=(e)=>{
console.log("asas",e)
this.setState({dkey:e.key});
localStorage.setItem("dkey",e.key);
localStorage.setItem("skey",'sss12');
}
sactivate=(e)=>{
  console.log("bbb",e)
 // alert(e.keyPath[1])
  //this.setState({dkey:e.key});
  try{
  localStorage.setItem("skey",e.keyPath[1]);
  }
  catch(e)
  {}
  }
  render() {
    const {themeType, navStyle, pathname,sprops} = this.props;
    localStorage.setItem('com',this.props.sprops.cstate.company);
    console.log("phere",sprops.cstate);
    const selectedKeys = pathname.substr(1);
    const defaultOpenKeys = selectedKeys.split('/')[1];
    return (<Auxiliary>
      <div style={{   width: "100%",paddingLeft: "47%",
    
       height: "35px",backgroundColor: "#fff7e6"}}  type="warning" showIcon={false} ></div>
      <SidebarLogo/>
      <div className="gx-sidebar-content">
      
        <CustomScrollbars className="gx-layout-sider-scrollbar">
          <Menu
           defaultSelectedKeys={[`${localStorage.getItem("dkey")}`]}
        defaultOpenKeys={[`${localStorage.getItem("skey")}`]}
            theme={themeType === THEME_TYPE_LITE ? 'lite' : 'dark'}
            mode="inline">

            
              <Menu.Item key="1" onClick={this.activate}>
                <Link   to={{
  pathname: '/dashboard',
  state: {
    ssprops: sprops.cstate.company
  }
}} ><i className="icon icon-dasbhoard"/>
                       <IntlMessages id="sidebar.dashboard"/></Link>
              </Menu.Item>
              <SubMenu key="sub1" onClick={this.sactivate} className={this.getNavStyleSubMenuClass(navStyle)}
                         title={<span>   <i className="icon icon-widgets" />
                         <IntlMessages id="Onboarding"/></span>}>
                         <Menu.Item key="2" onClick={this.activate}>
              <Link to={{
  pathname: '/renewal',
  state: {
    ssprops: sprops.cstate.company,
    subtype:'Onboarding',
    nav:'Single Send',
    form: true
  }
}} ><i className="icon icon-wysiwyg"/>
                  <IntlMessages id="Single Send"/></Link>
              </Menu.Item>
             {this.state.hideNav==true?<p></p>: <Menu.Item key="3" onClick={this.activate}>
              <Link to={{
  pathname: '/renewal',
  state: {
    ssprops: sprops.cstate.company,
    subtype:'Onboarding',
    nav:'Bulk Send',
    form: false
  }
}} ><i className="icon icon-files"/>
                  <IntlMessages id="Bulk Send"/></Link>
              </Menu.Item>}
         
              {this.state.hideNav==true?<p></p>:    <Menu.Item key="4" onClick={this.activate}>
              <Link to={{
  pathname: '/CTA',
  state: {
    ssprops: sprops.cstate.company,
    subtype:'Onboarding',
    form: false
  }
}}><i className="icon icon-setting"/>
                  <IntlMessages id="Configure"/></Link>
              </Menu.Item>}
              </SubMenu>
         

            
              {this.state.hideNav==true?<p></p>: <Menu.Item key="5" onClick={this.activate}>
                <Link to={{
  pathname: '/claims',
  state: {
    action:"Smart Claims Program"
  }
}}><i className="icon icon-wysiwyg"/>
                       <IntlMessages id="Claims"/></Link>
              </Menu.Item>}
            
              <SubMenu key="sub2"  className={this.getNavStyleSubMenuClass(navStyle)}
                         title={<span> <CloudSyncOutlined style={{fontSize:"20px",    paddingTop: "10px"}} />
                         <IntlMessages id="Renewal"/></span>}    onClick={this.sactivate}>
                         <Menu.Item key="6" onClick={this.activate}>
              <Link to={{
  pathname: '/renewal',
  state: {
    ssprops: sprops.cstate.company,
    subtype:'Renewals',
    nav:'Single Send',
    form: true
  }
}}><i className="icon icon-wysiwyg"/>
                  <IntlMessages id="Single Send"/></Link>
              </Menu.Item>
              {this.state.hideNav==true?<p></p>:  <Menu.Item key="7" onClick={this.activate}>
              <Link to={{
  pathname: '/renewal',
  state: {
    ssprops: sprops.cstate.company,
    subtype:'Renewals',
    nav:'Bulk Send',
    form: false
  }
}}><i className="icon icon-files"/>
                  <IntlMessages id="Bulk Send"/></Link>
              </Menu.Item>}
          
              {this.state.hideNav==true?<p></p>:    <Menu.Item key="8" onClick={this.activate}>
              <Link to={{
  pathname: '/CTA',
  state: {
    ssprops: sprops.cstate.company,
    subtype:'Renewals',
    form: false
  }
}}><i className="icon icon-setting"/>
                  <IntlMessages id="Configure"/></Link>
              </Menu.Item>}
              </SubMenu>
           
            
            
            
                           <SubMenu key="sub3" className={this.getNavStyleSubMenuClass(navStyle)}
                         title={<span> <CloudSyncOutlined style={{fontSize:"20px",    paddingTop: "10px"}} onClick={this.sactivate}/>
                         <IntlMessages id="CAT Notifications"/></span>} onClick={this.sactivate}>
                         <Menu.Item key="9" onClick={this.activate}>
              <Link to={{
  pathname: '/renewal',
  state: {
    ssprops: sprops.cstate.company,
    subtype:'CAT',
    nav:'Single Send',
    form: true
  }
}}><i className="icon icon-wysiwyg"/>
                  <IntlMessages id="Single Send"/></Link>
              </Menu.Item>
              {this.state.hideNav==true?<p></p>:     <Menu.Item key="10" onClick={this.activate}>
              <Link to={{
  pathname: '/renewal',
  state: {
    ssprops: sprops.cstate.company,
    subtype:'CAT',
    nav:'Bulk Send',
    form: false
  }
}}><i className="icon icon-files"/>
                  <IntlMessages id="Bulk Send"/></Link>
              </Menu.Item>}
          
              {this.state.hideNav==true?<p></p>:  <Menu.Item key="11" onClick={this.activate}>
              <Link to={{
  pathname: '/CTA',
  state: {
    ssprops: sprops.cstate.company,
    subtype:'CAT',
    form: false
  }
}}><i className="icon icon-setting"/>
                  <IntlMessages id="Configure"/></Link>
              </Menu.Item>}
              </SubMenu>
           
            
              {this.state.hideNav==true?<p></p>:  <SubMenu key="sub4" className={this.getNavStyleSubMenuClass(navStyle)}
                         title={<span> <FunnelPlotOutlined  style={{fontSize:"20px",    paddingTop: "10px"}}/>
                         <IntlMessages id="Reports"/></span>} onClick={this.sactivate}>
                         <Menu.Item key="12" onClick={this.activate}>
                         <Link to={{
  pathname: '/report',
  state: {
   rtype:'Unsubscribed'
  }
}}><SolutionOutlined style={{fontSize:"16px",    paddingTop: "10px"}}/>
                                                          <IntlMessages
                                                            id="Unsubscribed"/></Link>
                              </Menu.Item>
                              
                           </SubMenu>}
                           {this.state.hideNav==true?<p></p>:  <SubMenu key="sub5" className={this.getNavStyleSubMenuClass(navStyle)}
                         title={<span> <SettingOutlined  style={{fontSize:"20px",    paddingTop: "10px"}}/>
                         <IntlMessages id="Settings"/></span>} onClick={this.sactivate}>
               
           
                <Menu.Item key="13" onClick={this.activate} style={{marginBottom: "-11px"}}>
                {localStorage.getItem('user')=='WARD-Super-Admin' ? <Link
                    to={{
                      pathname: "/create",
                      mprops:this.props
                    }}
                  >
                    <i className="icon icon-add-circle" />
                    <IntlMessages id="Create Company" />
                  </Link>:
                  <Link to="/"
                  onClick={ this.access } >
                  <i className="icon icon-add-circle" />
                  <IntlMessages id="Create Company" />
                </Link>}
                </Menu.Item>
                <Menu.Item key="14" onClick={this.activate} style={{marginBottom: "-11px"}}>
                {localStorage.getItem('user')=='WARD-Super-Admin' ||localStorage.getItem('user')=='WARD-Company-Admin' ?   <Link
                    to={{
                      pathname: "/manage"
                    }}
                  >
                    <i className="icon icon-ckeditor" />
                    <IntlMessages id="Manage Company" />
                  </Link>:
                   <Link
                   to="/"
                   onClick={ this.access }
                 >
                   <i className="icon icon-ckeditor" />
                   <IntlMessages id="Manage Company" />
                 </Link>}
                </Menu.Item>
          
                
          
                <Menu.Item key="15" onClick={this.activate} style={{marginBottom: "-11px"}}>
                {localStorage.getItem('user')=='WARD-Super-Admin' ||localStorage.getItem('user')=='WARD-Company-Admin'  ? <Link
                    to={{
                      pathname: "/User",
                      state: {
                        action:"Create User"
                      }
                    }}
                  >
                    <i className="icon icon-add-circle" />
                    <IntlMessages id="Create User" />
                  </Link>:
                  <Link to="/"
                  onClick={ this.access }
                   >
                  <i className="icon icon-add-circle" />
                  <IntlMessages id="Create User" />
                </Link>}
                </Menu.Item>
                <Menu.Item key="16" onClick={this.activate} style={{marginBottom: "-11px"}}>
                {localStorage.getItem('user')=='WARD-Super-Admin' ||localStorage.getItem('user')=='WARD-Company-Admin' ?  <Link
                    to={{
                      pathname: "/User",
                      state: {
                        action:"Manage User"
                      }
                    }}
                  >
                    <i className="icon icon-ckeditor" />
                    <IntlMessages id="Manage User" />
                  </Link>:
                   <Link
                   to="/"
                   onClick={ this.access }
                 >
                   <i className="icon icon-ckeditor" />
                   <IntlMessages id="Manage User" />
                 </Link>
                   }
                </Menu.Item>
                <Menu.Item key="17" onClick={this.activate} style={{marginBottom: "-11px"}}>
                {localStorage.getItem('user')=='WARD-Super-Admin' ?  <Link
                    to={{
                      pathname: "/manageheader",
                      state: {
                        action:"Email Template Setup"
                      }
                    }}
                  >
                    <i className="icon icon-ckeditor" /><span style={{    fontSize: "13px"}}>
                    <IntlMessages id="Email Template Setup"  /></span>
                  </Link>:
                   <Link
                   to="/"
                   onClick={ this.access }
                 >
                   <i className="icon icon-ckeditor" />
                   <span style={{    fontSize: "13px"}}>
                    <IntlMessages id="Email Template Setup"  /></span>
                 </Link>
                   }
                </Menu.Item>
              </SubMenu>}
              
                

              
            
                
              
             

            

           

          </Menu>
        </CustomScrollbars>
      </div>
    </Auxiliary>
    );
  }
}

SidebarContent.propTypes = {};
const mapStateToProps = ({settings}) => {
  const {navStyle, themeType, locale, pathname} = settings;
  return {navStyle, themeType, locale, pathname}
};
export default connect(mapStateToProps)(SidebarContent);

