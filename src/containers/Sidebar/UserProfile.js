import React, {Component} from "react";
import {connect} from "react-redux";
import {Avatar, Popover} from "antd";
import {userSignOut} from "appRedux/actions/Auth";
import {Redirect} from 'react-router-dom';
import Amplify, { Auth } from 'aws-amplify';
class UserProfile extends Component {
  constructor(props) {
		super(props);
    this.state = {
      collapsed: false,
      isAuthenticated: false,
      isAuthenticating: true,
      username:'',
      Group:'',
      Userinfo:'',
    }
  }

  async componentDidMount() {
   
     //   this.userHasAuthenticated(true);
      var user=localStorage.getItem('userEmail');
        this.setState({Userinfo:user})
        var uinfo=localStorage.getItem('user');
        //const user =  await Auth.currentAuthenticatedUser();
        //console.log(user.signInUserSession.accessToken.payload["cognito:groups"])
        this.setState({Group:uinfo})
      //  this.props.history.push('/home')
  if(!user)
  {
    this.setState({collapsed:true});
  }
   


  }
  getusername()
  {
    var username=this.state.Userinfo;
    console.log("inside uname:"+username);
    var uname=username.split("@");
    return uname[0];
  }
  handleLogout = async () => {
	//	await Auth.signOut();
    //this.setState({ isAuthenticated: false });
	//	this.userHasAuthenticated(false);
		console.log("PROPS FROM APP.JS "+JSON.stringify(this.props));
   localStorage.clear(); 
   this.setState({username:''})
   this.setState({Group:''})
   this.setState({collapsed:true}) 
	
  
  };
  render() {
  if(this.state.collapsed)
  {

    return  <Redirect  to="/signin" />
  }
 
else{
    //console.log("here",this.props);
    const userMenuOptions = (
      <ul className="gx-user-popover">
          <li>Role: {this.state.Group}</li>
        <li>My Account</li>
        <li>Connections</li>
        <li onClick={this.handleLogout}>Logout
        </li>
      </ul>
    );

    return (

      <div className="gx-flex-row gx-align-items-center gx-mb-4 gx-avatar-row">
        <Popover placement="bottomRight" content={userMenuOptions} trigger="click">
          <Avatar src='https://via.placeholder.com/150x150'
                  className="gx-size-40 gx-pointer gx-mr-3" alt=""/>
          <span className="gx-avatar-name"> {this.getusername()}<i
            className="icon icon-chevron-down gx-fs-xxs gx-ml-2"/></span>
        </Popover>
      </div>

    )

  }
}
}

export default connect(null, {userSignOut})(UserProfile);
