import React, { Component } from "react";

import {
  Layout,
  Select,
  Popover,
  Input,
  Avatar,
  Menu,
  Badge,
  Button,Alert,
  Tooltip
} from "antd";
import JsGravatar from 'js-gravatar';
import { Link, Redirect } from "react-router-dom";
import AvatarGroup from "@material-ui/lab/AvatarGroup";
import CustomScrollbars from "util/CustomScrollbars";
import languageData from "./languageData";
import apiCall from "../../apiutil/apicall";
import {
  switchLanguage,
  toggleCollapsedSideNav
} from "../../appRedux/actions/Setting";
import SearchBox from "components/SearchBox";
import UserInfo from "components/UserInfo";
import AppNotification from "components/AppNotification";
import MailNotification from "components/MailNotification";
import Auxiliary from "util/Auxiliary";
import axios from "axios";
import "./index.css";
import {
  NAV_STYLE_DRAWER,
  NAV_STYLE_FIXED,
  NAV_STYLE_MINI_SIDEBAR,
  TAB_SIZE
} from "../../constants/ThemeSetting";
import { connect } from "react-redux";
import { userFacebookSignIn } from "../../appRedux/actions/Auth";
import {
  MenuUnfoldOutlined,
  UserAddOutlined,
  SearchOutlined,
  MenuFoldOutlined,
  user,
  PlusCircleFilled,
  FileSearchOutlined,
  UserOutlined,DownOutlined,
  LogoutOutlined,
  PlusCircleOutlined,
  VideoCameraOutlined,WarningOutlined,
  ProfileOutlined,
  SettingOutlined,
  RedoOutlined,
  HourglassOutlined,
  UploadOutlined,
  HomeOutlined,
  FieldTimeOutlined,
  LineChartOutlined,
  FileSyncOutlined,
  LeftOutlined
} from "@ant-design/icons";
const { Header } = Layout;
const { SubMenu } = Menu;

class Topbar extends Component {
  constructor(props) {
    super(props);
    this.getAlert = this.getAlert.bind(this);
    this.state = {
      searchText: "",
      company: [],
      gcheck: true,
      fname: "",
      lname: "",
      users: [],
      susers: [],
      logoimg: "",
      scntrl: false,
      hideNav:false
    };
  }

  async componentDidMount() {

    window.addEventListener("resize", this.resize.bind(this));
    this.resize();
    this.props.setClick(this.getAlert);
    /*setTimeout(function() {
      console.log("PROPS FROM APP.JS " + JSON.stringify(this.props));
      localStorage.clear();

      try{
        window.location.href = '/signin'
      }
      catch(es)
      {
        console.log("es",es)
      }


  }, 20 * 60 * 1000);
*/
if(!localStorage.getItem('userEmail'))
  {

  //  alert(localStorage.getItem('userEmail'))
    window.location.href = '/signin'
  }
    try {
      var userid = localStorage.getItem("UserIdinfo");
      var fname = localStorage.getItem("UserInitialsinfo").split(" ");
      //console.log("qqq",localStorage.getItem('CompanyIdinfo'));
      //localStorage.setItem('CompanyIdinfo'

      if (localStorage.getItem("user") == "WARD-Company-Admin") {
        try {
          var res1 = await apiCall.getApi(
            `user/company/` + localStorage.getItem("CompanyIdinfo")
          );
          console.log("res1", res1);
          var x = [];
          res1.data.Items.forEach(function(obj) {
            x.push(obj);
          });
          this.setState({ susers: x });
        } catch (e) {
          console.log("weee", e);
        }
      }
      if (localStorage.getItem("user") != "WARD-Super-Admin") {
        this.setState({ gcheck: true });
        console.log("here" + this.state.gcheck);

        var x = "";
        var fname;
        var lname;
        var uid = "";

        var comid = localStorage.getItem("CompanyIdinfo");
        try{
        var res = await apiCall.getApi(`company/getlogo/${comid}`);
        this.setState({ logoimg: "data:image/*;base64," + res.data.base64 });
        }
        catch(e)
        {

        }
        var res = await apiCall.getApi(`user/${userid}`);

        /* res.data.Items.forEach(function(obj) {
          if(localStorage.getItem('userEmail')===obj.EmailAddress)
          {
          console.log("datafrom3 Api",obj);
          localStorage.setItem("userId",obj.UserId);
        x=obj.CompanyId
        fname=obj.FirstName;
        lname=obj.LastName;
        //localStorage.setItem("firstname",obj.FirstName);
        //localStorage.setItem("lastname",obj.LastName);
        //localStorage.setItem("uuid",obj.UserId);
          }

        });*/

        x = res.data.Item.CompanyId;

        var com = "";
        //https://yi70tn8ms6.execute-api.us-east-1.amazonaws.com/dev/api/v1/company/9a87dc93-e965-4878-94f0-8d966e1beea9
        console.log(x);
        var res2 = await apiCall.getApi(`company/${x}`);
        console.log("company Api", res2.data.Item);

        com = res2.data.Item.Name;

        var acom = [];
        acom.push(com);
        this.setState({ company: acom });

        this.setState({ gcheck: true });
        this.props.func(x);
        this.setState({ fname: fname[0] });
        this.setState({ lname: fname[1] });
        localStorage.setItem("fname", fname[0]);
        localStorage.setItem("lname", fname[1]);
      } else {
        var company = this.state.company;
        var res = await apiCall.getApi(`company`);
        console.log("company datafrom Api", res.data);
        res.data.Items.forEach(function(obj) {
          console.log("datafrom Api", obj.Name);

          var r = company.includes(obj.Name);
          if (!r) {
            company.push(obj);
          }
        });
        //company.push('All');
        // company.push('All');
        console.log("herecomp", company);
        this.setState({ company: company });
        this.setState({ gcheck: false });
        //var res= await axios.get(`https://yi70tn8ms6.execute-api.us-east-1.amazonaws.com/dev/api/v1/user/company/`+localStorage.getItem('com'))
        //console.log("udatafrom Api",res);
        /*    var res= await axios.get(`https://yi70tn8ms6.execute-api.us-east-1.amazonaws.com/dev/api/v1/user`)
        //console.log("datafrom Api",res);
        var fname;
        var lname;
        var x=[];
        res.data.Items.forEach(function(obj) {
          x.push(obj);
          if(localStorage.getItem('userEmail')===obj.EmailAddress)
          {
          console.log("fname Api",obj);
     //   x=obj.CompanyId
     localStorage.setItem("userId",obj.UserId);
        fname=obj.FirstName;
        lname=obj.LastName;
        localStorage.setItem("firstname",obj.FirstName);
        localStorage.setItem("lastname",obj.LastName);
        localStorage.setItem("uuid",obj.UserId);
          }

        });*/
        //this.setState({users:x});
        this.setState({ fname: fname[0] });
        this.setState({ lname: fname[1] });
      }

      console.log("comp", this.state.company);
    } catch (ex) {
      console.log("2weee", ex);
    }
  }

  refresh = () => {
    // var company=this.state.company;
  };

  sel = async event => {
    console.log("22event", event);
    console.log("search", this.state.users);
    var searcharray = this.search(this.state.users, event);
    console.log("search", searcharray);
    this.setState({ susers: searcharray });
    console.log("usearch", this.state.susers);

    console.log("propssss", this.props);

    try {
      var res1 = await apiCall.getApi(`user/company/` + event);
      console.log("res1", res1);
      var x = [];
      res1.data.Items.forEach(function(obj) {
        x.push(obj);
      });

      this.setState({ susers: x });
    } catch (e) {}
    try {
      var res = await apiCall.getApi(`/company/getlogo/${event}`);
      this.setState({ scntrl: true });
      this.setState({ logoimg: "data:image/*;base64," + res.data.base64 });
    } catch (err) {
      this.setState({ logoimg: "" });
      console.log("ewewe", err);
    }
    this.props.func(event);
  };

  handleLogout = async () => {
    //	await Auth.signOut();
    //this.setState({ isAuthenticated: false });
    //	this.userHasAuthenticated(false);
    console.log("PROPS FROM APP.JS " + JSON.stringify(this.props));
    localStorage.clear();
    //alert("logout")
    window.location.href = '/signin'
   
  };
  search(source, nam) {
    var results;
    //console.log("setcompcall",nam)
    //console.log("res"+JSON.stringify(source));
    //nam = nam;
    results = source.filter(function(entry) {
      return entry.CompanyId.indexOf(nam) !== -1;
    });
    console.log("res" + JSON.stringify(results));
    return results;
  }
  languageMenu = () => (
    <CustomScrollbars className="gx-popover-lang-scroll">
      <ul className="gx-sub-popover">
        {languageData.map(language => (
          <li
            className="gx-media gx-pointer"
            key={JSON.stringify(language)}
            onClick={e => this.props.switchLanguage(language)}
          >
            <i className={`flag flag-24 gx-mr-2 flag-${language.icon}`} />
            <span className="gx-language-text">{language.name}</span>
          </li>
        ))}
      </ul>
    </CustomScrollbars>
  );

  updateSearchChatUser = evt => {
    this.setState({
      searchText: evt.target.value
    });
  };

  usermenu = () => (
    <Menu>
      {this.state.susers.map((x, y) => (
        
        <Menu.Item style={{ marginRight: "0px" }}>
          <Avatar
            style={{ 
              
              backgroundImage:`url(${JsGravatar({ email:x.EmailAddress, size: 50, defaultImage: 'blank' })})`,
              backgroundColor: "rgb(24, 144, 255)",
          
           
           
            backgroundSize: "cover"
          }}
            icon={<UserOutlined style={{ marginRight: "0px",opacity: "0.4"}} />}
          />{" "}
          {x.FirstName[0].toUpperCase() + x.FirstName.slice(1)}{" "}
          {x.LastName[0].toUpperCase() + x.LastName.slice(1)}
        </Menu.Item>
      ))}
    </Menu>
  );

  resize() {
    let currentHideNav = (window.innerWidth <= 760);
    if (currentHideNav !== this.state.hideNav) {
        this.setState({hideNav: currentHideNav});
        console.log('1resize',this.state.hideNav);
    }
}

  getAlert = async () => {
    try {
      this.setState({ company: [] });
      var userid = localStorage.getItem("UserIdinfo");
      var fname = localStorage.getItem("UserInitialsinfo").split(" ");
      //console.log("qqq",localStorage.getItem('CompanyIdinfo'));
      //localStorage.setItem('CompanyIdinfo'

      if (localStorage.getItem("user") == "WARD-Company-Admin") {
        var res1 = await apiCall.getApi(
          `user/company/` + localStorage.getItem("CompanyIdinfo")
        );
        console.log("res1", res1);
        var x = [];
        res1.data.Items.forEach(function(obj) {
          x.push(obj);
        });
        this.setState({ susers: x });
      }
      if (localStorage.getItem("user") != "WARD-Super-Admin") {
        this.setState({ gcheck: true });
        console.log("here" + this.state.gcheck);

        var x = "";
        var fname;
        var lname;
        var uid = "";

        var comid = localStorage.getItem("CompanyIdinfo");
        var res = await apiCall.getApi(`company/getlogo/${comid}`);
        this.setState({ logoimg: "data:image/*;base64," + res.data.base64 });
        var res = await apiCall.getApi(`user/${userid}`);

        /* res.data.Items.forEach(function(obj) {
            if(localStorage.getItem('userEmail')===obj.EmailAddress)
            {
            console.log("datafrom3 Api",obj);
            localStorage.setItem("userId",obj.UserId);
          x=obj.CompanyId
          fname=obj.FirstName;
          lname=obj.LastName;
          //localStorage.setItem("firstname",obj.FirstName);
          //localStorage.setItem("lastname",obj.LastName);
          //localStorage.setItem("uuid",obj.UserId);
            }

          });*/

        x = res.data.Item.CompanyId;

        var com = "";
        //https://yi70tn8ms6.execute-api.us-east-1.amazonaws.com/dev/api/v1/company/9a87dc93-e965-4878-94f0-8d966e1beea9
        console.log(x);
        var res2 = await apiCall.getApi(`company/${x}`);
        console.log("company Api", res2.data.Item);

        com = res2.data.Item.Name;

        var acom = [];
        acom.push(com);
        this.setState({ company: acom });

        this.setState({ gcheck: true });
        this.props.func(x);
        this.setState({ fname: fname[0] });
        this.setState({ lname: fname[1] });
        localStorage.setItem("fname", fname[0]);
        localStorage.setItem("lname", fname[1]);
      } else {
        var company = [];
        var res = await apiCall.getApi(`company`);
        console.log("company datafrom Api", res.data);
        res.data.Items.forEach(function(obj) {
          console.log("datafrom Api", obj.Name);

          var r = company.includes(obj.Name);
          if (!r) {
            company.push(obj);
          }
        });
        //company.push('All');
        // company.push('All');
        console.log("herecomp", company);
        this.setState({ company: company });
        this.setState({ gcheck: false });
        //var res= await axios.get(`https://yi70tn8ms6.execute-api.us-east-1.amazonaws.com/dev/api/v1/user/company/`+localStorage.getItem('com'))
        //console.log("udatafrom Api",res);
        /*    var res= await axios.get(`https://yi70tn8ms6.execute-api.us-east-1.amazonaws.com/dev/api/v1/user`)
          //console.log("datafrom Api",res);
          var fname;
          var lname;
          var x=[];
          res.data.Items.forEach(function(obj) {
            x.push(obj);
            if(localStorage.getItem('userEmail')===obj.EmailAddress)
            {
            console.log("fname Api",obj);
       //   x=obj.CompanyId
       localStorage.setItem("userId",obj.UserId);
          fname=obj.FirstName;
          lname=obj.LastName;
          localStorage.setItem("firstname",obj.FirstName);
          localStorage.setItem("lastname",obj.LastName);
          localStorage.setItem("uuid",obj.UserId);
            }

          });*/
        //this.setState({users:x});
        this.setState({ fname: fname[0] });
        this.setState({ lname: fname[1] });
      }

      console.log("comp", this.state.company);
    } catch (ex) {}
  };
  setscnrtl = () => {
    this.setState({ scntrl: false });
  };

  render() {
    const { locale, width, navCollapsed, navStyle } = this.props;
    return (
      <Auxiliary>
              <Tooltip title={<div style={{width:"546px"}}>You are currently in the DEV environment </div>}>
              <div style={{   width: "100%",paddingLeft: "47%",    paddingTop: "8px",
    
    height: "35px",backgroundColor: "#fff7e6"}}><span style={{marginTop: "-18px"}}><span style={{color:'orange'}}> <WarningOutlined/></span> DEV Site </span></div>
 <br/>
              </Tooltip>
             
        <Header style={{height: "72px",    marginTop: "-18px"}}>
          {navStyle === NAV_STYLE_DRAWER ||
          ((navStyle === NAV_STYLE_FIXED ||
            navStyle === NAV_STYLE_MINI_SIDEBAR) &&
            width < TAB_SIZE) ? (
            <div className="gx-linebar gx-mr-3">
              <i
                className="gx-icon-btn icon icon-menu"
                onClick={() => {
                  this.props.toggleCollapsedSideNav(!navCollapsed);
                }}
              />
            </div>
          ) : null}
          
         {this.state.hideNav==true?<p></p>: <Link to="/" className="gx-d-block gx-d-lg-none gx-pointer">
            <img
              alt=""
              style={{
                height: "40px",
                width: "auto"
              }}
              src={require("assets/images/w-logo.png")}
            />
          </Link>}

          {this.state.logoimg ? (
            this.state.hideNav==true?<img
              src={this.state.logoimg}
              style={{
                height: "26px",
                width: "auto",
                marginLeft: "-18px",
                marginRight: "10px"
              }}
            />:<img
              src={this.state.logoimg}
              style={{
                height: "40px",
                width: "auto",
                marginLeft: "-29px",
                marginRight: "7px"
              }}
            />
          ) : (
            localStorage.getItem("user") != "WARD-Super-Admin"?<h4> {this.state.company[0]}</h4> :<p></p>
          )}
          {this.state.gcheck ? (
            <p></p>
          ) : (
            <div>
           
              {this.state.scntrl ? (
                <Tooltip title="Switch Company">
                  <Button
                    type="default"
                   
                    style={{ marginTop: "14px", marginLeft:"5px"}}
                    onClick={this.setscnrtl}
                  >
                    <DownOutlined/> 
                  </Button>
                </Tooltip>
              ) : (
                this.state.hideNav==true? <Select
                showSearch
                style={{ marginLeft:"5px", width: "106px" }}
                placeholder="Select Company"
                optionFilterProp="children"
                onSelect={this.sel}
              >
                <option value="All">ALL</option>
                {this.state.company.map((x, y) => (
                  <option key={x.CompanyId}>{x.Name}</option>
                ))}
              </Select>:<Select
                  showSearch
                  style={{ marginLeft:"5px", width: "150px" }}
                  placeholder="Select Company"
                  optionFilterProp="children"
                  onSelect={this.sel}
                >
                  <option value="All">ALL</option>
                  {this.state.company.map((x, y) => (
                    <option key={x.CompanyId}>{x.Name}</option>
                  ))}
                </Select>
              )}{" "}
            </div>
          )}

          <ul className="gx-header-notifications gx-ml-auto">
             <li className="gx-notify gx-notify-search gx-d-inline-block gx-d-lg-none">
             {this.state.gcheck ? (
               this.state.hideNav?<p></p>:<Input type="text" value={this.state.company[0]} />
              ) : (
             <p></p>
              )}
            

            </li>
        {/* {this.state.logoimg==''? <li style={{marginRight: "290px"}}>
              <Tooltip title={<div style={{width:"500px"}}>You are currently in the test environment which restricts emails to @wardlawdigital.com, @wardlawclaims.com, and @selectmethods.com</div>}><span><Alert style={{    width: "136px",
    paddingLeft: "46px"}} message="Test Site" type="warning" showIcon /></span></Tooltip>
           </li>:<li style={{marginRight: "250px"}}>
           <Tooltip title={<div style={{width:"500px"}}>You are currently in the test environment which restricts emails to @wardlawdigital.com, @wardlawclaims.com, and @selectmethods.com</div>}><span><Alert style={{    width: "136px",
              paddingLeft: "46px"}} message="Test Site" type="warning" showIcon /></span></Tooltip> </li>}*/}
            <li className="gx-language">
          
              <div
                className="gx-flex-row gx-align-items-center gx-mb-4 gx-avatar-row"
                style={{ marginTop: "11px" }}
              >
                
                <span className="gx-avatar-name"></span>

                {this.state.hideNav==true?<p></p>: <Popover
                  overlayClassName="gx-popover-horizantal"
                  placement="bottomRight"
                  content={this.usermenu()}
                  trigger="click"
                >
                  <span className="gx-pointer gx-flex-row gx-align-items-center">
                    {this.state.susers.length > 0 ? (
                      <AvatarGroup
                        max={3}
                        style={{ fontSize: "0px", color: "transparent" }}
                      >
                        {this.state.susers.map((x, y) => (
                          <Avatar
                            className="gx-size-40 gx-pointer "
                            style={{
                              backgroundImage:`url(${JsGravatar({ email:x.EmailAddress, size: 50, defaultImage: 'blank' })})`,
                              backgroundColor: "#1890ff",
                              backgroundSize: "cover",
                              boxShadow: "3px 6px 7px -1px #888888"
                            }}
                          >
                            {x.FirstName.charAt(0).toUpperCase()}
                            {x.LastName.charAt(0).toUpperCase()}
                          </Avatar>
                        ))}
                      </AvatarGroup>
                    ) : (
                      <p></p>
                    )}
                  </span>
                </Popover>}
              </div>
            </li> 

            {width >= TAB_SIZE ? (
              <li className="gx-user-nav" style={{ marginBottom: "-12px" }}>
               {this.state.hideNav==true? <Tooltip title="Logout"><div style={{    marginTop: "0px"}}><Button  onClick={() => this.handleLogout()}><img src="https://i.vippng.com/png/small/0-3017_abmeldung-button-logfile-area-text-png-image-with.png" width="15px"/></Button></div></Tooltip>: <UserInfo uprops={this.state} />}
              </li>
            ) : (
              <Auxiliary>
                <li className="gx-user-nav" style={{ marginBottom: "-12px" }}>
                {this.state.hideNav==true?<Tooltip title="Logout"><div style={{    marginTop: "0px"}}> <Button  onClick={() => this.handleLogout()}><img src="https://i.vippng.com/png/small/0-3017_abmeldung-button-logfile-area-text-png-image-with.png" width="15px"/></Button></div></Tooltip>: <UserInfo uprops={this.state} />}
                </li>
              </Auxiliary>
            )}
          </ul>
        </Header>
        </Auxiliary>
    );
  }
}

const mapStateToProps = ({ settings }) => {
  const { locale, navStyle, navCollapsed, width } = settings;
  return { locale, navStyle, navCollapsed, width };
};

export default connect(mapStateToProps, {
  toggleCollapsedSideNav,
  switchLanguage
})(Topbar);
