import React from "react";
import {Button, Checkbox, Form, Icon, Input, message,Tooltip} from "antd";
import {connect} from "react-redux";
import {Link} from "react-router-dom";
import "./signin.css";
import Amplify, { Auth } from 'aws-amplify';
import { Layout,Card,notification } from 'antd';
import validator from '../validators/validator';
import { Row, Col } from 'antd';
import {InfoCircleOutlined} from '@ant-design/icons';
import {
  hideMessage,
  showAuthLoader,
  userFacebookSignIn,
  userGithubSignIn,
  userGoogleSignIn,
  userSignIn,
  userTwitterSignIn
} from "appRedux/actions/Auth";
import IntlMessages from "util/IntlMessages";
import CircularProgress from "components/CircularProgress/index";

const FormItem = Form.Item;

class SignIn extends React.Component {
  constructor(props) {
		super(props);
    this.state = {
        collapsed:false,
        toDashboard:false,
        username:'',
        Group:'',
        Userinfo:'',
        email: '',
        password: '',
        passwordChallenge:false,
        mfachallenge:false,
        newPassword:'',
        coguser:{},
        verify:'',
        opassword:'',
        uemail:'',
        nnpassword:'',
        load:false,
        gload:false,
        vreset:false,
        Nerror:'',
        npass:'',
        eflag:false,
        perror:'',
        eflag2:false
    };
  }
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.showAuthLoader();
        this.props.userSignIn(values);
      }
    });
  };
  formhandleChange=event=>{
    
    this.setState({
			[event.target.id]: event.target.value.trim()
    });
  
  
 
  }
  formsubmit=async event => {
    //event.preventDefault();
    localStorage.setItem("dkey",'1');
  if(this.state.email&&this.state.password)
  {
console.log("state",this.state);
    this.setState({ isLoading: true });
    this.setState({load:true});
    this.setState({
      gload: true
      // confirmLoading: false,
     });
//if(this.state.passwordChallenge==false)
		try {
    var x=	await Auth.signIn(this.state.email, this.state.password);
    console.log("res",x)
  this.setState({coguser:x});
  //alert(x.challengeName)
//localStorage.setItem("usession",JSON.stringify(x));
    if (x.challengeName === 'NEW_PASSWORD_REQUIRED'){
      this.setState({passwordChallenge: true,isLoading: false});
      this.setState({
        gload: false
        // confirmLoading: false,
       });
console.log("new password required"+JSON.stringify(x));


    }

    else if(x.challengeName==='SMS_MFA')
    { setTimeout(() => {
      this.setState({
        load: false
       // confirmLoading: false,
      });
      this.setState({
       gload: false
       // confirmLoading: false,
      });
      this.setState({mfachallenge:true})
    }, 4000);

      //var data=await Auth.currentUserInfo()
      console.log("this is a test",this.state.coguser);
     
    }
   else{
      try{
        var data=await Auth.currentUserInfo()
          console.log("this is a test",data);
          console.log("ID-token",Auth.get)
          var x=await Auth.currentSession();
          console.log("session",x)
          this.setState({Userinfo:data.attributes.email})
          const user =  await Auth.currentAuthenticatedUser();
          console.log("user",user)
          this.setState({Group:user.signInUserSession.accessToken.payload["cognito:groups"][0]})
          localStorage.setItem("userEmail",data.attributes.email)
   var x=localStorage.getItem("userEmail")
   localStorage.setItem("user",user.signInUserSession.accessToken.payload["cognito:groups"][0])
   try {
    Auth.currentAuthenticatedUser({bypassCache: true})
   const currentUserInfo = await Auth.currentUserInfo()
   const UserIdinfo = currentUserInfo.attributes['custom:UserId']
   const UserInitialsinfo = currentUserInfo.attributes['custom:UserInitials']
   const CompanyIdinfo = currentUserInfo.attributes['custom:CompanyId']
localStorage.setItem('UserIdinfo',UserIdinfo)
localStorage.setItem('UserInitialsinfo',UserInitialsinfo)
localStorage.setItem('CompanyIdinfo',CompanyIdinfo)

console.log('currentUserInfo '+UserIdinfo+'  '+UserInitialsinfo+'  '+CompanyIdinfo);




   }
   catch (err) {
    console.log('error fetching user info: ', err);
  }
  this.props.history.push({
     pathname: '/dashboard',
     userData: this.state.Userinfo,
     groupData: this.state.Group
   });
          }
          catch(e) {
            alert(e);
            this.setState({ isLoading: false });
            this.setState({gload:false})
          }
    }
		
			
			
			
		} catch (e) {
      //alert(e.message);
      notification.open({
        message: 'Alert',
        description:
        e.message
      });
      if(e.message=='Password reset required for the user')
      {
       this.setState({vreset:true})
       this.setState({gload:false})
      }
      this.setState({ isLoading: false });
      this.setState({gload:false})
    }
    setTimeout(() => {
      this.setState({
        load: false
       // confirmLoading: false,
      });
     
    }, 2000);
  }
  else{
    if(!this.state.email)
    {
   // message.error("Email fields are missing");
    this.setState({Nerror:"Email field is missing"})
    }
    else if(!this.state.password)
{
 // message.error("Password fields are missing");
  this.setState({perror:"Password field is missing"})
    }

  }


	};


  componentDidUpdate() {
  // console.log("email",this.state.email)
    if (this.props.showMessage) {
      setTimeout(() => {
        this.props.hideMessage();
      }, 100);
    }
    if (this.props.authUser !== null) {
      this.props.history.push('/');
    }
  }
  verifymfa=async ()=>
  {
    this.setState({
      gload: true
      // confirmLoading: false,
     });
   let user=this.state.coguser;
    console.log("s",this.state);
    if (user.challengeName === 'SMS_MFA'){
      try{
      var res=await Auth.confirmSignIn(this.state.coguser,this.state.verify);
      console.log("resauth",res);
      
        notification.open({
          message: 'Alert',
          description:
            'MFA Verified successfully',
        
        });


       
          var data=await Auth.currentUserInfo()
          console.log("this is a test",data);
          var x=await Auth.currentSession();
          console.log("session",x)
          this.setState({Userinfo:data.attributes.email})
          const user =  await Auth.currentAuthenticatedUser();
          console.log("user",user)
          this.setState({Group:user.signInUserSession.accessToken.payload["cognito:groups"][0]})
          localStorage.setItem("userEmail",data.attributes.email)
   var x=localStorage.getItem("userEmail")
    console.log("userEmail"+x)
    const currentUserInfo = await Auth.currentUserInfo()
    const UserIdinfo = currentUserInfo.attributes['custom:UserId']
    const UserInitialsinfo = currentUserInfo.attributes['custom:UserInitials']
    const CompanyIdinfo = currentUserInfo.attributes['custom:CompanyId']
 localStorage.setItem('UserIdinfo',UserIdinfo)
 localStorage.setItem('UserInitialsinfo',UserInitialsinfo)
 localStorage.setItem('CompanyIdinfo',CompanyIdinfo)
   setTimeout(() => {
    
    this.setState({
     gload: false
     // confirmLoading: false,
    });
   
  }, 2000);


        localStorage.setItem("user",user.signInUserSession.accessToken.payload["cognito:groups"][0])


         this.props.history.push({
           pathname: '/dashboard',
           userData: this.state.Userinfo,
           groupData: this.state.Group
         });
            }
            catch(e) {
              console.log("error",e);
              notification.open({
                message: 'Alert',
                description:
                  'MFA Verification failed',
               
              });
              this.setState({gload:false})
              this.setState({ isLoading: false });
            }
      
      
    }

  }

  changepassword=async event=>
  {
    this.setState({
      gload: true
      // confirmLoading: false,
     });
   if (this.state.coguser.challengeName === 'NEW_PASSWORD_REQUIRED'){
   // console.log("USER EMAIL"+this.props.location.state.detail.basic_Email+"old password"+this.props.location.state.detail.basic_password+"new password: "+this.state.basic_npassword);
   
   try{
   var eflg=0;
   if((this.state.opassword==this.state.nnpassword || this.state.opassword==this.state.npassword)&&(this.state.opassword==this.state.password) )
   {
    notification.open({
      message: 'Alert',
      description:
        `Validation error current password cannot be used as a new password`
    });
    eflg=1;
   } 
   if(this.state.opassword=='' )
   {
    notification.open({
      message: 'Alert',
      description:
        `Validation error current password is mandatory field.`
    });
    eflg=1;
   } 
   if(this.state.opassword!=this.state.password )
   {
    notification.open({
      message: 'Alert',
      description:
        `Validation error current password doesn't match`
    });
    eflg=1;
   } 
   if(this.state.npassword=='' || this.state.nnpassword=='')
   {
    notification.open({
      message: 'Alert',
      description:
        `Validation error few of required field is missing.`
    });
    eflg=1;
   } 
else if(this.state.npassword!=this.state.nnpassword)
{
  notification.open({
    message: 'Alert',
    description:
      `Validation error password doesn't match`
  });
  eflg=1;
}
else 
{
  var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
  if(!strongRegex.test(this.state.nnpassword))
  {
    notification.open({
      message: 'Alert',
      description:
        `Validation Error Password don't satisfy the desired format`
    });
    eflg=1;
  }

}


if(eflg==0)
{

   var res=await Auth.completeNewPassword(this.state.coguser,this.state.npassword);
    console.log("here",res)
    if(res.username)
    {
      notification.open({
        message: 'Alert',
        description:
          'Password changed successfully'
      });
     
    this.setState({passwordChallenge: false});
    //this.props.history.push("/signin");
    this.setState({
      gload: false
      // confirmLoading: false,
     });
    }
  }
  else{
    this.setState({
      gload: false
      // confirmLoading: false,
     });
  }
  }
  catch(err)
  {
   notification.open({
     message: 'Alert',
     description:
       err.message
   });
    
  }


  
   }
   console.log("changed password"+JSON.stringify(res));
  
  }
  resetpassword=async event=>{
    this.setState({
      gload: true
      // confirmLoading: false,
     });
     if(this.state.npassword===this.state.nnpassword)
     {
       try{
       var res=await Auth.forgotPasswordSubmit(this.state.email,this.state.verify,this.state.npassword);
       console.log("err",res)
       notification.open({
        message: 'Alert',
        description:
          'Password changed successfully'
      });
      this.setState({vreset: false});
   this.setState({gload:false})
       }
       catch(err)
       {
        notification.open({
          message: 'Alert',
          description:
            err.message
        });
         
       }

     }

  }

  validate=event=>
{
 

  if(event.target.id=='email')
  {
    if(event.target.value)
    {
    var bool=validator.email(event.target.value)
  ///console.log("bool",bool);
  if(!bool)
  {
    this.setState({eflag:false})
  this.setState({Nerror:"Invalid Email"})
  
  }
  else{
   // this.setState({eflag:false})
    this.setState({Nerror:""})
  }
}
else
{
  this.setState({Nerror:"Email field is missing"})
}
}
else{

  if(event.target.id=='password'&&event.target.value=='')
  {
    this.setState({eflag:false})
    this.setState({perror:"Password field is missing"})
  }
  else{
    this.setState({perror:""})
    this.setState({eflag:false})
  }

}
if(event.target.id=='opassword' && event.target.value)
  {
    //alert(event.target.value)   // var bool=validator.email(event.target.value)
  ///console.log("bool",bool);
  
  if(this.state.password!=event.target.value)
  {
   // this.setState({eflag2:true})
  this.setState({Nerror:"The current password doesn't match please review"})
  
  }
  else{
   // this.setState({eflag:false})
    this.setState({Nerror:""})
  }
}
if(event.target.id=='npassword' && event.target.value)
  {
    //alert(event.target.value)   // var bool=validator.email(event.target.value)
  ///console.log("bool",bool);
  
  var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
  //var strongRegex =  new RegExp("^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{8,})");
  if(this.state.password==event.target.value)
  {
    //this.setState({eflag:true})
  this.setState({npass:"Current password cannot be used as a new password"})
  
  }
  else if(!strongRegex.test(event.target.value))
  {
   // this.setState({eflag2:true})
    this.setState({npass:"Password should satisfy the desired format"})
    /*notification.open({
      message: 'Alert',
      description:
        <div><ul>
           <li>Minimum 8 character long</li>
          <li>Require numbers</li>
          <li>Require special character</li>
          <li>Require uppercase letters</li>
          <li>Require lowercase letters</li>
        </ul></div>
    });*/
  }
  else{
  //  alert(this.state.nnpassword);
/*  this.setState({nnpass:""})
   if(this.state.nnpassword!=this.state.npassword)
   {
    this.setState({npass:"Password doesn't match"})
  
   }
   else
   {
    this.setState({eflag2:false})
    this.setState({npass:""})
   }*/
    //this.setState({eflag:false})
    this.setState({npass:""})
  }
}
if(event.target.id=='nnpassword' && event.target.value)
  {
    
    if(this.state.npassword!=event.target.value)
    {
  //    this.setState({eflag2:true})
     
      this.setState({nnpass:"Password doesn't match"})
  }
  else{
    this.setState({nnpass:""})
    if(this.state.npass=='')
    {
    this.setState({eflag2:false})
  
    }
  }
}


}
  render() {
    const {getFieldDecorator} = this.props.form;
    const {showMessage, loader, alertMessage} = this.props;
  const text=<div><ul><li>Minimum 8 character long</li><li>Require numbers</li><li>Require special character</li><li>Require uppercase letters</li><li>Require lowercase letters</li></ul></div>;
    if(this.state.vreset)
    {
      return (
        <div className="gx-app-login-wrap">
          <div className="gx-app-login-container">
            <div className="gx-app-login-main-content">
              <div className="gx-app-logo-content">
                <div className="gx-app-logo-content-bg">
                  
                </div>
                <div className="gx-app-logo-wid">
                  <h1><IntlMessages id="app.userAuth.signIn"/></h1>
                 <p> Password Reset Form</p>
                </div>
                <div className="gx-app-logo">
                <img src={require('assets/images/WARD_Digital_Logo_Lockup_Color_Logo.png')}  style={{
        width: "24%",
        marginTop: "42px"

      }}/>
                </div>
              </div>
              <div className="gx-app-login-content">
                <Form autocomplete="off" onSubmit={()=>this.verifymfa(this)} className="gx-signin-form gx-form-row0">
  
                  <FormItem>
                   
                      <Input id="verify" placeholder="Enter Verification Code sent to Email" value={this.state.verify} onChange={this.formhandleChange} required/>
                    
                  </FormItem>
                  <Form.Item>
                  <Input.Password size="large"  id="npassword" placeholder="Enter new password" value={this.state.npassword} onChange={this.formhandleChange} required/>
  </Form.Item>
  <Form.Item>
  <Input.Password size="large"  id="nnpassword" placeholder="Confirm new password" value={this.state.nnpassword} onChange={this.formhandleChange}  required/>
  </Form.Item>
                  
                  <FormItem>
                    <Button type="primary" className="gx-mb-0"  onClick={()=>this.resetpassword(this)} onEnter={()=>this.resetpassword(this)}>
                      Reset Password
                    </Button>{this.state.gload?(<img src={require("assets/images/294.gif")} width="67"/>):<p></p>}
                
                  </FormItem>
                
                  <span
                    className="gx-text-light gx-fs-sm"> </span>
                </Form>
              </div>
  
              {loader ?
                <div className="gx-loader-view">
                  <CircularProgress/>
                </div> : null}
              {showMessage ?
                message.error(alertMessage.toString()) : null}
            </div>
          </div>
        </div>
      );
  }



    if(this.state.mfachallenge)
    {
      return (
        <div className="gx-app-login-wrap">
              <div className="gx-app-login-wrap">
      <div style={{

display: "flex",
  alignItems: "center",
  justifyContent: "center",
  marginTop: "67px"
      }}><img src={require('assets/images/WARD_Digital_Logo_Lockup_Color_Logo.png')}  style={{
        width: "24%",
        marginTop: "42px"

      }}/></div><br/>
 <div className="gx-login-container">
        <div className="gx-login-content">
          <div className="gx-login-header gx-text-center">
            <h1 className="gx-login-title">Code Verification</h1>
            <h5> Welcome to Wardlaw Digital Portal</h5>
          </div>
          <Form autocomplete="off" onSubmit={()=>this.verifymfa(this)} className="gx-signin-form gx-form-row0">

<FormItem>
<Tooltip placement="right" title={"Please enter the verification code sent to registered mobile number."}>
    <Input id="verify" placeholder="Enter Verification Code" value={this.state.verify} onChange={this.formhandleChange} required/>
  </Tooltip>
</FormItem>


<FormItem>
  <Button type="primary2" className="gx-mb-0"  onClick={()=>this.verifymfa(this)} block>
    Verify
  </Button>{this.state.gload?(<div className="aloading"><img src={require("assets/images/294.gif")} width="67"/></div>):<p></p>}
 

</FormItem>

<span
  className="gx-text-light gx-fs-sm"> </span>
</Form>
</div>

{loader ?
<div className="gx-loader-view">
<CircularProgress/>
</div> : null}
{showMessage ?
message.error(alertMessage.toString()) : null}
</div>
</div>
     </div>   
      );
  }

  else if (this.state.passwordChallenge)
  {
    //this.setState({eflag:true})
    return (
      <div className="gx-app-login-wrap">
      <div style={{

display: "flex",
  alignItems: "center",
  justifyContent: "center",
  marginTop: "67px"
      }}><img src={require('assets/images/WARD_Digital_Logo_Lockup_Color_Logo.png')}  style={{
        width: "24%",
        marginTop: "42px"

      }}/></div><br/>
 <div className="gx-login-container">
        <div className="gx-login-content">
          <div className="gx-login-header gx-text-center">
            <h1 className="gx-login-title">Change Password</h1>
            <h5> Welcome to Wardlaw Digital Portal</h5>
          </div>
          <Form autocomplete="off"  className="gx-signin-form gx-form-row0">

<Form.Item


>
<Input.Password size="large"  id="opassword" placeholder="Enter Current Password" onBlur={this.validate} value={this.state.opassword} onChange={this.formhandleChange}  required/>
<div style={{ fontSize: 12, color: "red" }}>
        {this.state.Nerror}
        </div>

</Form.Item>

<Form.Item


>
<Tooltip placement="right" title={text}>
<Input.Password size="large"  id="npassword" placeholder="Enter New Password" onBlur={this.validate} value={this.state.npassword} onChange={this.formhandleChange} required/>
</Tooltip>
<div style={{ fontSize: 12, color: "red" }}>
        {this.state.npass}

       
        </div>

</Form.Item>
<Form.Item>
<Tooltip placement="right" title={text}>
<Input.Password size="large"  id="nnpassword" placeholder="Confirm New Password" onBlur={this.validate} value={this.state.nnpassword} onChange={this.formhandleChange}  required/>
</Tooltip>
<div style={{ fontSize: 12, color: "red" }}>
        {this.state.nnpass}

       
        </div>

</Form.Item>


<Form.Item>
<Button type="primary2" className="gx-mb-0"  onClick={()=>this.changepassword(this)}  disabled={this.state.eflag2} block>
            <IntlMessages id="Change Password"/>
          </Button>{this.state.gload?(<div className="aloading"><img src={require("assets/images/294.gif")} width="67"/></div>):<p></p>}
 
</Form.Item>


    </Form>
  </div>

  {loader ?
    <div className="gx-loader-view">
      <CircularProgress/>
    </div> : null}
  {showMessage ?
    message.error(alertMessage.toString()) : null}

      </div>
   
    </div>
      )

  }
else{
  return (
    <div className="gx-app-login-wrap" >
      <div style={{

display: "flex",
  alignItems: "center",
  justifyContent: "center",
  marginTop: "67px"
      }}><img src={require('assets/images/WARD_Digital_Logo_Lockup_Color_Logo.png')}  style={{
        width: "24%",
        marginTop: "42px"

      }}/></div><br/>
 <div className="gx-login-container">
        <div className="gx-login-content">
          <div className="gx-login-header gx-text-center">
            <h1 className="gx-login-title">Sign In</h1>
            <h5> Welcome to Bluejay Portal</h5>
          </div>
          <Form  className="gx-login-form gx-form-row0">
          <FormItem>
       
  <Input  prefix={<Icon type="user" style={{color: 'rgba(0,0,0,.25)'}}/>} placeholder="Email" id="email" placeholder="Email" onBlur={this.validate} value={this.state.email} onChange={this.formhandleChange} required/>
          <div style={{ fontSize: 12, color: "red" }}>
          {this.state.Nerror}
        </div>
      </FormItem>
      <FormItem>
       
       <Input prefix={<Icon type="lock" style={{color: 'rgba(0,0,0,.25)'}}/>} type="password" placeholder="Password" id="password" value={this.state.password} onBlur={this.validate} onChange={this.formhandleChange}  required/>
       <div style={{ fontSize: 12, color: "red" }}>
        {this.state.perror}
        </div>
   </FormItem>
   <FormItem>
        <Button  type="primary2" style={{backgroundColor:"#0346b9  !important"}} className="gx-mb-0"  onClick={()=>this.formsubmit(this)}  disabled={this.state.eflag} block>
          <IntlMessages id="app.userAuth.signIn"/>
        </Button>
    
      </FormItem>
      <FormItem>
       {/* <Button type="primary" className="gx-mb-0" block disabled><img style={{width: "18px",
    float: "left",
    backgroundColor: "aliceblue"
}} src={require("assets/images/logo-google.svg")}/>
         <span style={{marginTop:"2px"}}><IntlMessages id="Sign In with Google"/></span> 
</Button>*/}
        {this.state.gload?(<div className="aloading"><img src={require("assets/images/294.gif")} width="67"/></div>):<p></p>}
      </FormItem>
            <FormItem>
              {getFieldDecorator('remember', {
                valuePropName: 'checked',
                initialValue: true,
              })(
                <Checkbox>Remember me</Checkbox>
              )}
              {/*<Link className="gx-login-form-forgot" to="">Forgot password</Link>*/}
            </FormItem>
        
          </Form>
        </div>
      </div>
   
    </div>
  );

}

}

}

const WrappedNormalLoginForm = Form.create()(SignIn);

const mapStateToProps = ({auth}) => {
  const {loader, alertMessage, showMessage, authUser} = auth;
  return {loader, alertMessage, showMessage, authUser}
};

export default connect(mapStateToProps, {
  userSignIn,
  hideMessage,
  showAuthLoader,
  userFacebookSignIn,
  userGoogleSignIn,
  userGithubSignIn,
  userTwitterSignIn
})(WrappedNormalLoginForm);
