import React, { Component } from "react";
import { connect } from "react-redux";
import { Avatar, Popover, Button } from "antd";
import { userSignOut } from "appRedux/actions/Auth";
import { Redirect } from "react-router-dom";
import { DatePicker, Switch, message } from "antd";
import IntlMessages from "util/IntlMessages";
import apiCall from "../../apiutil/apicall";
import JsGravatar from 'js-gravatar';
import axios from "axios";
import Gravatar from 'react-gravatar';
import { Card, Row, Col, Input, Modal, notification, Tooltip, Icon } from "antd";
import { Form } from "antd";
import { PhoneOutlined, UserOutlined, MailOutlined } from "@ant-design/icons";

import {
  CheckCircleOutlined,
  CloseCircleOutlined,
  LogoutOutlined
} from "@ant-design/icons";

import AvatarGroup from "@material-ui/lab/AvatarGroup";
import UrlTextField from "../../dynamicForm/UrlTextField";

const FormItem = Form.Item;
const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 }
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 }
  }
};

class UserInfo extends Component {
  constructor(props) {
    super(props);

    this.getUserInfo();

    this.state = {
      collapsed: false,
      isAuthenticated: false,
      isAuthenticating: true,
      userId: "",
      username: "",
      Group: "",
      Userinfo: "",
      fname: "",
      lastname: "",
      dvisible: false,

      fNameError: "",
      lNameError: "",
      mobileError: "",
      isMFAUpdated: false,

      userDetail: {
        UserId: null,
        FirstName: null,
        LastName: null,
        MiddleName: null,
        EmailAddress: null,
        PhoneNum: null,
        RoleId: null,
        RoleName: null,
        RoleDisplayName: null,
        CompanyId: null,
        CompanyName: null,
        LastCompanyId: null,
        DefaultCompanyId: null,
        PrimaryCompanyId: null,
        Expires: null,
        IsEnabled: null,
        IsMFAEnabled: null,
        CreatedOn: null,
        CreatedBy: null,
        LastModifiedBy: localStorage.getItem("userId"),
        av:''
      }
    };
  }

  async componentDidMount() {
    //   this.userHasAuthenticated(true);
    if(!localStorage.getItem('userEmail'))
    {
  
    //  alert(localStorage.getItem('userEmail'))
      window.location.href = 'https://master.dmatxiep20kwq.amplifyapp.com/'
    }
    else
    {
    var avatar=JsGravatar({ email: localStorage.getItem('userEmail'), size: 50, defaultImage: 'blank' });
    
    this.setState({av:avatar});
    var user = localStorage.getItem("userEmail");
    var user_Id = localStorage.getItem("userId");
    this.setState({ Userinfo: user, UserId: user_Id });
    var uinfo = localStorage.getItem("user");
    //const user =  await Auth.currentAuthenticatedUser();
    //console.log(user.signInUserSession.accessToken.payload["cognito:groups"])
    this.setState({ Group: uinfo });
    //  this.props.history.push('/home')
    if (!user) {
      this.setState({ collapsed: true });
    }

    console.log("fname", this.props);
  }
  }

  async getUserInfo() {
    var userId = localStorage.getItem("UserIdinfo");
    try {
      var res = await apiCall.getApi("user/" + userId, "");
      this.setState({ userDetail: res.data.Item });
      if (!res.data.Item.IsMFAEnabled) {
        this.state.userDetail["IsMFAEnabled"] = "N";
      }
    } catch (err) {
      //message.error("Unable to get User");
     
    }
  }

  getusername() {
    var username = this.props.uprops.fname.charAt(0).toUpperCase()+this.props.uprops.fname.slice(1)+' '+this.props.uprops.lname.charAt(0).toUpperCase()+this.props.uprops.lname.slice(1);
    //var username = this.state.Userinfo;
   // console.log("inside uname:" + username);
    //var uname = username.split("@");
    return username;
  }

  async addmodal() {
    //alert("here");
    await this.getUserInfo();
    this.setState({ isMFAUpdated: false });
    this.setState({ dvisible: true, confirmLoading: false });
  }

  handleCancel = () => {
    console.log("Clicked cancel button");
    this.setState({
      dvisible: false,
      confirmLoading: false
    });
    // this.props.mvisible.rmodal(false);
  };

  updateUser = async () => {
    if (this.validateInfo()) {
      this.setState({
        ModalText: "The modal will be closed after two seconds",
        confirmLoading: true
      });
      var params = this.state.userDetail;
      console.log(params);
      var res_mfa = null;
      try {
        var res = await apiCall.putApi("cognitouser", params);

        if (res.data.Item) {
          if (this.state.isMFAUpdated) {
            var mfa_params = {
              LoggedInUser: localStorage.getItem("UserIdinfo")
            };
            if (this.state.userDetail.IsMFAEnabled == "Y") {
              res_mfa = await apiCall.putApi(
                "user/mfa/enable/" + localStorage.getItem("UserIdinfo"),
                mfa_params
              );
            } else {
              res_mfa = await apiCall.putApi(
                "user/mfa/disable/" + localStorage.getItem("UserIdinfo"),
                mfa_params
              );
            }
          }

          setTimeout(() => {
            this.setState({
              dvisible: false,
              confirmLoading: false
            });
          }, 2000);

          notification.open({
            message: "Alert",
            description: `User ${this.state.userDetail.FirstName} updated successfully`,
            icon: <CheckCircleOutlined style={{ color: "#228B22" }} />
          });
        } else {
          notification.open({
            message: "Alert",
            description: `${res.message.message}`,
            icon: <CloseCircleOutlined style={{ color: "red" }} />
          });

          this.setState({
            confirmLoading: false
          });
        }
      } catch (error) {
        notification.open({
          message: "Alert",
          description: error.message + " : Cannot update user",
          icon: <CloseCircleOutlined style={{ color: "red" }} />
        });
        this.setState({
          confirmLoading: false
        });
      }
    }
  };

  validateInfo = () => {
    var code = process.env.REACT_APP_COUNTRYCODE;
    console.log("Inside Validate method.");
    let fNameError = "";
    let lNameError = "";
    let mobileError = "";
    let phoneNumber = this.state.userDetail.PhoneNum;

    var regex_name = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/g;
    var regex_mobileNumber = /^[+][1-9]\d{5,11}$/g;

    if (
      this.state.userDetail.FirstName == null ||
      this.state.userDetail.FirstName == ""
    ) {
      fNameError = "Please enter first name.";
    } else if (regex_name.test(this.state.userDetail.FirstName)) {
      fNameError = "Please enter a valid first name.";
    } else {
      fNameError = "";
    }

    if (
      this.state.userDetail.LastName == null ||
      this.state.userDetail.LastName == ""
    ) {
      lNameError = "Please enter last name.";
    } else if (regex_name.test(this.state.userDetail.LastName)) {
      lNameError = "Please enter a valid last name.";
    } else {
      lNameError = "";
    }

    if (this.state.userDetail.IsMFAEnabled == "Y") {
      if (phoneNumber == null || phoneNumber == "") {
        mobileError = "Please enter Phone Number.";
      } else {
        if (!phoneNumber.startsWith("+")) {
          var user = this.state.userDetail;
          user.PhoneNum = code + phoneNumber;
          this.setState({ userDetail: user });
        }
      }
    } else {
      if (phoneNumber != null && phoneNumber != "") {
        if (!phoneNumber.startsWith("+")) {
          var user = this.state.userDetail;
          user.PhoneNum = code + phoneNumber;
          this.setState({ userDetail: user });
        }
        // if (!regex_mobileNumber.test(this.state.userDetail.PhoneNum)) {
        //   mobileError = "Please enter a valid Phone Number.";
        // } else {
        //   mobileError = "";
        // }
      } else {
        mobileError = "";
      }
    }
    this.setState({ fNameError });

    this.setState({ lNameError });

    this.setState({ mobileError });

    if (fNameError != "" || lNameError != "" || mobileError != "") {
      return false;
    } else {
      return true;
    }
  };

  updateUserMFAStatus = (value, event) => {
    if (value) {
      this.setState({ isMFAUpdated: true });
      this.setState(prevState => ({
        userDetail: {
          ...prevState.userDetail,
          IsMFAEnabled: "Y"
        }
      }));
    } else {
      this.setState({ isMFAUpdated: true });
      this.setState(prevState => ({
        userDetail: {
          ...prevState.userDetail,
          IsMFAEnabled: "N"
        }
      }));
    }
  };

  handleLogout = async () => {
    //	await Auth.signOut();
    //this.setState({ isAuthenticated: false });
    //	this.userHasAuthenticated(false);
    console.log("PROPS FROM APP.JS " + JSON.stringify(this.props));
    localStorage.clear();
    this.setState({ username: "" });
    this.setState({ Group: "" });
    this.setState({ collapsed: true });
  };

  showMyProfile = async () => {
    console.log("PROPS FROM APP.JS " + JSON.stringify(this.props));
    localStorage.clear();
    this.setState({ username: "" });
    this.setState({ Group: "" });
    this.setState({ collapsed: true });
  };

  handleInputChange = event => {
    let fieldId = event.target.id;
    let value = event.target.value == "" ? null : event.target.value;

    this.setState(prevState => ({
      userDetail: {
        ...prevState.userDetail,
        [fieldId]: value
      }
    }));
  };

  render() {
    if (this.state.collapsed) {
      return <Redirect to="/signin" />;
    } else {
      const userMenuOptions = (
        // <ul className="gx-user-popover">
        //   <li onClick={() => this.addmodal(this)}>My Account</li>
        //   <li>Connections</li>
        //   <li onClick={() => this.handleLogout()}>Logout</li>
        // </ul>
        <ul className="gx-user-popover">
          <li
            className="gx-media gx-pointer"
            onClick={() => this.addmodal(this)}
          >
            <UserOutlined className="gx-mr-2" style={{ color: "#62B926" }} />
            <span className="gx-language-text">My Account</span>
          </li>
          <li
            className="gx-media gx-pointer"
            onClick={() => this.handleLogout()}
          >
            <LogoutOutlined className="gx-mr-2" style={{ color: "#f5222d" }} />
            <span className="gx-language-text">Logout</span>
          </li>
        </ul>
      );

      const content = (
        <div>
          <h4>User access to portal will expire on this date.</h4>
        </div>
      );

      return (
        <div>
          <div
            className="gx-flex-row gx-align-items-center gx-mb-4 gx-avatar-row"
            style={{ marginTop: "3px" }}
          >
            <Popover
              placement="bottomRight"
              content={userMenuOptions}
              trigger="click"
            >
              
              <Avatar
                className="gx-size-40 gx-pointer "
                style={{
                  backgroundImage:`url(${this.state.av})`,
                  backgroundSize: "cover",
                  boxShadow: "3px 6px 7px -1px #888888",
                  backgroundColor: "#4eafe8",
                  backgroundColor: "rgb(24, 144, 255)"
                }}
                alt=""
              >   <span style={{opacity: "0.4"}}> {this.props.uprops.fname.charAt(0).toUpperCase()}
                {this.props.uprops.lname.charAt(0).toUpperCase()}</span>
              </Avatar>
              <span className="gx-avatar-name">
                {" "}
                {this.getusername()}
                <i className="icon icon-chevron-down gx-fs-xxs gx-ml-2" />
              </span>
            </Popover>
          </div>
          <Modal
            bodyStyle={{ padding: "0" }}
            title=""
            visible={this.state.dvisible}
            onOk={this.updateUser}
            onCancel={this.handleCancel}
            width={900}
            footer={[
              <Button key="back" onClick={this.handleCancel}>
                Cancel
              </Button>,
              <Button
                key="submit"
                type="primary"
                loading={this.state.confirmLoading}
                onClick={this.updateUser}
              >
                Save
              </Button>
            ]}
          >
            <div>
              <Card
                className="gx-card"
                title={
                  <h2 style={{ color: "#1890ff" }}>
                    <IntlMessages id={"My Account"} />
                  </h2>
                }
                style={{ marginBottom: "0px" }}
              >
                <Form
                  autocomplete="off"
                  onSubmit={this.handleSubmit}
                  style={{ marginLeft: "3px" }}
                >
                  <Row>
                    <Col span={12} className="colcustom">
                      <Form.Item
                        className="fcust"
                        label="First Name"
                        {...formItemLayout}
                      >
                        <Input
                          placeholder="First Name *"
                          id="FirstName"
                          value={this.state.userDetail.FirstName}
                          onChange={this.handleInputChange}
                          prefix={<UserOutlined />}
                          required
                        />
                        <div style={{ fontSize: 12, color: "red" }}>
                          {this.state.fNameError}
                        </div>
                      </Form.Item>
                    </Col>

                    <Col span={12} className="colcustom">
                      <Form.Item
                        className="fcust"
                        label="Last Name"
                        {...formItemLayout}
                      >
                        <Input
                          placeholder="Last Name *"
                          id="LastName"
                          value={this.state.userDetail.LastName}
                          onChange={this.handleInputChange}
                          prefix={<UserOutlined />}
                          required
                        />
                        <div style={{ fontSize: 12, color: "red" }}>
                          {this.state.lNameError}
                        </div>
                      </Form.Item>
                    </Col>

                    <Col span={12} className="colcustom">
                      <Form.Item
                        className="fcust"
                        label="Email"
                        {...formItemLayout}
                      >
                        <Input
                          type="email"
                          placeholder="Email Address *"
                          id="EmailAddress"
                          value={this.state.userDetail.EmailAddress}
                          prefix={<MailOutlined />}
                          disabled
                          required
                        />
                      </Form.Item>
                    </Col>

                    <Col span={12} className="colcustom">
                      <Form.Item
                        className="fcust"
                        label={
                          <span>Phone #&nbsp;
                            <Tooltip title="Format:  [+] [country code] [subscriber number]. Eg: +12019999999">
                              <Icon type="question-circle-o" />
                            </Tooltip>
                          </span>
                        }
                        {...formItemLayout}
                      >
                        <Input
                          type="phone"
                          placeholder="Phone Number"
                          id="PhoneNum"
                          value={this.state.userDetail.PhoneNum}
                          onChange={this.handleInputChange}
                          prefix={<PhoneOutlined />}
                        />
                        <div style={{ fontSize: 12, color: "red" }}>
                          {this.state.mobileError}
                        </div>
                      </Form.Item>
                    </Col>

                    <Col span={12} className="colcustom">
                      <Form.Item label="Company" {...formItemLayout}>
                        <Input
                          type="Company"
                          placeholder={this.state.companyname}
                          id="CompanyName"
                          value={this.state.userDetail.CompanyName}
                          prefix={<i className="icon icon-company" />}
                          disabled
                        />
                      </Form.Item>
                    </Col>

                    <Col span={12} className="colcustom">
                      <Form.Item
                        className="fcust"
                        label="MFA"
                        {...formItemLayout}
                      >
                        <Switch
                          checkedChildren="Enable"
                          unCheckedChildren="Disable"
                          checked={
                            this.state.userDetail.IsMFAEnabled == "Y"
                              ? true
                              : false
                          }
                          onChange={this.updateUserMFAStatus}
                        />
                      </Form.Item>
                    </Col>
                  </Row>
                </Form>
              </Card>
            </div>
          </Modal>
        </div>
      );
    }
  }
}

export default connect(null, { userSignOut })(UserInfo);
