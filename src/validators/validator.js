export default class validator {
   
    static email(x)
    {
       /* var atposition=x.indexOf("@");  
        var dotposition=x.lastIndexOf(".");  
        if (atposition<1 || dotposition<atposition+2 || dotposition+2>=x.length){  
         // alert("Please enter a valid e-mail address \n atpostion:"+atposition+"\n dotposition:"+dotposition);  
          return false;  
          }  */
          //var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
          var re =/[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,4}/igm;
          return re.test(x);
    }
    static allnumeric(inputtxt)
   {
    /*  var numbers = /^[0-9]+$/;
console.log(inputtxt);
if(inputtxt.match(numbers))
{
     return true;
      }
      else
      {
    
      return false;
      }*/
      var re = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
      var re1 = /^\(?[+]?([0-9]{5})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
      var re2 = /^\(?[+]?([0-9]{4})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
      var re3 = /^\(?([0-9]{4})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
      return re.test(inputtxt)||re1.test(inputtxt)||re2.test(inputtxt)||re3.test(inputtxt);
   } 
   static validatefName(input)
   {
      var regex = /^[a-zA-Z ]{2,30}$/;
      return regex.test(input);
   }
   static validatelName(input)
   {
      var regex = /^[a-zA-Z ]{1,30}$/;
      return regex.test(input);
   }
   
   static  jsort(array,prop, desc) {
   array.sort(function(a, b) {
      if (a[prop] < b[prop])
          return desc ? 1 : -1;
      if (a[prop] > b[prop])
          return desc ? -1 : 1;
      return 0;
  });
}
}