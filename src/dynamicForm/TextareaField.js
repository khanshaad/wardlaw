import React from "react";
import { Upload,Spin,Row,Col,Form,Input,Select,Switch,Modal,Popover,Tooltip} from 'antd';
import {
  IdcardOutlined,MenuUnfoldOutlined,EditOutlined,DeleteOutlined,FileExcelTwoTone,EyeOutlined,CheckCircleOutlined,IssuesCloseOutlined,FunctionOutlined,
    MenuFoldOutlined,user,CheckCircleFilled,DownloadOutlined,WarningOutlined,FilterOutlined,
    UserOutlined,LogoutOutlined,PlusCircleOutlined,FormOutlined,
    VideoCameraOutlined,ProfileOutlined,SettingOutlined,RedoOutlined,HourglassOutlined,PhoneOutlined,LinkOutlined,CheckOutlined,CloseOutlined,
    UploadOutlined,HomeOutlined,FieldTimeOutlined,LineChartOutlined,FileSyncOutlined
  } from '@ant-design/icons';

const TextareaField = ({name,ltext,placeholder,required,_handleChange}) => {
  const { TextArea } = Input;
 var x
if(name=="name")
{
x=<UserOutlined/>
}
else if(name=="email")
{
  x=<IdcardOutlined />
}
else if(name=="description")
{
  x=<PhoneOutlined />
}
  return (
  
    <div>
    <p style={{marginBottom: "-5px"}}>{ltext}</p>
    <TextArea placeholder={placeholder}
     style={{ width: "520px"}}
     rows={5} 
     onChange={_handleChange} 
     prefix={ x
       }
     autocomplete='off'
     required={required}
     name={name}
     
     />


    </div>
  );
};

export default TextareaField;
