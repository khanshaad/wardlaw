import React from "react";
import { Upload,Spin,Row,Col,Form,Input,Select,Switch,Modal,Popover,Tooltip} from 'antd';
import {FontColorsOutlined,BankOutlined,
  IdcardOutlined,MenuUnfoldOutlined,EditOutlined,DeleteOutlined,FileExcelTwoTone,EyeOutlined,CheckCircleOutlined,IssuesCloseOutlined,FunctionOutlined,
    MenuFoldOutlined,user,CheckCircleFilled,DownloadOutlined,WarningOutlined,FilterOutlined,
    UserOutlined,LogoutOutlined,PlusCircleOutlined,FormOutlined,
    VideoCameraOutlined,ProfileOutlined,SettingOutlined,RedoOutlined,HourglassOutlined,PhoneOutlined,LinkOutlined,CheckOutlined,CloseOutlined,
    UploadOutlined,HomeOutlined,FieldTimeOutlined,LineChartOutlined,FileSyncOutlined
  } from '@ant-design/icons';

const InputTextField = ({name,ltext,placeholder,required,_handleChange,disabled,wide,values}) => {
 var x
if(name=="name")
{
x=<UserOutlined/>
}
else if(name=="email")
{
  x=<IdcardOutlined />
}
else if(name=="phone")
{
  x=<PhoneOutlined />
}else if(name=="label")
{
  x=<FontColorsOutlined />
}
else if(name=="Company")
{
  x=<BankOutlined />
}
  return (
   
    <div class="form-group">
    <Form.Item
        label={ltext}
   
     
      >
    <Input type="text" placeholder={placeholder}
     style={{ width:wide}} 
     onChange={_handleChange} 
     prefix={ x
       }
     autocomplete='off'
     required={required}
     name={name}
     value={values}
     disabled={disabled}
     />
     </Form.Item>
    </div>
  );
};

export default InputTextField;
