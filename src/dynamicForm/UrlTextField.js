import React from "react";
import { Upload,Spin,Row,Col,Form,Input,Select,Switch,Modal,Popover,Tooltip} from 'antd';
import {
  IdcardOutlined,MenuUnfoldOutlined,EditOutlined,DeleteOutlined,FileExcelTwoTone,EyeOutlined,CheckCircleOutlined,IssuesCloseOutlined,FunctionOutlined,
    MenuFoldOutlined,user,CheckCircleFilled,DownloadOutlined,WarningOutlined,FilterOutlined,
    UserOutlined,LogoutOutlined,PlusCircleOutlined,FormOutlined,
    VideoCameraOutlined,ProfileOutlined,SettingOutlined,RedoOutlined,HourglassOutlined,PhoneOutlined,LinkOutlined,CheckOutlined,CloseOutlined,
    UploadOutlined,HomeOutlined,FieldTimeOutlined,LineChartOutlined,FileSyncOutlined
  } from '@ant-design/icons';

const UrlTextField = ({name,ltext,placeholder,required,_handleChange}) => {
    const { Option } = Select;

    const selectBefore = (
      <Select defaultValue="http://" className="select-before">
        <Option value="http://">http://</Option>
        <Option value="https://">https://</Option>
      </Select>
    );
 var x
if(name=="name")
{
x=<UserOutlined/>
}
else if(name=="email")
{
  x=<IdcardOutlined />
}
else if(name=="description")
{
  x=<PhoneOutlined />
}
  return (
    <Tooltip title={ltext}>
    <div style={{ marginBottom: 16 }}>
    <Input addonBefore={selectBefore}  name={name} style={{ width: "305px"}} defaultValue="mysite"  onChange={_handleChange} />

    </div></Tooltip>
  );
};

export default UrlTextField;
