import React from "react";
import { Upload,Spin,Row,Col,Form,Input,Select,Switch,Modal,Popover,Tooltip} from 'antd';
import "./dropdown.css";
import {
  IdcardOutlined,MenuUnfoldOutlined,EditOutlined,DeleteOutlined,FileExcelTwoTone,EyeOutlined,CheckCircleOutlined,IssuesCloseOutlined,FunctionOutlined,
    MenuFoldOutlined,user,CheckCircleFilled,DownloadOutlined,WarningOutlined,FilterOutlined,
    UserOutlined,LogoutOutlined,PlusCircleOutlined,FormOutlined,
    VideoCameraOutlined,ProfileOutlined,SettingOutlined,RedoOutlined,HourglassOutlined,PhoneOutlined,LinkOutlined,CheckOutlined,CloseOutlined,
    UploadOutlined,HomeOutlined,FieldTimeOutlined,LineChartOutlined,FileSyncOutlined
  } from '@ant-design/icons';

const Dropdown = ({name,ltext,defaultval,placeholder,val,required,_handleChange,wide}) => {
 var x
if(name=="name")
{
x=<UserOutlined/>
}
else if(name=="email")
{
  x=<IdcardOutlined />
}
else if(name=="phone")
{
  x=<PhoneOutlined />
}
else if(name=="chtype")
{
  x=<PhoneOutlined />
}
  return (
    
    <div class="form-group">
    
    <Form.Item
        label={ltext}
     
     
      >
<select class="form-control" name={name} defaultValue={defaultval} placeholder={placeholder}

    onChange={_handleChange} 
    required={required}
    style={{ width:wide,
    fontSize:'11px',
    fontWeight: "bold"

    }}>
     <option hidden>{placeholder}</option>
 {val.map(values=><option value={values} key={values}>{values}</option>)}
</select>

</Form.Item>

    </div>
  
  );
};

export default Dropdown;
