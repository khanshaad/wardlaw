import React from "react";
import { Upload,Spin,Row,Col,Form,Input,Select,Switch,Modal,Popover,Tooltip,DatePicker} from 'antd';
import "./dropdown.css";
import moment from "moment";
import {
  IdcardOutlined,MenuUnfoldOutlined,EditOutlined,DeleteOutlined,FileExcelTwoTone,EyeOutlined,CheckCircleOutlined,IssuesCloseOutlined,FunctionOutlined,
    MenuFoldOutlined,user,CheckCircleFilled,DownloadOutlined,WarningOutlined,FilterOutlined,
    UserOutlined,LogoutOutlined,PlusCircleOutlined,FormOutlined,
    VideoCameraOutlined,ProfileOutlined,SettingOutlined,RedoOutlined,HourglassOutlined,PhoneOutlined,LinkOutlined,CheckOutlined,CloseOutlined,
    UploadOutlined,HomeOutlined,FieldTimeOutlined,LineChartOutlined,FileSyncOutlined
  } from '@ant-design/icons';

const Datepicker = ({name,ltext,defaultval,placeholder,val,required,_handleChange,wide}) => {
 var x
if(name=="name")
{
x=<UserOutlined/>
}
else if(name=="email")
{
  x=<IdcardOutlined />
}
else if(name=="phone")
{
  x=<PhoneOutlined />
}
else if(name=="chtype")
{
  x=<PhoneOutlined />
}
  return (
 
    <div class="form-group">
    
    <Form.Item
        label={ltext}
      
     
      >

    <DatePicker placeholder={placeholder} name={name} defaultValue={defaultval?moment(defaultval):''}  onChange={_handleChange} style={{ width:wide}} 
    required={required}
    />

</Form.Item>
    </div>
   
  );
};

export default Datepicker;
