import React, { Component } from "react";
import { SketchPicker } from "react-color";
import {
  Card,
  Steps,
  Button,
  message,
  Col,
  Form,
  Icon,
  Input,
  Row,
  Select,
  Tooltip,
  DatePicker,
  Switch,
  Modal,
  Upload,
  notification
} from "antd";

import { CheckCircleOutlined, CloseCircleOutlined,UploadOutlined, ShopOutlined, ControlOutlined, BgColorsOutlined, DiffOutlined } from "@ant-design/icons";

import IntlMessages from "util/IntlMessages";
import moment from "moment";
import axios from "axios";
import apiCall from '../../apiutil/apicall';
import "./index.css";

const { TextArea } = Input;
const baseUrl = process.env.REACT_APP_APIBASEURL;
const FormItem = Form.Item;
const Option = Select.Option;
const Step = Steps.Step;
class Create extends Component {
  // static getDerivedStateFromProps(props, state) {
  //   //console.log(this.props.location.company);
  //   if (props.location.company) {
  //       axios.get(
  //         "https://yi70tn8ms6.execute-api.us-east-1.amazonaws.com/dev/api/v1/company/" + props.location.company
  //       )
  //       .then(res => {
  //         //console.log(res.data.Item);
  //         return { companyDetails: {...state.companyDetails, companyDetails: res.data.Item }};
  //       })
  //       .catch(err => {
  //         console.log(err);
  //       });
  //     }
  //   };

  constructor(props) {
    super(props);

    if (this.props.location.company) {
      this.getCompany(this.props.location.company);
    }


    this.state = {
      current: 0,
      loading: false,
      logoData: null,
      isNotSuperAdmin: localStorage.getItem('user') == 'WARD-Super-Admin' ? false : true,
      confirmDirty: false,
      previewVisible: false,
      previewImage: "",
      previewCarrierLogoVisible: false,
      previewCarrierLogoImage: "",
      fileList: [],
      CarrierLogofileList: [],
      displayPC1: false,
      displayPC2: false,
      displaySC1: false,
      displaySC2: false,
      companyLogoFile: null,

      nameError: "",
      domainError: "",
      validUntilError: "",
      websiteError: "",
      agencyError: "",
      agentNameError: "",
      descriptionError: "",
      renewalsError: "",
      onBoardingError: "",
      claimsError: "",
      catError: "",
      logoError: "",
      basicInfoColor: "#038fde",
      subscriptionInfoColor: "#038fde",
      colorThemesandLogoColor: "#038fde",
      additionalInfoColor: "#038fde",

      companyDetails: {
        CompanyId: this.props.location.company ? this.props.location.company : "",
        Name: null,
        Domain: null,
        Expires: moment(new Date().getFullYear()+'-12-31').format("YYYY-MM-DD"),
        IsEnabled: "Y",
        IsMFAEnabled: "N",
        LanguageCode: "en_us",
        Website: null,
        Note: null,
        LogoPath: null,
        PrimaryColor1: "#0076FF",
        PrimaryColor2: "#0076FF",
        SecondaryColor1: "#0076FF",
        SecondaryColor2: "#0076FF",
        EncryptionKey: null,
        Subscriptions: [
          {
            Type: "RENEWALS",
            Status: "Active",
            ValidUntil: moment(new Date().getFullYear()+'-12-31').format("YYYY-MM-DD")
          },
          {
            Type: "ONBOARDING",
            Status: "Active",
            ValidUntil:moment(new Date().getFullYear()+'-12-31').format("YYYY-MM-DD")
          },
          {
            Type: "CLAIMS",
            Status: "Active",
            ValidUntil: moment(new Date().getFullYear()+'-12-31').format("YYYY-MM-DD")
          },
          {
            Type: "CAT",
            Status: "Active",
            ValidUntil: moment(new Date().getFullYear()+'-12-31').format("YYYY-MM-DD")
          },
        ],
        // AgentName: null,
        // AgencyName: null,
        CreatedBy: null,
        CreatedOn: null,
        LastModifiedBy: null,
        LastModifiedOn: null
      }
    };
  }

  validate = () => {
    let isBasicInfoValid = false;
    let isSubscriptionInfoValid = false;
    let isLogoValid = false;
    let isAdditionalInfoValid = false;

    isBasicInfoValid = this.validateBasicInfo();
    isSubscriptionInfoValid = this.validateSubscriptionInfo();
    isLogoValid = this.validateLogo();
    isAdditionalInfoValid = this.validateAdditionalInfo();

    // if(isBasicInfoValid) {
    //   this.setState({basicInfoColor : "#038fde"});
    // } else {
    //   this.setState({basicInfoColor : "#ff0000"});
    // }

    // if(isSubscriptionInfoValid) {
    //   this.setState({subscriptionInfoColor : "#038fde"});
    // } else {
    //   this.setState({subscriptionInfoColor : "#ff0000"});
    // }

    // if(isLogoValid) {
    //   this.setState({colorThemesandLogoColor : "#038fde"});
    // } else {
    //   this.setState({colorThemesandLogoColor : "#ff0000"});
    // }

    // if(isAdditionalInfoValid) {
    //   this.setState({additionalInfoColor : "#038fde"});
    // } else {
    //   this.setState({additionalInfoColor : "#ff0000"});
    // }

    if (
      isBasicInfoValid &&
      isSubscriptionInfoValid &&
      isLogoValid &&
      isAdditionalInfoValid
    ) {
      return true;
    } else {
      return false;
    }
  };

  validateBasicInfo = () => {
    let currDate = moment().format("YYYY-MM-DD");
    let nameError = "";
    let domainError = "";
    let validUntilError = "";
    let websiteError = "";
    let descriptionError = "";

    //[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]
    var regex_name = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/g;
    var regex_companyName = /[`!@#$%^&*()_+\-=\[\]{};':"\\|<>\/?~]/g;
    var regex_domain = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,<>\/?~]/g;
    var regex_url = /^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/g;

    if (
      this.state.companyDetails.Name == null ||
      this.state.companyDetails.Name == ""
    ) {
      nameError = "Please enter company name.";
    } else if (regex_companyName.test(this.state.companyDetails.Name)) {
      nameError = "Please enter a valid company name.";
    } else {
      nameError = "";
    }

    if (
      this.state.companyDetails.Domain == null ||
      this.state.companyDetails.Domain == ""
    ) {
      domainError = "Please enter company domain.";
    } else if (regex_domain.test(this.state.companyDetails.Domain)) {
      domainError = "Please enter a valid company domain.";
    } else {
      domainError = "";
    }

    if (this.state.companyDetails.Expires == "") {
      validUntilError = "Please select Valid Until.";
    } else if (
      moment(this.state.companyDetails.Expires).isBefore(currDate, "day")
    ) {
      validUntilError = "Valid Until cannot be set to previous dates.";
    } else {
      validUntilError = "";
    }

    if (
      this.state.companyDetails.Website != null &&
      this.state.companyDetails.Website != ""
    ) {
      if (!regex_url.test(this.state.companyDetails.Website)) {
        websiteError = "Please enter a valid company URL.";
      } else {
        websiteError = "";
      }
    } else {
      websiteError = "";
    }

    if (
      this.state.companyDetails.Note != null &&
      this.state.companyDetails.Note != ""
    ) {
      if (regex_companyName.test(this.state.companyDetails.Note)) {
        descriptionError = "Please enter a valid company description.";
      } else {
        descriptionError = "";
      }
    } else {
      descriptionError = "";
    }

    this.setState({ nameError });

    this.setState({ domainError });

    this.setState({ validUntilError });

    this.setState({ websiteError });

    this.setState({ descriptionError });

    if (
      nameError != "" ||
      domainError != "" ||
      validUntilError != "" ||
      websiteError != "" ||
      descriptionError != ""
    ) {
      this.setState({ basicInfoColor: "#ff0000" });
      return false;
    } else {
      this.setState({ basicInfoColor: "#038fde" });
      return true;
    }
  };

  validateSubscriptionInfo = () => {
    let currDate = moment().format("YYYY-MM-DD");
    let renewalsError = "";
    let onBoardingError = "";
    let claimsError = "";
    let catError = "";

    if (
      this.state.companyDetails.Subscriptions[0].Status == "Active" ||
      this.state.companyDetails.Subscriptions[0].Status == "Trial"
    ) {
      if (
        this.state.companyDetails.Subscriptions[0].ValidUntil == "" ||
        this.state.companyDetails.Subscriptions[0].ValidUntil == null
      ) {
        renewalsError = "Please select a date for Valid Until.";
      } else if (
        moment(this.state.companyDetails.Subscriptions[0].ValidUntil).isBefore(
          currDate,
          "day"
        )
      ) {
        renewalsError = "Valid Until cannot be set to previous dates.";
      } else {
        renewalsError = "";
      }
    } else {
      renewalsError = "";
    }

    if (
      this.state.companyDetails.Subscriptions[1].Status == "Active" ||
      this.state.companyDetails.Subscriptions[1].Status == "Trial"
    ) {
      if (
        this.state.companyDetails.Subscriptions[1].ValidUntil == "" ||
        this.state.companyDetails.Subscriptions[1].ValidUntil == null
      ) {
        onBoardingError = "Please select a date for Valid Until.";
      } else if (
        moment(this.state.companyDetails.Subscriptions[1].ValidUntil).isBefore(
          currDate,
          "day"
        )
      ) {
        onBoardingError = "Valid Until cannot be set to previous dates.";
      } else {
        onBoardingError = "";
      }
    } else {
      onBoardingError = "";
    }

    if (
      this.state.companyDetails.Subscriptions[2].Status == "Active" ||
      this.state.companyDetails.Subscriptions[2].Status == "Trial"
    ) {
      if (
        this.state.companyDetails.Subscriptions[2].ValidUntil == "" ||
        this.state.companyDetails.Subscriptions[2].ValidUntil == null
      ) {
        claimsError = "Please select a date for Valid Until.";
      } else if (
        moment(this.state.companyDetails.Subscriptions[2].ValidUntil).isBefore(
          currDate,
          "day"
        )
      ) {
        claimsError = "Valid Until cannot be set to previous dates.";
      } else {
        claimsError = "";
      }
    } else {
      claimsError = "";
    }

    if (
      this.state.companyDetails.Subscriptions[3].Status == "Active" ||
      this.state.companyDetails.Subscriptions[3].Status == "Trial"
    ) {
      if (
        this.state.companyDetails.Subscriptions[3].ValidUntil == "" ||
        this.state.companyDetails.Subscriptions[3].ValidUntil == null
      ) {
        catError = "Please select a date for Valid Until.";
      } else if (
        moment(this.state.companyDetails.Subscriptions[3].ValidUntil).isBefore(
          currDate,
          "day"
        )
      ) {
        catError = "Valid Until cannot be set to previous dates.";
      } else {
        catError = "";
      }
    } else {
      catError = "";
    }

    this.setState({ renewalsError });

    this.setState({ onBoardingError });

    this.setState({ claimsError });

    this.setState({ catError });

    if (
      renewalsError != "" ||
      onBoardingError != "" ||
      claimsError != "" ||
      catError != ""
    ) {
      this.setState({ subscriptionInfoColor: "#ff0000" });
      return false;
    } else {
      this.setState({ subscriptionInfoColor: "#038fde" });
      return true;
    }
  };

  validateLogo = () => {
    let logoError = "";

    if (
      this.state.companyLogoFile != null &&
      this.state.companyLogoFile != ""
    ) {
      var fileExt = this.state.companyLogoFile.name.split(".").pop();
      fileExt = fileExt.toUpperCase();

      if (
        fileExt != "ICO" &&
        fileExt != "JPG" &&
        fileExt != "JPEG" &&
        fileExt != "PNG" &&
        fileExt != "BMP"
      ) {
        logoError =
          "Invalid file type uploaded. Valid files types are: JPG/JPEG/PNG/BMP/ICO";
      } else {
        logoError = "";
      }
    } else {
      logoError = "";
    }

    this.setState({ logoError });

    if (logoError != "") {
      this.setState({ colorThemesandLogoColor: "#ff0000" });
      return false;
    } else {
      this.setState({ colorThemesandLogoColor: "#038fde" });
      return true;
    }
  };

  validateAdditionalInfo = () => {
    // let agencyError = "";
    // let agentNameError = "";

    // var regex_name = /[`!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/g;
    // var regex_companyName = /[`!@#$%^&*()_+\-=\[\]{};':"\\|<>\/?~]/g;

    // if (
    //   this.state.companyDetails.AgencyName != null &&
    //   this.state.companyDetails.AgencyName != ""
    // ) {
    //   if (regex_companyName.test(this.state.companyDetails.AgencyName)) {
    //     console.log("Inside Agency error");
    //     agencyError = "Please enter a valid Agency name.";
    //   } else {
    //     agencyError = "";
    //   }
    // } else {
    //   agencyError = "";
    // }

    // console.log("Agent name: " + this.state.companyDetails.AgentName);

    // if (
    //   this.state.companyDetails.AgentName != null &&
    //   this.state.companyDetails.AgentName != ""
    // ) {
    //   if (regex_name.test(this.state.companyDetails.AgentName)) {
    //     agentNameError = "Please enter a valid Agent name.";
    //   } else {
    //     agentNameError = "";
    //   }
    // } else {
    //   agentNameError = "";
    // }

    // this.setState({ agencyError });

    // this.setState({ agentNameError });

    // if (agencyError != "" || agentNameError != "" ) {
    //   return false;
    // } else {
    //   return true;
    // }
    this.setState({ additionalInfoColor: "#038fde" });
    return true;
  };

  createCompany = async () => {
    //console.log(this.state.companyDetails);
    this.setState({ loading: true });
    const isValid = this.validate();
    if (isValid) {
      this.setState(prevState => ({
        companyDetails: {
          ...prevState.companyDetails,
          //Website: "www."+ this.state.companyDetails.Website,
          CreatedBy: localStorage.getItem("userEmail"),
          CreatedOn: new Date("YYYY-mm-ddTHH:MM:ssZ")
        }
      }));

      var res = await apiCall.postApi("company", this.state.companyDetails);

          const companyId = res.data.Item.CompanyId;
          console.log("CompanyId: " + companyId);
          const LogoFormData = new FormData();
          LogoFormData.append("upload", this.state.companyLogoFile);
          if (this.state.companyLogoFile) {
            axios({
              method: "POST",
              url: `${baseUrl}` + "company/uploadlogo/" + companyId,
              data: LogoFormData,
              headers: {
                "Content-Type": "multipart/form-data;"
              }
            })
              .then(res1 => {
                notification.open({
                  message: "Alert",
                  description:
                    "Company " +
                    this.state.companyDetails.Name +
                    " created successfully.",
                  icon: <CheckCircleOutlined style={{ color: "#228B22" }} />
                });
                this.props.location.mprops.sprops.cstate.resetcom()
                console.log("nnprops",this.props.location);
                this.setState({ loading: false });
                this.props.history.push("/manage");
              })
              .catch(err => {
                notification.open({
                  message: "Alert",
                  description:
                    "Company created but failed to upload company logo.",
                  icon: <CloseCircleOutlined style={{ color: "#FF0000" }} />
                });
                this.setState({ loading: false });
                this.props.history.push("/manage");
              });
          } else {
            notification.open({
              message: "Alert",
              description:
                "Company " +
                this.state.companyDetails.Name +
                " created successfully.",
              icon: <CheckCircleOutlined style={{ color: "#228B22" }} />
            });
            this.props.location.mprops.sprops.cstate.resetcom()
            this.setState({ loading: false });
            this.props.history.push("/manage");
          }

    } else {
      notification.open({
        message: "Alert",
        description: "Validations failed.",
        icon: <CloseCircleOutlined style={{ color: "#FF0000" }} />
      });
      this.setState({ loading: false });
    }
  };

  updateCompany = async () => {
    //console.log(this.state.companyDetails);
    this.setState({ loading: true });
    try {
      const isValid = this.validate();
      if (isValid) {
        this.setState(prevState => ({
          companyDetails: {
            ...prevState.companyDetails,
            //Website: "www."+ this.state.companyDetails.Website,
            LastModifiedBy: localStorage.getItem("userEmail"),
            LastModifiedOn: new Date("YYYY-mm-ddTHH:MM:ssZ")
          }
        }));

        var res = await apiCall.putApi("company", this.state.companyDetails);

        const companyId = res.data.Item.CompanyId;
        console.log("CompanyId: " + companyId);
        const LogoFormData = new FormData();
        LogoFormData.append("upload", this.state.companyLogoFile);
        if (this.state.companyLogoFile) {
          axios({
            method: "POST",
            url:
              `${baseUrl}` + "company/uploadlogo/" + companyId,
            data: LogoFormData,
            headers: {
              "Content-Type": "multipart/form-data;"
            }
          })
            .then(res1 => {
              notification.open({
                message: "Alert",
                description:
                  "Updated Company " +
                  this.state.companyDetails.Name +
                  " successfully.",
                icon: <CheckCircleOutlined style={{ color: "#228B22" }} />
              });
            //  this.props.location.mprops.sprops.cstate.resetcom();
              this.setState({loading: false});
              this.props.history.push("/manage");
            })
              .then(res1 => {
                notification.open({
                  message: "Alert",
                  description:
                    "Company " +
                    this.state.companyDetails.Name +
                    " updated successfully.",
                  icon: <CheckCircleOutlined style={{ color: "#228B22" }} />
                });
                this.setState({loading: false});
                this.props.history.push("/manage");
              })
              .catch(err => {
                notification.open({
                  message: "Alert",
                  description:
                    "Company updated but failed to upload company logo.",
                  icon: <CloseCircleOutlined style={{ color: "#FF0000" }} />
                });
                this.setState({loading: false});
              });
          }
           else {
          notification.open({
            message: "Alert",
            description:
              "Updated Company " +
              this.state.companyDetails.Name +
              " successfully.",
            icon: <CheckCircleOutlined style={{ color: "#228B22" }} />
          });
          this.setState({ loading: false });
          this.props.history.push("/manage");
        }
      } else {
        notification.open({
          message: "Alert",
          description: "Validations failed.",
          icon: <CloseCircleOutlined style={{ color: "#FF0000" }} />
        });
        this.setState({ loading: false });
      }
    } catch (error) {
      notification.open({
        message: "Alert",
        description: error.message + " : Cannot update company",
        icon: <CloseCircleOutlined style={{ color: "red" }} />
      });
      this.setState({
        confirmLoading: false
      });
    }
  };

  handleCompanyLogoChange = () => {
    //const LogoFormData = new FormData();
    //LogoFormData.append("companyLogo", file);
    console.log(document.getElementById("uploadCompanyLogo").files[0]);

    if (document.getElementById("uploadCompanyLogo").files.length > 0) {
      this.setState({
        companyLogoFile: document.getElementById("uploadCompanyLogo").files[0]
      });
      var reader = new FileReader();
      var filedata = null;
      reader.onload = function(){
        var output = document.getElementById('previewCompanyLogo');
        output.src = reader.result;
        filedata = reader.result;
      };
      reader.readAsDataURL(document.getElementById('uploadCompanyLogo').files[0]);
      setTimeout(() => {
        this.setState({ logoData: filedata });
        //console.log("logo: "+this.state.logoData)
      }, 2000);
    } else {
      this.setState({ companyLogoFile: null });
    }
  };

  getImage = async id => {
    try {
      var res = await apiCall.getApi("company/getlogo/" + id, "");
      this.setState({ logoData: "data:image/*;base64," + res.data.base64 });
    } catch (err) {
      message.error("Unable to get Company logo");
      console.log(err);
    }
  }


  getCompany = async id => {
    try {
      var res = await apiCall.getApi("company/" + id, "");
      this.setState({ companyDetails: res.data.Item });
      console.log(this.companyDetails);
      if (res.data.Item.LogoPath != null) {
        this.getImage(id);
      }
    } catch (err) {
      message.error("Unable to get Company");
      console.log(err);
    }
  };

  next() {
    let isValid = false;
    if (this.state.current == 0) {
      isValid = this.validateBasicInfo();
    } else if (this.state.current == 1) {
      isValid = this.validateSubscriptionInfo();
    } else if (this.state.current == 2) {
      isValid = this.validateLogo();
    }

    if (isValid) {
      const current = this.state.current + 1;
      this.setState({ current });
    }
  }

  prev() {
    const current = this.state.current - 1;
    this.setState({ current });
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (!err) {
        console.log("Received values of form: ", values);
      }
    });
  };

  handleConfirmBlur = e => {
    const value = e.target.value;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  };

  onDateChange = (date, dateString) => {
    this.setState(prevState => ({
      companyDetails: {
        ...prevState.companyDetails,
        Expires: dateString
      }
    }));
  };

  handleSubscriptionChange = value => {
    this.setState(prevState => ({
      companyDetails: {
        ...prevState.companyDetails,
        Subscription: value
      }
    }));
    //console.log(`selected ${value}`);
  };

  handleCancel = () => this.setState({ previewVisible: false });

  handleCarrierLogoCancel = () =>
    this.setState({ previewCarrierLogoVisible: false });

  handlePreview = file => {
    this.setState({
      previewImage: file.url || file.thumbUrl,
      previewVisible: true
    });
  };

  handleCarrierLogoPreview = file => {
    this.setState({
      previewCarrierLogoImage: file.url || file.thumbUrl,
      previewCarrierLogoVisible: true
    });
  };

  handleChange = ({ fileList }) => {
    //const LogoFormData = new FormData();
    //LogoFormData.append("companyLogo", file);
    //console.log(fileList[0].originFileObj);
    if (fileList.length > 0) {
      this.setState({ companyLogoFile: fileList[0].originFileObj });
    } else {
      this.setState({ companyLogoFile: null });
    }
    this.setState({ fileList });
  };

  handleCarrierLogoChange = ({ fileList }) =>
    this.setState({ CarrierLogofileList: fileList });

  handlePC1Click = () => {
    this.setState({ displayPC1: !this.state.displayPC1 });
  };

  handlePC2Click = () => {
    this.setState({ displayPC2: !this.state.displayPC2 });
  };

  handleSC1Click = () => {
    this.setState({ displaySC1: !this.state.displaySC1 });
  };

  handleSC2Click = () => {
    this.setState({ displaySC2: !this.state.displaySC2 });
  };

  handlePC1Close = () => {
    this.setState({ displayPC1: false });
  };

  handlePC2Close = () => {
    this.setState({ displayPC2: false });
  };

  handleSC1Close = () => {
    this.setState({ displaySC1: false });
  };

  handleSC2Close = () => {
    this.setState({ displaySC2: false });
  };

  handlePC1Change = color => {
    this.setState(prevState => ({
      companyDetails: {
        ...prevState.companyDetails,
        PrimaryColor1: color.hex
      }
    }));
    //this.setState({ companyDetails: { PrimaryColor1 : color.hex }});
  };

  handlePC2Change = color => {
    this.setState(prevState => ({
      companyDetails: {
        ...prevState.companyDetails,
        PrimaryColor2: color.hex
      }
    }));
    //this.setState({ companyDetails: { PrimaryColor2 : color.hex }});
  };

  handleSC1Change = color => {
    this.setState(prevState => ({
      companyDetails: {
        ...prevState.companyDetails,
        SecondaryColor1: color.hex
      }
    }));
    //this.setState({ companyDetails: { SecondaryColor1 : color.hex }});
  };

  handleSC2Change = color => {
    this.setState(prevState => ({
      companyDetails: {
        ...prevState.companyDetails,
        SecondaryColor2: color.hex
      }
    }));
    //this.setState({ companyDetails: { SecondaryColor2 : color.hex }});
  };

  handleInputChange = event => {
    let fieldId = event.target.id;
    let value = event.target.value == "" ? null : event.target.value;

    this.setState(prevState => ({
      companyDetails: {
        ...prevState.companyDetails,
        [fieldId]: value
      }
    }));
  };

  handleWebsiteChange = event => {
    let fieldId = event.target.id;
    let enteredValue = event.target.value == "" ? null : event.target.value;
    if (enteredValue != null || enteredValue != "") {
      let prefix = enteredValue.split(".")[0];
      if (prefix.toUpperCase() != "WWW") {
        enteredValue = "www." + enteredValue;
      }
    }
    this.setState(prevState => ({
      companyDetails: {
        ...prevState.companyDetails,
        [fieldId]: enteredValue
      }
    }));
  };

  handleSubscriptionsStatusChange = index => event => {
    let property = this.state.companyDetails.Subscriptions[index];
    property.Status = event;
    this.setState({ property });
  };

  handleSubscriptionsValidUntilChange = (index, dateFormat) => date => {
    let property = this.state.companyDetails.Subscriptions[index];
    if (date != null) {
      property.ValidUntil = date.format(dateFormat);
    } else {
      property.ValidUntil = null;
    }
    this.setState({ property });
  };

  updateMFAStatus = (value, event) => {
    if (value) {
      this.setState(prevState => ({
        companyDetails: {
          ...prevState.companyDetails,
          IsMFAEnabled: "Y"
        }
      }));
    } else {
      this.setState(prevState => ({
        companyDetails: {
          ...prevState.companyDetails,
          IsMFAEnabled: "N"
        }
      }));
    }
  };

  updateCompanyStatus = (value, event) => {
    if (value) {
      this.setState(prevState => ({
        companyDetails: {
          ...prevState.companyDetails,
          IsEnabled: "Y"
        }
      }));
    } else {
      this.setState(prevState => ({
        companyDetails: {
          ...prevState.companyDetails,
          IsEnabled: "N"
        }
      }));
    }
  };

  changeStep = currStepValue => {
    //const current = this.state.current + 1;
    this.setState({ current: currStepValue });
  };

  render() {
    //this.props.location.mprops.sprops.cstate.resetcom()
    //console.log("nnprops",this.props.location.mprops.sprops.cstate);
    //console.log(this.state.companyDetails.Subscriptions);
    const { current } = this.state;
    const { getFieldDecorator } = this.props.form;
    const {
      previewCarrierLogoVisible,
      previewCarrierLogoImage,
      CarrierLogofileList
    } = this.state;
    const { previewVisible, previewImage, fileList } = this.state;

    const uploadButton = (
      <div>
        <Icon type="plus" />
        <div className="ant-upload-text" style={{ width: 100 }}>
          Upload Company Logo
        </div>
      </div>
    );
    const uploadCarrierButton = (
      <div>
        <Icon type="plus" />
        <div className="ant-upload-text" style={{ width: 100 }}>
          Upload Carrier Logo
        </div>
      </div>
    );
    const PC1background = `${this.state.companyDetails.PrimaryColor1}`;
    const PC2background = `${this.state.companyDetails.PrimaryColor2}`;
    const SC1background = `${this.state.companyDetails.SecondaryColor1}`;
    const SC2background = `${this.state.companyDetails.SecondaryColor2}`;

    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 }
      }
    };
    const tailFormItemLayout = {
      wrapperCol: {
        xs: {
          span: 24,
          offset: 0
        },
        sm: {
          span: 16,
          offset: 8
        }
      }
    };

    // const selectBefore = (
    //   <Select defaultValue="https://" style={{ width: 90 }}>
    //     <Option value="http://">http://</Option>
    //     <Option value="https://">https://</Option>
    //   </Select>
    // );

    const dateFormat = "YYYY-MM-DD";

    const steps = [
      {
        title: (
          <Button
            type="link"
            style={{ color: this.state.basicInfoColor }}
            onClick={() => this.changeStep(0)}
          >
            Basic Info
          </Button>
        ),
        content: (
          <div class='companyedit'>
          <Form>
            <Row>
              <Col span={12}>
                <FormItem
                  {...formItemLayout}
                  label={
                    <span>
                      {" "}
                      <span style={{ color: "red", fontSize: "18px" }}>
                        *
                      </span>{" "}
                      Name{" "}
                    </span>
                  }
                >
                  <Input
                    id="Name"
                    value={this.state.companyDetails.Name}
                    onChange={this.handleInputChange}
                    //disabled={this.state.isNotSuperAdmin}
                    disabled={this.props.location.company == null ? false:true}
                    maxlength="24"
                  />
                  <div style={{ fontSize: 12, color: "red" }}>
                    {this.state.nameError}
                  </div>
                </FormItem>
              </Col>
              <Col span={12}>
                <FormItem
                  {...formItemLayout}
                  label={
                    <span>
                      {" "}
                      <span style={{ color: "red", fontSize: "18px" }}>
                        *
                      </span>{" "}
                      Domain{" "}
                    </span>
                  }
                >
                  <Input
                    id="Domain"
                    value={this.state.companyDetails.Domain}
                    onChange={this.handleInputChange}
                    disabled={this.state.isNotSuperAdmin}
                   
                  />
                  <div style={{ fontSize: 12, color: "red" }}>
                    {this.state.domainError}
                  </div>
                </FormItem>
              </Col>
            </Row>

            <Row>
              <Col span={12}>
                <FormItem
                  {...formItemLayout}
                  label={
                    <span>
                      <span style={{ color: "red", fontSize: "18px" }}>*</span>
                      &nbsp;Valid Until&nbsp;
                      <Tooltip title="Last date for Company subscription.">
                        <Icon type="question-circle-o" />
                      </Tooltip>
                    </span>
                  }
                >
                  <DatePicker
                    value={moment(
                      this.state.companyDetails.Expires,
                      dateFormat
                    )}
                    format={dateFormat}
                    className="gx-mb-3 gx-w-100"
                    onChange={this.onDateChange}
                    disabled={this.state.isNotSuperAdmin}
                  />
                  <div style={{ fontSize: 12, color: "red" }}>
                    {this.state.validUntilError}
                  </div>
                </FormItem>
              </Col>

              <Col span={12}>
                <FormItem {...formItemLayout} label={<span> Website </span>}>
                  <Input
                    addonBefore="https://"
                    id="Website"
                    value={this.state.companyDetails.Website}
                    onChange={this.handleInputChange}
                    placeholder="website url"
                    disabled={this.state.isNotSuperAdmin}
                    maxlength="24"
                  />
                  <div style={{ fontSize: 12, color: "red" }}>
                    {this.state.websiteError}
                  </div>
                </FormItem>
              </Col>
            </Row>

            <Row>
              <Col span={12}>
                <FormItem
                  {...formItemLayout}
                  label={<span> Description </span>}
                >
                  <TextArea
                    placeholder="Desc."
                    id="Note"
                    value={this.state.companyDetails.Note}
                    onChange={this.handleInputChange}
                    autosize={{ minRows: 5, maxRows: 6 }}
                    disabled={this.state.isNotSuperAdmin}
                    maxlength="50"
                  />
                  <div style={{ fontSize: 12, color: "red" }}>
                    {this.state.descriptionError}
                  </div>
                </FormItem>
              </Col>
              <Col span={12}>
                <Row>
                  <Col span={24}>
                  <FormItem
                  {...formItemLayout}
                  label={
                    <span>
                      {" "}
                      MFA&nbsp;
                      <Tooltip title="If enabled - MFA required while company user login.">
                        <Icon type="question-circle-o" />
                      </Tooltip>
                    </span>
                  }
                >
                  {getFieldDecorator('companyMFAStatus', {
                                valuePropName: 'checked',
                                initialValue: this.state.companyDetails.IsMFAEnabled == "Y" ? true : false
                            }
                            )(
                  <Switch
                    checkedChildren="Enable"
                    unCheckedChildren="Disable"
                    onChange={this.updateMFAStatus}
                    //disabled={this.state.isNotSuperAdmin}
                    disabled={true}
                  />)}
                </FormItem>
                  </Col>
                  <Col span={24}>
                    <FormItem
                      {...formItemLayout}
                      label={
                        <span>
                          {" "}
                          Status&nbsp;
                          <Tooltip title="Used to specify whether the company is active or not.">
                            <Icon type="question-circle-o" />
                          </Tooltip>
                        </span>
                      }
                    >
                      {getFieldDecorator("companyStatus", {
                        valuePropName: "checked",
                        initialValue:
                          this.state.companyDetails.IsEnabled == "Y"
                            ? true
                            : false
                      })(
                        <Switch
                          checkedChildren="Active"
                          unCheckedChildren="Inactive"
                          onChange={this.updateCompanyStatus}
                          disabled="true"
                        />
                      )}
                    </FormItem>
                  </Col>
                </Row>
              </Col>
            </Row>

            </Form>
            </div>
        ),
        icon: <ShopOutlined />
      },
      {
        title: (
          <Button
            type="link"
            style={{ color: this.state.subscriptionInfoColor }}
            onClick={() => this.changeStep(1)}
          >
            Subscription Info
          </Button>
        ),
        content: (
          <div class='companyedit'>
            <Form onSubmit={this.handleSubmit}>
              <Row>
                <Col span={5}></Col>
                <Col span={18}>
                  <Row style={{marginBottom: -15}}>
                    <Col span={12}>
                      <FormItem
                        {...formItemLayout}
                        label={
                          <span style={{ fontSize: "15px", color: "#038fde" }}>
                            TYPE
                          </span>
                        }
                      >
                        <span style={{ fontSize: "15px", color: "#038fde" }}>
                          STATUS
                        </span>
                      </FormItem>
                    </Col>
                    <Col span={12}>
                      <FormItem>
                        <span style={{ fontSize: "15px", color: "#038fde" }}>
                          VALID UNTIL
                        </span>
                      </FormItem>
                    </Col>
                  </Row>

                  <Row style={{marginBottom: -20}}>
                <Col span={12}>
                  <FormItem {...formItemLayout} label="RENEWALS">
                    <Select
                      className="gx-mr-3 gx-mb-3"
                      defaultValue={this.state.companyDetails.Subscriptions[0].Status}
                      onChange={this.handleSubscriptionsStatusChange(0)}
                      disabled={this.state.isNotSuperAdmin}
                    >
                      <Option value="Active">Active</Option>
                      <Option value="InActive">InActive</Option>
                      <Option value="Trial">Trial</Option>
                    </Select>
                  </FormItem>
                </Col>
                <Col span={12}>
                {this.state.companyDetails.Subscriptions[0].Status != "InActive" ?
                  <FormItem>
                    <DatePicker
                      id = "Subscriptions[0].ValidUntil"
                      defaultValue={moment(
                        this.state.companyDetails.Subscriptions[0].ValidUntil,
                        dateFormat
                      )}
                      format={dateFormat}
                      className="gx-mb-3 gx-w-100"
                      onChange={this.handleSubscriptionsValidUntilChange(0, dateFormat)}
                      disabled={this.state.isNotSuperAdmin}

                    />
                    <div style={{ fontSize: 12, color: "red" }}>
                      {this.state.renewalsError}
                    </div>
                  </FormItem> : null }
                </Col>
              </Row>

                  <Row style={{marginBottom: -20}}>
                <Col span={12}>
                  <FormItem {...formItemLayout} label="ONBOARDING">
                    <Select
                      className="gx-mr-3 gx-mb-3"
                      defaultValue={this.state.companyDetails.Subscriptions[1].Status}
                      onChange={this.handleSubscriptionsStatusChange(1)}
                      disabled={this.state.isNotSuperAdmin}
                    >
                      <Option value="Active">Active</Option>
                      <Option value="InActive">InActive</Option>
                      <Option value="Trial">Trial</Option>
                    </Select>
                  </FormItem>
                </Col>
                <Col span={12}>
                {this.state.companyDetails.Subscriptions[1].Status != "InActive" ?
                  <FormItem>
                    <DatePicker
                      defaultValue={moment(
                        this.state.companyDetails.Subscriptions[1].ValidUntil,
                        dateFormat
                      )}
                      format={dateFormat}
                      className="gx-mb-3 gx-w-100"
                      onChange={this.handleSubscriptionsValidUntilChange(1, dateFormat)}
                      disabled={this.state.isNotSuperAdmin}
                    />
                    <div style={{ fontSize: 12, color: "red" }}>
                      {this.state.onBoardingError}
                    </div>
                    </FormItem> : null }
                </Col>
              </Row>

                  <Row style={{marginBottom: -20}}>
                <Col span={12}>
                  <FormItem {...formItemLayout} label="CLAIMS">
                    <Select
                      className="gx-mr-3 gx-mb-3"
                      defaultValue={this.state.companyDetails.Subscriptions[2].Status}
                      onChange={this.handleSubscriptionsStatusChange(2)}
                      disabled={this.state.isNotSuperAdmin}
                    >
                      <Option value="Active">Active</Option>
                      <Option value="InActive">InActive</Option>
                      <Option value="Trial">Trial</Option>
                    </Select>
                  </FormItem>
                </Col>
                <Col span={12}>
                {this.state.companyDetails.Subscriptions[2].Status != "InActive" ?
                  <FormItem>
                    <DatePicker
                      defaultValue={moment(
                        this.state.companyDetails.Subscriptions[2].ValidUntil,
                        dateFormat
                      )}
                      format={dateFormat}
                      className="gx-mb-3 gx-w-100"
                      onChange={this.handleSubscriptionsValidUntilChange(2, dateFormat)}
                      disabled={this.state.isNotSuperAdmin}
                    />
                    <div style={{ fontSize: 12, color: "red" }}>
                      {this.state.claimsError}
                    </div>
                    </FormItem> : null }
                </Col>
              </Row>

                  <Row style={{marginBottom: -20}}>
                <Col span={12}>
                  <FormItem {...formItemLayout} label="CAT">
                    <Select
                      className="gx-mr-3 gx-mb-3"
                      defaultValue={this.state.companyDetails.Subscriptions[3].Status}
                      onChange={this.handleSubscriptionsStatusChange(3)}
                      disabled={this.state.isNotSuperAdmin}
                    >
                      <Option value="Active">Active</Option>
                      <Option value="InActive">InActive</Option>
                      <Option value="Trial">Trial</Option>
                    </Select>
                  </FormItem>
                </Col>
                <Col span={12}>
                {this.state.companyDetails.Subscriptions[3].Status != "InActive" ?
                  <FormItem>
                    <DatePicker
                      defaultValue={moment(
                        this.state.companyDetails.Subscriptions[3].ValidUntil,
                        dateFormat
                      )}
                      format={dateFormat}
                      className="gx-mb-3 gx-w-100"
                      onChange={this.handleSubscriptionsValidUntilChange(3, dateFormat)}
                      disabled={this.state.isNotSuperAdmin}
                    />
                    <div style={{ fontSize: 12, color: "red" }}>
                      {this.state.catError}
                    </div>
                    </FormItem> : null }
                </Col>
              </Row>
                </Col>
              </Row>
            </Form>
          </div>
        ),
        icon: <ControlOutlined />
      },
      {
        title: (
          <Button
            type="link"
            style={{ color: this.state.colorThemesandLogoColor }}
            onClick={() => this.changeStep(2)}
          >
            Color Themes and Logo
          </Button>
        ),
        content: (
          <div class='companyedit'>
          <Row>
            <Col span={14}>
              <Row>
                <Col span={24}>
                  <FormItem
                    {...formItemLayout}
                    label={<span> Primary Color #1 </span>}
                  >
                    <div style={{ paddingTop: 12 }}>
                      <div
                        className="cp-swatch"
                        onClick={this.handlePC1Click.bind(this)}
                      >
                        <div
                          className="cp-color"
                          style={{
                            backgroundColor: PC1background,
                            height: 20,
                            width: 150
                          }}
                        />
                      </div>
                      {this.state.displayPC1 ? (
                        <div className="cp-popover">
                          <div
                            className="cp-cover"
                            onClick={this.handlePC1Close.bind(this)}
                          />
                          <SketchPicker
                            color={this.state.companyDetails.PrimaryColor1}
                            onChange={this.handlePC1Change.bind(this)}
                          />
                        </div>
                      ) : null}
                    </div>
                  </FormItem>
                </Col>
                <Col span={24}>
                  <FormItem
                    {...formItemLayout}
                    label={<span> Primary Color #2 </span>}
                  >
                    <div style={{ paddingTop: 12 }}>
                      <div
                        className="cp-swatch"
                        onClick={this.handlePC2Click.bind(this)}
                      >
                        <div
                          className="cp-color"
                          style={{
                            backgroundColor: PC2background,
                            height: 20,
                            width: 150
                          }}
                        />
                      </div>
                      {this.state.displayPC2 ? (
                        <div className="cp-popover">
                          <div
                            className="cp-cover"
                            onClick={this.handlePC2Close.bind(this)}
                          />
                          <SketchPicker
                            color={this.state.companyDetails.PrimaryColor2}
                            onChange={this.handlePC2Change.bind(this)}
                          />
                        </div>
                      ) : null}
                    </div>
                  </FormItem>
                </Col>
                <Col span={24}>
                  <FormItem
                    {...formItemLayout}
                    label={<span> Secondary Color #1 </span>}
                  >
                    <div style={{ paddingTop: 12 }}>
                      <div
                        className="cp-swatch"
                        onClick={this.handleSC1Click.bind(this)}
                      >
                        <div
                          className="cp-color"
                          style={{
                            backgroundColor: SC1background,
                            height: 20,
                            width: 150
                          }}
                        />
                      </div>
                      {this.state.displaySC1 ? (
                        <div className="cp-popover">
                          <div
                            className="cp-cover"
                            onClick={this.handleSC1Close.bind(this)}
                          />
                          <SketchPicker
                            color={this.state.companyDetails.SecondaryColor1}
                            onChange={this.handleSC1Change.bind(this)}
                          />
                        </div>
                      ) : null}
                    </div>
                  </FormItem>
                </Col>
                <Col span={24}>
                  <FormItem
                    {...formItemLayout}
                    label={<span> Secondary Color #2 </span>}
                  >
                    <div style={{ paddingTop: 12 }}>
                      <div
                        className="cp-swatch"
                        onClick={this.handleSC2Click.bind(this)}
                      >
                        <div
                          className="cp-color"
                          style={{
                            backgroundColor: SC2background,
                            height: 20,
                            width: 150
                          }}
                        />
                      </div>
                      {this.state.displaySC2 ? (
                        <div className="cp-popover">
                          <div
                            className="cp-cover"
                            onClick={this.handleSC2Close.bind(this)}
                          />
                          <SketchPicker
                            color={this.state.companyDetails.SecondaryColor2}
                            onChange={this.handleSC2Change.bind(this)}
                          />
                        </div>
                      ) : null}
                    </div>
                  </FormItem>
                </Col>
              </Row>
            </Col>
            <Col span={10}>
              <Row>
                <Col span={24}>
                  <div>
                    {/* <Upload
                      //action="//jsonplaceholder.typicode.com/posts/"
                      listType="picture-card"
                      fileList={fileList}
                      onPreview={this.handlePreview}
                      onChange={this.handleChange}
                    >
                      {fileList.length >= 1 ? null : uploadButton}
                    </Upload>
                    <Modal
                      visible={previewVisible}
                      footer={null}
                      onCancel={this.handleCancel}
                    >
                      <img
                        alt="example"
                        style={{ width: "200%" }}
                        src={previewImage}
                      />
                    </Modal> */}
                        <FormItem
                      {...formItemLayout}
                      label={<span> Company Logo&nbsp;
                      <Tooltip title="Valid file types: JPEG/JPG/PNG/BMP/ICO.">
                          <Icon type="question-circle-o" />
                        </Tooltip>
                      </span>}
                    >
                      <label class="custom-file-upload">
                      <input type='file' id="uploadCompanyLogo" accept="image/*" onChange={this.handleCompanyLogoChange} /><UploadOutlined style={{marginTop: "11px"}}/> Upload Logo
                      </label><div style={{ fontSize: 12, color: "red" }}>
                        {this.state.logoError}
                      </div>
                    </FormItem>
                  </div>
                </Col>
                <Col span={24}>
                  <p></p>
                </Col>
                <Col span={24} style={{marginTop: "15px"}}>
                  <img id="previewCompanyLogo" src={this.state.logoData} style={{ height: "70px", width: "auto" }}/>
                </Col>
              </Row>
            </Col>
          </Row>
          </div>
        ),
        icon: <BgColorsOutlined />
      },
      {
        title: (
          <Button
            type="link"
            style={{ color: this.state.additionalInfoColor }}
            onClick={() => this.changeStep(3)}
          >
            Additional Info
          </Button>
        ),
        content: (
          <div class='companyedit'>
          <Form>
            <Row>
              <Col span={12}>
                <FormItem {...formItemLayout} label={<span> Field 1 </span>}>
                  <Input
                    id="Field1"
                    //onChange={this.handleInputChange}
                  />
                  {/* <div style={{ fontSize: 12, color: "red" }}>
                    {this.state.agencyError}
                  </div> */}
                </FormItem>
              </Col>
              <Col span={12}>
                <FormItem {...formItemLayout} label={<span> Field 2 </span>}>
                  <Input
                    id="Field2"
                    //onChange={this.handleInputChange}
                  />
                  {/* <div style={{ fontSize: 12, color: "red" }}>
                    {this.state.agentNameError}
                  </div> */}
                </FormItem>
              </Col>
            </Row>
          </Form>
          </div>
        ),
        icon: <DiffOutlined />
      }
    ];

    return (
      <Card
        className="gx-card"
        title={
          <h2 style={{ color: "#1890ff" }}>
            <IntlMessages id={this.props.location.company == null ? "Create Company" : "Edit Company"} />
          </h2>
        }
      >
        <Steps current={current}>
          {steps.map(item => (
            <Step key={item.title} title={item.title} icon={item.icon}/>
          ))}
        </Steps>
        <div className="steps-content">{steps[this.state.current].content}</div>
        {/* <div className="steps-action">
          {this.state.current > 0 && (
            <Button style={{ marginLeft: 8 }} onClick={() => this.prev()}>
              Previous
            </Button>
          )}
          {this.state.current === steps.length - 1 && (
            <Button
              type="primary"
              loading={this.state.loading}
              onClick={
                this.props.location.company == null
                  ? this.createCompany
                  : this.updateCompany
              }
            >
              Done
            </Button>
          )}
          {this.state.current < steps.length - 1 && (
            <Button type="primary" onClick={() => this.next()}>
              Next
            </Button>
          )}
        </div> */}

        <div >
          <Button type="primary"
          style={{marginTop: 15, marginBottom: -5}}
          loading={this.state.loading} onClick={this.props.location.company == null ? this.createCompany : this.updateCompany}>
            Save
          </Button>
        </div>
      </Card>
    );
  }
}

export default Form.create()(Create);
