import React, { Component } from "react";
import {
  Card,
  Button,
  message,
  Col,
  Form,
  Input,
  Row
} from "antd";
import IntlMessages from "util/IntlMessages";
import axios from "axios";
import apiCall from '../../apiutil/apicall';

const { TextArea } = Input;
const FormItem = Form.Item;

class Config extends Component {
  constructor(props) {
    super(props);
    this.getSubsConfig();
    this.state = {
      subsHeaders: []
    };
  }

  getSubsConfig = async () => {
    try {
      var res = await apiCall.getApi("subscriptionheaders");
      // var res = await axios.get(
      //   "http://localhost:3001/api/v1/subscriptionheaders"
      // );
      console.log("Res: "+JSON.stringify(res, null, 2))
      if(res.data.Items)
      res.data.Items.sort((a, b) => {
        return a.Order - b.Order;
      });
      this.setState({ subsHeaders: res.data.Items });
    } catch (err) {
      message.error("Unable to get Subscriptions Headers");
      console.log(err);
    }
  };

  UpdateSubsHeaders = async () => {
    try {
      var res = await apiCall.putApi("subscriptionheaders", this.state.subsHeaders);
      // var res = await axios.put(
      //   "http://localhost:3001/api/v1/subscriptionheaders", this.state.subsHeaders
      // );
      console.log("Res: "+JSON.stringify(res, null, 2))
      if(res.data.status == 200)
      message.success("Updated Subscriptions Headers");
    } catch (err) {
      message.error("Unable to Update Subscriptions Headers");
      console.log(err);
    }
  };

  handleHeaderChange = index => event => {
    console.log(event)
    let value = event.target.value == "" ? null : event.target.value;
    let data = this.state.subsHeaders;
    data[index].Header = value;
    data[index].LastmodifiedBy = localStorage.getItem('UserIdinfo');
    this.setState({subsHeaders : data});
  };

  handleSubHeaderChange = index => event => {
    console.log(event)
    let value = event.target.value == "" ? null : event.target.value;
    let data = this.state.subsHeaders;
    data[index].SubHeader = value;
    data[index].LastmodifiedBy = localStorage.getItem('UserIdinfo');
    this.setState({subsHeaders : data});
  };

  render() {
    const formItemLayout = {
      labelCol: {
        xs: { span: 24 },
        sm: { span: 8 }
      },
      wrapperCol: {
        xs: { span: 24 },
        sm: { span: 16 }
      }
    };

    return (
      <Card
        className="gx-card"
        title={
          <h2 style={{ color: "#1890ff" }}>
            <IntlMessages id="End-user Email Template Setup" />
          </h2>
        }
      >
        <Card title="" style={{ marginTop: -15 }}>
          <Row>
            <Col span={24}>
              <Row style={{ marginBottom: -15 }}>
                <Col span={14}>
                  <FormItem {...formItemLayout}
                    label={
                      <span style={{ fontSize: "15px", color: "#038fde" }}>
                        SUBSCRIPTION TYPE
                          </span>
                    }
                  >
                    <span style={{ fontSize: "15px", color: "#038fde" }}>
                      HEADER
                  </span>
                  </FormItem>
                </Col>
                <Col span={10}>
                  <FormItem>
                  <span style={{ fontSize: "15px", color: "#038fde", marginLeft: 15 }}>
                    SUB HEADER
                  </span>
                  </FormItem>
                </Col>
              </Row>

              {this.state.subsHeaders.map((subsType, index) => (
                <Row key={index}>
                  <Col span={14}>
                    <FormItem {...formItemLayout} label={subsType.DisplayName}>
                      <TextArea
                        placeholder="Header"
                        id="Header"
                        value={subsType.Header}
                        onChange={this.handleHeaderChange(index)}
                        autosize={{ minRows: 2, maxRows: 3 }}
                      //disabled={this.state.isNotSuperAdmin}
                      //maxlength="50"
                      />
                    </FormItem>
                  </Col>
                  <Col span={10}>
                    <TextArea
                      placeholder="SubHeader"
                      id="SubHeader"
                      value={subsType.SubHeader}
                      onChange={this.handleSubHeaderChange(index)}
                      autosize={{ minRows: 2, maxRows: 3 }}
                    //disabled={this.state.isNotSuperAdmin}
                    //maxlength="50"
                    />
                  </Col>
                </Row>
              ))}
            </Col>
          </Row>
        </Card>

        <div>
          <Button type="primary" style={{ marginTop: -15, marginBottom: -5 }} onClick={this.UpdateSubsHeaders}>
            Save
          </Button>
        </div>
      </Card>
    );
  }
}

export default Config;
