import React, {Component} from "react";
import { Card,Table,Button,Modal,Form,Col,Row,notification} from 'antd';
import IntlMessages from "util/IntlMessages";
import axios from 'axios';

import './fcust.css'
import InputTextField from '../../dynamicForm/InputTextField'
import Datepicker from "../../dynamicForm/Datepicker";
import apiCall from '../../apiutil/apicall';
import * as _JFORM from "./Form.json";
import Dropdown from '../../dynamicForm/Dropdown';
import TextareaField from '../../dynamicForm/TextareaField';
import UrlTextField from '../../dynamicForm/UrlTextField';
import { AreaChartOutlined,CheckCircleOutlined,CloseCircleOutlined} from '@ant-design/icons';
class CTAPage  extends Component {
    constructor(props) {
		super(props);
    this.state = {
      collapsed: false,
      comid:'',
      userdata:[],
      dvisible:false,
      fields:[],
      cdata:''
    }
}
    async componentDidMount() {
       // var company=this.state.company;
       // var ainfo=[];

       this.setState({fields:_JFORM[0].CTA})
      
        var comid=localStorage.getItem('com');
        if(comid!='All' && comid)
        {
          //comid='';
    //    alert("here")
        this.setState({comid});
      
      var  res= await apiCall.getApi(`ctasetup/company/${comid}`)
        console.log("res",res)
     
      
       var x=this.search(res.data.Items, this.props.location.state.subtype)
       this.setState({cdata:x})
        }
    }
async reload()
{
  var comid=localStorage.getItem('com');
  if(comid!='All' && comid)
  {
    //comid='';
//    alert("here")
  this.setState({comid});

var  res= await apiCall.getApi(`ctasetup/company/${comid}`)
  console.log("res",res)


 var x=this.search(res.data.Items, this.props.location.state.subtype)
 this.setState({cdata:x})
  }
}
    search(source, nam){
      var results;
      //console.log("setcompcall",nam)  
      //console.log("res"+JSON.stringify(source));
      nam = nam.toUpperCase();
      results = source.filter(function(entry) {
          return entry.SubscriptionType.toUpperCase().indexOf(nam) !== -1;
      });
  
      console.log("res"+JSON.stringify(results));
      return results;
  }
    addmodal()
  {
    //alert("here");
    var setCom=this.state.fields;
    console.log("setCom",this.state.fields[2]); 
    setCom[2].placeholder=this.state.companyname;
    this.setState({fields:setCom});
    this.setState({dvisible:true});
    
  }
  handleCancel = () => {
    console.log('Clicked cancel button');
    this.setState({
      dvisible: false,
    });
   // this.props.mvisible.rmodal(false);
  };

  _handleChange=event=>{
    // console.log("einput",JSON.stringify(event))
   this.setState({
   [event.currentTarget.name]:event.currentTarget.value
   
   });
   
   }

   handleOk =async () => {
    this.setState({
      ModalText: 'The modal will be closed after two seconds',
      confirmLoading: true,
    });
  
  

    
    //console.log("uparams",params);
    
   

  var params = {
   
    "CompanyId":localStorage.getItem('com'),
    "SubscriptionType": this.props.location.state.subtype,
    "Label": this.state.label,
    "URL": this.state.url,
    "Description": this.state.description,
    "CreatedBy": localStorage.getItem('UserIdinfo')

   
}

console.log("data",params)
try{

var res=await apiCall.postApi('ctasetup', params);
console.log("res-u",res);

//alert(res.data.Item)

if(res.data.Item)
{
    setTimeout(() => {
        this.setState({
         dvisible: false,
          confirmLoading: false,
        });
       
      }, 2000);

      notification.open({
        message: 'Alert',
        description:
          `CTA added successfully`,
        icon: <CheckCircleOutlined  style={{ color: '#228B22' }} />,
      });
     this.reload();
  // this.props.mvisible.rmodal(false);
}

else
{
   

      notification.open({
        message: 'Alert',
        description:
          `${res.message.message}`,
        icon: <CloseCircleOutlined  style={{ color: '#228B22' }} />,
      }); 
}
}
//"Record with same combination of company and subscription type already exists. Please perform an update action."
catch(error)
{
  console.log("error",error)
   
      notification.open({
        message: 'Alert',
        description:
          "Record with same combination of company and subscription type already exists. Please perform an update action.",
        icon: <CloseCircleOutlined  style={{ color: 'red' }} />,
      }); 
      setTimeout(() => {
        this.setState({
         dvisible: false,
          confirmLoading: false,
        });
       
      }, 2000);
}

  }
 ///console.log(props);
  render()
  {
    try{
    const{fields}=this.state;
    let {subtype}=this.props.location.state;
    let columns = [
       
            {
                // company: 'Company Name',
                // Adminname: 'company',
                key: 'Label',
                title: 'Label',
                dataIndex: 'Label'
                },
        {
        // company: 'Company Name',
        // Adminname: 'company',
        key: 'URL',
        title: 'URL',
        dataIndex: 'URL'
        },
        {
            // company: 'Company Name',
            // Adminname: 'company',
            key: 'Description',
            title: 'Description',
            dataIndex: 'Description'
            },
            ,
        {
            // company: 'Company Name',
            // Adminname: 'company',
            key: 'SubscriptionType',
            title: 'Subscription Type',
            dataIndex: 'SubscriptionType'
            }
        
          
              
                    
     
            ];
            
  return (
    <div>
       <Card style={{    borderRadius: "11px"}}>
         
    <h2 className="title gx-mb-4" style={{color: "#1890ff"}}><IntlMessages id={<span>{subtype} > Configure</span>}/>
    
    
    {(localStorage.getItem('user')=='WARD-Super-Admin'||localStorage.getItem('user')=='WARD-Company-Admin')&&(localStorage.getItem('com')!='All' && localStorage.getItem('com'))?<Button style={{float:"right",marginLeft: "-242px"}} onClick={()=>this.addmodal(this)}>+Add CTA Config</Button>:<p></p>}

    
    </h2>
       <div className="gx-d-flex justify-content-center">
  
    </div>
    <div class="ant-row">
    <div class="ant-col ant-col-24" >




<Table  dataSource={this.state.cdata}  columns={columns} pagination={{ defaultPageSize: 10, showSizeChanger: true, pageSizeOptions: ['10', '20', '30']}} size="small"/>


</div>


</div></Card>
<Modal bodyStyle={{ padding: '0' }}
  title=""
  visible={this.state.dvisible}
  onOk={this.handleOk}
  confirmLoading={this.state.confirmLoading}
  onCancel={this.handleCancel}
style={{marginLeft: "24%",marginTop:"-20px", height: "419px"}}  width={800}>
 
 <Card title={<span style={{color: "#1890ff"}} >CTA Form</span>}> 
        <Form autocomplete="off" onSubmit={this.submitForm}> <Row>
     {fields.map(form=>{
       console.log("form",form);
       if(form.input_type=='text')
     {
       return(
       
        <Col span={6} className="colcustom" style={{paddingLeft: "86px",marginLeft: "20px"}}>
        <Form.Item className="fcust" >
        <InputTextField
        name={form.name}
        require={form.required}
        placeholder={form.placeholder}
        key={form.placeholder}
         disabled={form.disabled}
        _handleChange={this._handleChange}
        />
        </Form.Item>
        </Col>



       )
     }
     if(form.input_type=='BigText')
     {
       return(
       
        <Col span={12} className="colcustom" style={{    marginLeft: "88px"}}>
        <Form.Item className="fcust" >
        <TextareaField
        name={form.name}
        require={form.required}
        placeholder={form.placeholder}
        key={form.placeholder}
        _handleChange={this._handleChange}
        />
        </Form.Item>
        </Col>



       )
     }
     if(form.input_type=='urltext')
     {
       return(
       
        <Col span={6} className="colcustom" style={{    marginLeft: "88px"}}>
        <Form.Item className="fcust" >
        <UrlTextField
        name={form.name}
        require={form.required}
        placeholder={form.placeholder}
        key={form.placeholder}
        _handleChange={this._handleChange}
        />
        </Form.Item>
        </Col>



       )
     }
     if(form.input_type=='dropdown')
     {
       return(
       
        <Col span={8} className="colcustom">
        <Form.Item className="fcust" >
        <Dropdown
        name={form.name}
        require={form.required}
        placeholder={form.placeholder}
        val={form.values}
        key={form.placeholder}
        _handleChange={this._handleChange}
        />
        </Form.Item>
        </Col>



       )
     }
     if(form.input_type=='date')
     {
       return(
       
        <Col span={8} className="colcustom">
        <Form.Item className="fcust" >
        <Datepicker
        name={form.name}
        require={form.required}
        placeholder={form.placeholder}
        key={form.placeholder}
        _handleChange={this._handleDateChange}
        />
        </Form.Item>
        </Col>



       )
     }

     })}
  
     </Row>
</Form>



</Card>
      </Modal>
</div>
  
  
  
   
  );
    }
    catch(ex)
    {
      localStorage.setItem("dkey",'1');
      localStorage.setItem("skey",'1122');
      window.location.href = '/dashboard';
    }
};
}


export default CTAPage;
