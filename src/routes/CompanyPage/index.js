import React ,{ Component } from 'react';
import {Table,Switch} from "antd";
import IntlMessages from "util/IntlMessages";

import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import "./company.css";
import { DatePicker } from 'antd';


import axios from 'axios';
import { Upload,Spin} from 'antd';
import reactCSS from 'reactcss'
import { SketchPicker,SliderPicker,PhotoshopPicker,HuePicker,TwitterPicker } from 'react-color';

import { Layout, Menu,Dropdown,Card,Tabs,Row,Col,Divider,Input,Badge,Modal } from 'antd';
import { Tag } from 'antd';
import { Button,message } from 'antd';
import {  Breadcrumb } from 'antd';
import { Avatar } from 'antd';

import { UsrOutlined, LaptopOutlined, NotificationOutlined } from '@ant-design/icons';
import {
  MenuUnfoldOutlined,DeleteOutlined,EditOutlined,
  MenuFoldOutlined,user,PlusCircleFilled,ExclamationCircleFilled,CheckCircleFilled,IssuesCloseOutlined,
  UserOutlined,LogoutOutlined,PlusCircleOutlined,
  VideoCameraOutlined,ProfileOutlined,SettingOutlined,RedoOutlined,HourglassOutlined,
  UploadOutlined,HomeOutlined,FieldTimeOutlined,LineChartOutlined,FileSyncOutlined
} from '@ant-design/icons';
import {
  DesktopOutlined,EyeOutlined,
  PieChartOutlined,SmileOutlined,CloseCircleOutlined,CheckCircleOutlined,PlusOutlined,
  FileOutlined,
  TeamOutlined,
 
} from '@ant-design/icons';
import { Select } from 'antd';
import { Form, InputNumber,notification } from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import {AutoComplete, Cascader, Checkbox,  Icon,   Tooltip} from "antd";

const FormItem = Form.Item;
const Option = Select.Option;
const AutoCompleteOption = AutoComplete.Option;
const formItemLayout = {
    labelCol: {
      xs: {span: 24},
      sm: {span: 8},
    },
    wrapperCol: {
      xs: {span: 24},
      sm: {span: 16},
    },
  };
  const tailFormItemLayout = {
    wrapperCol: {
      xs: {
        span: 24,
        offset: 0,
      },
      sm: {
        span: 16,
        offset: 8,
      },
    },
  };
  const formItemLayoutsec = {
    labelCol: {
      xs: {span: 14},
      sm: {span: 8},
    },
    wrapperCol: {
      xs: {span: 14},
      sm: {span: 16},
    },
  };
class CompanyPage  extends Component {
    constructor(props) {
		super(props);
    this.state = {
      collapsed: false,
      company:['ALL'],
      domain:'',
      companyname:'',
      companydescription:'',
      cwebsite:'',
      editcomid:'',
      Admininfo:[],count:0,
      displayColorPicker: false,
      displayColorPicker1: false,
      displayColorPicker2: false,
      displayColorPicker3:false,
      pcolor11:"#145fa7",
      pcolor12:"#3eb8c2",
      scolor11:"#2a9966",
      scolor12:"#f97171",
      editflag:false,
      isEnabled:'',
      editrecordId:'',
      dload:false,
    color: {
      r: '241',
      g: '112',
      b: '19',
      a: '1',
    },
    pcolor1:{
      r: '241',
      g: '112',
      b: '19',
      a: '1',
    },
    scolor1:{
      r: '241',
      g: '112',
      b: '19',
      a: '1',
    },
    scolor2:{
      r: '241',
      g: '112',
      b: '19',
      a: '1',
    },


    searchflag:false,
    searchinfo:[],
    lan:[
      "bg_BG",
"ca_ES",
"zh_TW",
"cs_CZ",
"nl_BE",
"nl_NL",
"en_GB",
"en_US",
"et_EE",
"fi_FI",
"fr_BE",
"fr_FR",
"de_DE",
"el_GR",
"it_IT",
"ja_JP",
"ko_KR",
"nb_NO",
"fa_IR",
"pl_PL",
"pt_BR",
"pt_PT",
"ru_RU",
"sr_RS",
"sk_SK",
"es_ES",
"sv_SE",
"tr_TR",
"vi_VN",
"th_TH"
    ],
    ModalText: 'Content of the modal',
    visible: false,
    confirmLoading: false,
    };
  }
  async componentDidMount() {
    var company=this.state.company;
    var ainfo=[];
    var comid=localStorage.getItem('com');
   // console.log("comid"+comid);
    if(comid)
    {
      if(comid!='All')
      {
      console.log("comid"+comid);
    var res= await axios.get(`https://yi70tn8ms6.execute-api.us-east-1.amazonaws.com/dev/api/v1/company/${comid}`)
   ainfo.push(res.data.Item);
    var r=company.includes(res.data.Item.Name);
    if(!r)
    {
      company.push(res.data.Item.Name);
      
    }
  }
  else{
    var res= await axios.get(`https://yi70tn8ms6.execute-api.us-east-1.amazonaws.com/dev/api/v1/company/`)
 
    res.data.Items.forEach(function(obj) {
    
      console.log("datafrom Api",obj);
     ainfo.push(obj);
      var r=company.includes(obj.Name);
      if(!r)
      {
        company.push(obj.Name);
        
      }
   
    
    });
  }
    }

    
    

    console.log("herecomp"+company);
    this.setState({company:company})
    this.setState({
        Admininfo: ainfo
      })
    console.log("this.compa"+this.state.company);

  }
  handleChange = event => {
    this.setState({
          [event.target.id]: event.target.value
      });
};
sel=event=>{
 
    this.setCompany(event)
    }
  addmodal()
  {
   // this.setState({dload:true})
    //alert("here");
    this.setState({editflag:false});
    this.setState({isEnabled:''})
    this.setState({companyname:''})
    // LogoPath: "//logos//CS.jpg",
    this.setState({cwebsite:''});
     this.setState({validtill:''});
     this.setState({companydescription:''});
    // LanguageCode:'en_us',
    // EncryptionKey:"EncryptionKey",
      this.setState({domain:''});
      this.setState({pcolor11:'#088da5'})
      this.setState({pcolor12:'#088da5'});
     this.setState({scolor11:'#088da5'});
     this.setState({scolor12:'#088da5'});
      this.setState({Subscription:''});
      //var x=this.hexToRgb(res.data.Item.PrimaryColor1)
  
      this.setState({color:this.hexToRgb('#088da5')});
      this.setState({pcolor1:this.hexToRgb('#088da5')});
      this.setState({scolor1:this.hexToRgb('#088da5')});
      this.setState({scolor2:this.hexToRgb('#088da5')});
     // this.setState({editcomid:res.data.Item.CompanyId});

    this.setState({visible:true});
    
  }
  handleClick = () => {
    this.setState({ displayColorPicker: !this.state.displayColorPicker })
  };
  handleClick1 = () => {
    this.setState({ displayColorPicker1: !this.state.displayColorPicker1 })
  };
  handleClick2 = () => {
    this.setState({ displayColorPicker2: !this.state.displayColorPicker2 })
  };
  handleClick3 = () => {
    this.setState({ displayColorPicker3: !this.state.displayColorPicker3 })
  };
  

  handleClose = () => {
    this.setState({ displayColorPicker: false })
  };
  handleClose1 = () => {
    this.setState({ displayColorPicker1: false })
  };

  handleClose2 = () => {
    this.setState({ displayColorPicker2: false })
  };

  handleClose3 = () => {
    this.setState({ displayColorPicker3: false })
  };


  handleChangecolor = (color) => {
    this.setState({ color: color.rgb })
    console.log("color",color.rgb);
    this.setState({ pcolor11: color.hex })
  };
  handleChangecolor1 = (color) => {
    this.setState({ pcolor1: color.rgb })
    this.setState({ pcolor12: color.hex })
  };
  handleChangecolor2 = (color) => {
    this.setState({ scolor1: color.rgb })
    this.setState({ scolor11: color.hex })
  };
  handleChangecolor3 = (color) => {
    this.setState({ scolor2: color.rgb })
    this.setState({ scolor12: color.hex })
  };
  registerlan=event=>{
    // console.log("selected lan"+event)
    this.setState({selectedlan:event})
   }
   seldate=event=>{
     console.log("Datesel"+JSON.stringify(event));

     var seldt=new Date(event);
     var today=new Date();
     console.log("date "+seldt+"-------"+today);
     if(seldt<today)
     {
      message.error("Past date selected");
     }
     else{
     this.setState({validtill:JSON.stringify(event)})
     }
     //console.log("Datesel"+this.state.validtill);
   }
   selenabled=event=>{
    
     this.setState({isEnabled:event})
    }
    selsubs=event=>{
      this.setState({Subscription:event})
    }
  handleOk = () => {
    this.setState({
      ModalText: 'The modal will be closed after two seconds',
      confirmLoading: true,
    });
    setTimeout(() => {
      this.setState({
        visible: false,
        confirmLoading: false,
      });
    }, 2000);
  };
  
  handleCancel = () => {
    console.log('Clicked cancel button');
    this.setState({
      visible: false,
    });
  };

  submit= async() =>
{
   
    var x=this.state.company;
    console.log(x);
    var com=this.state.companyname;
if(this.state.editflag)
    {
      var params = {
        IsEnabled: this.state.isEnabled,
        Name: this.state.companyname.toUpperCase(),
        LogoPath: "//logos//CS.jpg",
        Website: this.state.cwebsite,
        ValidUntil: this.state.validtill,
        Note: this.state.companydescription,
        LanguageCode:'en_us',
        EncryptionKey:"EncryptionKey",
        Domain: this.state.domain,
        PrimaryColor1: this.state.pcolor11,
        PrimaryColor2: this.state.pcolor12,
        SecondaryColor1: this.state.scolor11,
        SecondaryColor2: this.state.scolor12,
        Subscription: this.state.Subscription,
        CompanyId: this.state.editcomid
      }
      console.log('putparams',params)
      var res=await axios.put('https://yi70tn8ms6.execute-api.us-east-1.amazonaws.com/dev/api/v1/company', params);
      console.log('putres',res)

      notification.open({
        message: 'Alert',
        description:
          'Company Updated',
        icon: <CheckCircleOutlined  style={{ color: '#228B22' }} />,
      });
    
    setTimeout(() => {
      this.setState({
        visible: false,
        confirmLoading: false,
      });
     
    }, 2000);

    this.reloaddata();
     }
     

    else if(com && this.state.domain)
    {
    var found=x.includes(this.state.companyname);
   if(!found)
   {
    x.push(this.state.companyname);
    notification.open({
      message: 'Alert',
      description:
        'New Company '+this.state.companyname+' added successfully',
      icon: <CheckCircleOutlined  style={{ color: '#228B22' }} />,
    });
  
    this.setState({company:x});
    const { Admininfo} = this.state;
    const newData = {Name:this.state.companyname,cdomain:this.state.domain,companydescription:this.state.companydescription,cwebsite:this.state.cwebsite,themeColor:this.state.color};
    console.log("adassdsd",newData)
    
  
   
    var params = {
      IsEnabled: this.state.isEnabled,
      Name: this.state.companyname.toUpperCase(),
      LogoPath: "//logos//CS.jpg",
      Website: this.state.cwebsite,
      ValidUntil: this.state.validtill,
      Note: this.state.companydescription,
      LanguageCode:'en_us',
      EncryptionKey:"EncryptionKey",
      Domain: this.state.domain,
      PrimaryColor1: this.state.pcolor11,
      PrimaryColor2: this.state.pcolor12,
      SecondaryColor1: this.state.scolor11,
      SecondaryColor2: this.state.scolor12,
      Subscription: this.state.Subscription,
      CreatedBy:localStorage.getItem('userEmail'),
      CreatedOn:new Date("YYYY-mm-ddTHH:MM:ssZ") ,
      CompanyId: "12"
    }
var res=await axios.post('https://yi70tn8ms6.execute-api.us-east-1.amazonaws.com/dev/api/v1/company', params);

    //var found=getKeyByValue(exampleObject, 100); 
 console.log("response",res.data.Item);

 //localStorage.setItem('com',res.data.Item.CompanyId);
   this.setState({
     // Admininfo: [...Admininfo, newData]
    Admininfo: [res.data.Item]
    })
    
    }
    else
    {
      notification.open({
        message: 'Alert',
        description:
          'Company Already Exists',
        icon: <CloseCircleOutlined style={{ color: '#FF0000' }} />,
      });
    }
    setTimeout(() => {
      this.setState({
        visible: false,
        confirmLoading: false,
      });
     
    }, 2000);
}
else{
    notification.open({
        message: 'Alert',
        description:
          'Required field are missing please review',
        icon: <CloseCircleOutlined style={{ color: '#FF0000' }} />,
      });
}


 
 
}

editCompany=async (id)=>{
  this.setState({visible:true});
  this.setState({editflag:true});
  this.setState({editrecordId:id})
  this.setState({dload:true})
console.log("edit",this.state.editrecordId);
var res= await axios.get(`https://yi70tn8ms6.execute-api.us-east-1.amazonaws.com/dev/api/v1/company/${id}`)
   //ainfo.push(res.data.Item);
   //this.setState({dload:true})
   if(res)
   {

    setTimeout(()=>{
      this.setState({dload:false})
    },2000)

   }
   console.log("eee",res.data.Item);
  this.setState({isEnabled:res.data.Item.IsEnabled})
  this.setState({companyname:res.data.Item.Name})
  // LogoPath: "//logos//CS.jpg",
  this.setState({cwebsite:res.data.Item.Website});
   this.setState({validtill:res.data.Item.ValidUntil});
   this.setState({companydescription:res.data.Item.Note});
  // LanguageCode:'en_us',
  // EncryptionKey:"EncryptionKey",
    this.setState({domain:res.data.Item.Domain});
    this.setState({pcolor11:res.data.Item.PrimaryColor1})
    this.setState({pcolor12:res.data.Item.PrimaryColor2});
   this.setState({scolor11:res.data.Item.SecondaryColor1});
   this.setState({scolor12:res.data.Item.SecondaryColor2});
    this.setState({Subscription:res.data.Item.Subscription});
    //var x=this.hexToRgb(res.data.Item.PrimaryColor1)

    this.setState({color:this.hexToRgb(res.data.Item.PrimaryColor1)});
    this.setState({pcolor1:this.hexToRgb(res.data.Item.PrimaryColor2)});
    this.setState({scolor1:this.hexToRgb(res.data.Item.SecondaryColor1)});
    this.setState({scolor2:this.hexToRgb(res.data.Item.SecondaryColor2)});
    this.setState({editcomid:res.data.Item.CompanyId});
//   CreatedBy:localStorage.getItem('userEmail'),
//   CreatedOn:new Date("YYYY-mm-ddTHH:MM:ssZ") ,
    
console.log("c1",this.hexToRgb(res.data.Item.PrimaryColor1))

}
 hexToRgb=(hex)=>{
  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return result ? {
    r: parseInt(result[1], 16),
    g: parseInt(result[2], 16),
    b: parseInt(result[3], 16),
    a: parseInt(1)
  } : null;
}
deleterec=async (comid)=>{
    //alert("to be deleted"+comid);
    
    var res=await axios.delete("https://yi70tn8ms6.execute-api.us-east-1.amazonaws.com/dev/api/v1/company/"+comid);
    if(res.data)
    {
    console.log("deleted"+JSON.stringify(res));
    notification.open({
      message: 'Alert',
      description:
        'Company deleted successfully',
      icon: <DeleteOutlined   style={{ color: '#0d4f8c' }} />,
    });
  
    this.reloaddata();
    }
    
  
    
    }
    reloaddata=async ()=>
    {
      var x=['ALL'];
    var ainfo=[];
    this.setState({company:x})
    var company=this.state.company;
    var res= await axios.get(`https://yi70tn8ms6.execute-api.us-east-1.amazonaws.com/dev/api/v1/company`)
    //console.log("datafrom Api",res.data);
    res.data.Items.forEach(function(obj) {
      
      console.log("datafrom Api",obj);
     ainfo.push(obj);
     
        company.push(obj.Name);
        
     
    
    });
    
    console.log("herecomp"+company);
    this.setState({company:company})
    this.setState({
        Admininfo: ainfo
      })
     // this.setState({searchflag:false})
      
    console.log("this.compa"+this.state.company);
      
    }
    setCompany = async value => {
        console.log("setcomp",value)  
        //alert(value);
this.setState({companyname:value})
var searcharray=this.search(this.state.Admininfo,value);
console.log("search Array"+JSON.stringify(this.state.Admininfo)+"value"+value);
console.log("searcharray"+JSON.stringify(searcharray));
this.setState({
    searchinfo: searcharray
  })
/*this.setState(prevState => ({
    searchflag: !prevState.searchflag
  }));*/
  if(value=='ALL')
  {
     // alert("here");
      this.setState({searchflag:false})
  }
  else{
    this.setState({searchflag:true})
  }
    }
    search(source, nam){
        var results;
        //console.log("setcompcall",nam)  
        //console.log("res"+JSON.stringify(source));
        nam = nam.toUpperCase();
        results = source.filter(function(entry) {
            return entry.Name.toUpperCase().indexOf(nam) !== -1;
        });
    
        console.log("res"+JSON.stringify(results));
        return results;
    }
    alert=()=>{
      message.error("You don't have access to perform this action")
    }
render()
{
    const { TabPane } = Tabs;
    const { Option } = Select;
    const styles = reactCSS({
        'default': {
          color: {
            width: '36px',
            height: '14px',
            borderRadius: '2px',
            background: `rgba(${ this.state.color.r }, ${ this.state.color.g }, ${ this.state.color.b }, ${ this.state.color.a })`,
          },
          swatch: {
            padding: '5px',
            background: '#fff',
            borderRadius: '1px',
            boxShadow: '0 0 0 1px rgba(0,0,0,.1)',
            display: 'inline-block',
            cursor: 'pointer',
          },
          popover: {
            position: 'absolute',
            zIndex: '2',
          },
          cover: {
            position: 'fixed',
            top: '0px',
            right: '0px',
            bottom: '0px',
            left: '0px',
          },
        },
      });
      const styles1 = reactCSS({
        'default': {
          color: {
            width: '36px',
            height: '14px',
            borderRadius: '2px',
            background: `rgba(${ this.state.pcolor1.r }, ${ this.state.pcolor1.g }, ${ this.state.pcolor1.b }, ${ this.state.pcolor1.a })`,
          },
          swatch: {
            padding: '5px',
            background: '#fff',
            borderRadius: '1px',
            boxShadow: '0 0 0 1px rgba(0,0,0,.1)',
            display: 'inline-block',
            cursor: 'pointer',
          },
          popover: {
            position: 'absolute',
            zIndex: '2',
          },
          cover: {
            position: 'fixed',
            top: '0px',
            right: '0px',
            bottom: '0px',
            left: '0px',
          },
        },
      });
      const styles2 = reactCSS({
        'default': {
          color: {
            width: '36px',
            height: '14px',
            borderRadius: '2px',
            background: `rgba(${ this.state.scolor1.r }, ${ this.state.scolor1.g }, ${ this.state.scolor1.b }, ${ this.state.scolor1.a })`,
          },
          swatch: {
            padding: '5px',
            background: '#fff',
            borderRadius: '1px',
            boxShadow: '0 0 0 1px rgba(0,0,0,.1)',
            display: 'inline-block',
            cursor: 'pointer',
          },
          popover: {
            position: 'absolute',
            zIndex: '2',
          },
          cover: {
            position: 'fixed',
            top: '0px',
            right: '0px',
            bottom: '0px',
            left: '0px',
          },
        },
      });
      const styles3 = reactCSS({
        'default': {
          color: {
            width: '36px',
            height: '14px',
            borderRadius: '2px',
            background: `rgba(${ this.state.scolor2.r }, ${ this.state.scolor2.g }, ${ this.state.scolor2.b }, ${ this.state.scolor2.a })`,
          },
          swatch: {
            padding: '5px',
            background: '#fff',
            borderRadius: '1px',
            boxShadow: '0 0 0 1px rgba(0,0,0,.1)',
            display: 'inline-block',
            cursor: 'pointer',
          },
          popover: {
            position: 'absolute',
            zIndex: '2',
          },
          cover: {
            position: 'fixed',
            top: '0px',
            right: '0px',
            bottom: '0px',
            left: '0px',
          },
        },
      });
    const layout = {
        labelCol: { span: 8 },
        wrapperCol: { span: 16 },
        };
    
        let columns = [
           
            {
            // company: 'Company Name',
            // Adminname: 'company',
            key: 'Name',
            title: 'Name',
            dataIndex: 'Name'
            },
            {
              // company: 'Company Name',
              // Adminname: 'company',
              key: 'IsEnabled',
              title: 'Status',
              render: (text, record) => (
                  <span>
                   
                  {record.IsEnabled=='Y'?<CheckCircleOutlined  style={{fontSize: "23px",color: "#52c41a"}} />:<IssuesCloseOutlined  style={{fontSize: "23px",color: "#f5222d"}} />}
                  </span>
                ),
              },
            {
                // company: 'Company Name',
                // Adminname: 'company',
                key: 'Subscription',
                title: 'Subscription',
                render: (text, record) => (
                  <span>
                   <p>{record.Subscription}</p>
                   </span>
                ),
                },
                {
                    // company: 'Company Name',
                    // Adminname: 'company',
                    key: 'Note',
                    title: 'Note',
                    dataIndex: 'Note'
                    },
                    {
                      title: 'Action',
                      key: 'action',
                      render: (text, record) => (
                        <span>
                         
                         {localStorage.getItem('user')=='WARD-Super-Admin'? <Button onClick={()=>this.editCompany(record.CompanyId)}><EditOutlined /></Button>:<Button onClick={()=>this.alert()}><DeleteOutlined /></Button>}
                        </span>
                      ),
                    }
                ];
  return (
    <div>
      <h2 className="title gx-mb-4" style={{color: "#1890ff"}}><IntlMessages id="Manage Company"/></h2>

      <div className="gx-d-flex justify-content-center">
    
      </div>
      <div class="ant-row">
      <div class="ant-col ant-col-24" >




<Card title={<span style={{color:"#1890ff"}}>
  <Form.Item label=""   {...formItemLayoutsec} style={{marginLeft:"10px"}}>

  </Form.Item>{(localStorage.getItem('user')=='WARD-Super-Admin'||localStorage.getItem('user')=='WARD-Company-Admin')&&localStorage.getItem('com')=='All'?<Button style={{float:"right",marginTop: "-26px"}} onClick={()=>this.addmodal(this)}>+Add Company</Button>:<p></p>}</span>}>

{this.state.searchflag?<Table className="gx-table-responsive" dataSource={this.state.searchinfo} columns={columns} style={{fontSize: "12px"}} size="small"/>:<Table className="gx-table-responsive" dataSource={this.state.Admininfo} columns={columns} style={{fontSize: "12px"}} size="small"/>}


</Card>
</div>


  </div>
  <Modal bodyStyle={{ padding: '0' }}
          title=""
          visible={this.state.visible}
          onOk={this.submit}
          confirmLoading={this.state.confirmLoading}
          onCancel={this.handleCancel}
        style={{marginLeft: "24%",marginTop:"-20px"}}  width={900}>
         
          {this.state.dload? <div>
         
         <Card className="gx-card" title="Company Registration Form">
           
         <Spin  style={{marginLeft: "404px",
    marginTop: "49px"}}/> 
           </Card></div>:<div>
         
          <Card className="gx-card" title="Company Registration Form">
        <Form autocomplete="off" onSubmit={this.handleSubmit}>
        <Row>
        <Col span={12} className="colcustom">
        <Form.Item label="Name *"  {...formItemLayout}>
      
{this.state.editflag?
<Input placeholder="company name" id="companyname" value={this.state.companyname} onChange={this.handleChange} disabled/>:
<Input placeholder="company name" id="companyname" value={this.state.companyname} onChange={this.handleChange} required/>}
</Form.Item>
</Col>
<Col span={12} className="colcustom">
<Form.Item label="Domain *"  {...formItemLayout}>
<Input placeholder="Domain" id="domain" value={this.state.domain} onChange={this.handleChange} required/>
</Form.Item>
</Col>

<Col span={12} className="colcustom">
<Form.Item label="Valid Until *" {...formItemLayout} >
<DatePicker placeholder={this.state.validtill} onChange={this.seldate} style={{width:"245px"}}/>
</Form.Item></Col>
<Col span={12} className="colcustom">
<Form.Item label="Enabled *"  {...formItemLayout}>
<Select defaultValue="Y" placeholder={this.state.isEnabled}
    onChange={this.selenabled}
    style={{ width: 245 }}>
     
  <option value="Y" selected="selected">YES</option>
  <option value="N">NO</option>
</Select>
</Form.Item></Col>
<Col span={12} className="colcustom">
<Form.Item label="Subscription *" {...formItemLayout}>
<Select defaultValue="RENEWAL" placeholder={this.state.Subscription}
    onChange={this.selsubs}
    style={{ width: 245 }}>
  <option value="RENEWAL" >RENEWAL</option>
  <option value="TRIAL">TRIAL</option>
  <option value="ONBOARDING">ONBOARDING</option>
  <option value="FNOL">FNOL</option>
 
</Select>
</Form.Item></Col>
<Col span={12} className="colcustom">
<Form.Item label="Website"  {...formItemLayout}>
<Input placeholder="website" id="cwebsite" value={this.state.cwebsite} onChange={this.handleChange}/>
</Form.Item></Col>
<Col span={12} className="colcustom">
<Form.Item label="Description"  {...formItemLayout}>
<TextArea placeholder="Description" id="companydescription" value={this.state.companydescription} onChange={this.handleChange}/>
</Form.Item></Col>
<Col span={12} className="colcustom">

<Form.Item label="MFA Settings"  {...formItemLayout}>
<Switch disabled="true" style={{ opacity: "0.5"}} defaultUnChecked />
</Form.Item></Col>

<Col span={12} className="colcustom">
          <Form.Item label="Primary Color #1" {...formItemLayout}>

<div style={{position: 'relative'}}>
<div style={ styles.swatch } onClick={ this.handleClick }>
<div style={ styles.color } />
</div>
{ this.state.displayColorPicker ? <div style={ styles.popover}>
<div style={ styles.cover } onClick={ this.handleClose }/>
<TwitterPicker  color={ this.state.color } onChange={ this.handleChangecolor } />
</div> : null }

</div>
</Form.Item></Col><Col span={12} className="colcustom">

          <Form.Item label="Primary Color #2" {...formItemLayout}>

<div style={{position: 'relative'}}>
<div style={ styles1.swatch } onClick={ this.handleClick1 }>
<div style={ styles1.color } />
</div>
{ this.state.displayColorPicker1 ? <div style={ styles1.popover}>
<div style={ styles1.cover } onClick={ this.handleClose1 }/>
<TwitterPicker  color={ this.state.pcolor1 } onChange={ this.handleChangecolor1 } />
</div> : null }

</div>
</Form.Item></Col><Col span={12} className="colcustom">  <Form.Item label="Secondary Color #1" {...formItemLayout}>

<div style={{position: 'relative'}}>
<div style={ styles2.swatch } onClick={ this.handleClick2 }>
<div style={ styles2.color } />
</div>
{ this.state.displayColorPicker2 ? <div style={ styles2.popover}>
<div style={ styles2.cover } onClick={ this.handleClose2 }/>
<TwitterPicker  color={ this.state.scolor1 } onChange={ this.handleChangecolor2 } />
</div> : null }

</div>
</Form.Item></Col><Col span={12} className="colcustom"> <Form.Item label="Secondary Color #2" {...formItemLayout}>

<div style={{position: 'relative'}}>
<div style={ styles3.swatch } onClick={ this.handleClick3 }>
<div style={ styles3.color } />
</div>
{ this.state.displayColorPicker3 ? <div style={ styles3.popover}>
<div style={ styles3.cover } onClick={ this.handleClose3 }/>
<TwitterPicker  color={ this.state.scolor2 } onChange={ this.handleChangecolor3 } />
</div> : null }

</div>
</Form.Item></Col>
<Col span={12} className="colcustom">

<FormItem
            {...formItemLayout}
            label="Upload Logo"
            extra=""
          >
           
              <Upload name="logo" action="/upload.do" listType="picture">
                <Button>
                  <Icon type="upload"/> Click to upload
                </Button>
              </Upload>
           
          </FormItem></Col>
</Row>
</Form>
          </Card>
        
          </div>}
          </Modal>
    </div>
  );
}
};

export default CompanyPage;
