import React ,{ Component } from 'react';
import {Table,Popover} from "antd";
import IntlMessages from "util/IntlMessages";
import Role from "./Role";
import './user.css';
import apiCall from '../../apiutil/apicall';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Company from "./Company";
import { DatePicker,Switch,message } from 'antd';
import validator from '../../validators/validator';
import axios from 'axios';
import { Upload } from 'antd';
import reactCSS from 'reactcss'
import { SketchPicker,SliderPicker,PhotoshopPicker,HuePicker,TwitterPicker } from 'react-color';

import { Layout, Menu,Dropdown,Card,Tabs,Row,Col,Divider,Input,Badge,Modal } from 'antd';
import { Tag } from 'antd';
import { Button } from 'antd';
import {  Breadcrumb } from 'antd';
import { Avatar } from 'antd';

import { UsrOutlined, LaptopOutlined, NotificationOutlined } from '@ant-design/icons';
import {
  MenuUnfoldOutlined,DeleteOutlined,CheckCircleFilled,CloseCircleFilled,FormOutlined,SearchOutlined,UnlockOutlined,
  MenuFoldOutlined,user,PlusCircleFilled,PhoneOutlined,ExclamationCircleFilled,IssuesCloseOutlined,
  UserOutlined,LogoutOutlined,PlusCircleOutlined,CheckOutlined,MailOutlined,CloseOutlined,
  VideoCameraOutlined,ProfileOutlined,SettingOutlined,RedoOutlined,HourglassOutlined,
  UploadOutlined,HomeOutlined,FieldTimeOutlined,LineChartOutlined,FileSyncOutlined
} from '@ant-design/icons';
import {
  DesktopOutlined,EyeOutlined,
  PieChartOutlined,SmileOutlined,CloseCircleOutlined,CheckCircleOutlined,PlusOutlined,
  FileOutlined,
  TeamOutlined,
 
} from '@ant-design/icons';
import Amplify, { Auth } from 'aws-amplify';
import { Select } from 'antd';
import { Form, InputNumber,notification } from 'antd';
import TextArea from 'antd/lib/input/TextArea';
import {AutoComplete, Cascader, Checkbox,  Icon,   Tooltip} from "antd";
import moment from "moment";
import apicall from '../../apiutil/apicall';
const FormItem = Form.Item;
const Option = Select.Option;
const AutoCompleteOption = AutoComplete.Option;
const formItemLayout = {
    labelCol: {
      xs: {span: 24},
      sm: {span: 8},
    },
    wrapperCol: {
      xs: {span: 24},
      sm: {span: 16},
    },
  };
  const formItemLayoutsec = {
    labelCol: {
      xs: {span: 14},
      sm: {span: 8},
    },
    wrapperCol: {
      xs: {span: 14},
      sm: {span: 16},
    },
  };
  const tailFormItemLayout = {
    wrapperCol: {
      xs: {
        span: 24,
        offset: 0,
      },
      sm: {
        span: 16,
        offset: 8,
      },
    },
  };
class UserPage  extends Component {
    constructor(props) {
		super(props);
    this.state = {
      collapsed: false,
      company:['ALL'],
      collapsed:true,
      logout: false,
      loading: false,
      rolename:'',
      ModalText: 'Content of the modal',
      visible: false,
      dvisible:false,
      confirmLoading: false,
      getdata:false,
      EmailAddress: "",
      usersearch:'',
      Nerror:'',
      ac:true,
Expires: moment(new Date().getFullYear()+'-12-31').format("YYYY-MM-DD"),
oExpires:"",
CreatedOn: "",
PhoneNum: null,
IsEnabled: "Y",
MiddleName: "",
RoleId: "",
UserId: "",
CompanyId: "",
companyname:'',
LastCompanyId: "16e4eaaf-2701-42f2-ab7b-53ec9606fe94",
DefaultCompanyId: "16e4eaaf-2701-42f2-ab7b-53ec9606fe94",
PrimaryCompanyId: "16e4eaaf-2701-42f2-ab7b-53ec9606fe94",
CreatedBy: "",
FirstName: "",
LastName: "",
RoleName:"",
roleid:[],
companyid:[],
accessKeyId:'',
userdata:[],
userinitdata:[],
ccode:[],
pcode:'',
flag:false,
eflag:false,
IsMFAEnabled:'',
efl:0,
efl2:1

      //userdata:[]
    };
  }
  async componentDidMount() {
   if(localStorage.getItem('com'))
   {
    this.showloader(true);
   }
    var x=[];
    console.log('localcom',localStorage.getItem('com'));
    //this.setState({userdata:x});
    var ccode=[];
   // console.log("uhere",this.props.location.state.ssprops());
   var res1= await axios.get(`https://gist.githubusercontent.com/Goles/3196253/raw/9ca4e7e62ea5ad935bb3580dc0a07d9df033b451/CountryCodes.json`)
  res1.data.forEach(function(obj)
  {
   ccode.push(obj);
  });
  console.log("ccode",ccode);
this.setState({ccode});
var companyId=localStorage.getItem('com');
try{
  var res;
  if(companyId=='All')
  {
    res= await apiCall.getApi(`user`)
  }
  else{
    res= await apiCall.getApi(`user/company/${companyId}`)
  }
        //console.log("datafrom Api",res);
        
        res.data.Items.forEach(function(obj) {
          console.log(obj.CompanyId+'----match----'+companyId);
         // if(obj.CompanyId==companyId)
         // {
           // alert("match");
          console.log("datafrom2 Api",obj);
          x.push(obj);
        //  }
        //this.setState({userinfo:obj});
       // this.setState({getdata:false})
        
        });
    if(x)
    {
      setTimeout(() => {
        this.showloader(false);
       
      }, 1000);
    }
    
        this.setState({
            userdata: x
          })
          this.setState({
            userinitdata: x
          })
    console.log("userinfo",this.state.userdata);
        }
        catch(ex)
        {
        console.log("error"+ex);
        }
 try
 {
//this.setState({visible:this.props.mvisible.visible});
   var x=[]; 
   var x2=[]; 
var res= await apiCall.getApi(`role`)
//console.log("datafrom Api",res);
res.data.Items.forEach(function(obj) {
  
  console.log("datafrom2 Api",obj);
  
  x.push(obj);

//this.setState({userinfo:obj});
  

});
var companyname='';
var res2= await apiCall.getApi(`company`)
//console.log("datafrom Api",res);
res2.data.Items.forEach(function(obj) {
  if(localStorage.getItem('com')==obj.CompanyId)
  {
companyname=obj.Name;
  }
  console.log("datafrom2 Api",obj);
  x2.push(obj);

//this.setState({userinfo:obj});
  

});
this.setState({companyname});
console.log("value of x",x);
this.setState({
    roleid: x
  })
  console.log("value of x",x);
this.setState({
    companyid: x2
  })
  
  
  console.log("compId",this.state.companyid)
}
catch(ex)
{
  console.log("error"+ex)
}

if(localStorage.getItem('user')=='WARD-Company-Admin')
{
var y=[];
x.forEach(function(obj) 
{
  if(obj.Name!= "WARD-Super-Admin")
  {
    //console.log("HERE",obj);
    y.push(obj);
  }
});
this.setState({roleid:y})
} 

  }

  showloader=(x)=>{
    //alert(x)
    this.setState({getdata:x})
  }
  setphone=event=>{
    if(this.state.flag==false)
    {
var x=this.state.pcode+this.state.PhoneNum;
    this.setState({PhoneNum:x});
    this.setState({flag:true});
    }
  }
  selsubs=event=>{
    console.log("scode",event);
    var pcode=this.state.ccode[event].dial_code;
    console.log('pcode',pcode);
    this.setState({pcode})
  }
  addmodal()
  {
    //alert("here");

    this.setState({ac:true})
    this.setState({UserId:''})
    this.setState({IsEnabled:'Y'})
  this.setState({FirstName:''})
  this.setState({RoleName:''})
  this.setState({LastName:''})
  this.setState({IsMFAEnabled:''})
  this.setState({EmailAddress:''})
  this.setState({PhoneNum:''})
  this.setState({Erole:""})
  this.setState({RoleId:''})
  this.setState({ELname:""})
  this.setState({Nerror:""})
  this.setState({Eemail:""})
 // this.setState({PhoneNum:''})
 // this.setState({Expires:''})
 // this.setState({IsEnabled:''})
 // this.setState({RoleId:''})
 // this.setState({RoleName:''})
 // this.setState({RoleDisplayName:''})
 // this.setState({CompanyId:''})
  //this.setState({CompanyName:''})
    this.setState({editFlag:false});
this.setState({dFlag:true})
    this.setState({dvisible:true});
    
  }
  selenabled=event=>{
    //console.log("qevent",event);
    this.state({eflag:false});
    this.setState({IsEnabled:event})
   }
   seldate=event=>{
    //  console.log("Datesel"+JSON.stringify(event));
    console.log("Datesel"+JSON.stringify(event));

     var seldt=new Date(event);
     var today=new Date();
     console.log("date "+seldt+"-------"+today);
     if(seldt<today)
     {
      message.error("Invalid date selected");
      this.setState({efl:1})
     }
     else{
     this.setState({Expires:event})
     this.setState({efl:0})
     }
     this.setState({dFlag:false});
     // this.setState({Expires:event})
      //console.log("Datesel"+this.state.validtill);
    }
    onChanges=(checked)=> {
        console.log("s",checked);
        if(checked)
        {
            this.setState({IsEnabled:"Y"})
        }
        else
        {
            this.setState({IsEnabled:"N"})
        }
      }
      onChanges2=(checked)=> {
        console.log("s",checked);
        if(checked)
        {
            this.setState({IsMFAEnabled:"Y"})
        }
        else
        {
            this.setState({IsMFAEnabled:"N"})
        }
      }
    registercompany=event=>{
        // console.log("selected lan"+event)
        this.setState({CompanyId:event})
       }
       registerrole=event=>{
         try{
           if(event)
           {
            this.setState({Erole:""})
            this.setState({efl:0})
          //  this.setState({ac:true})
         console.log("selected role"+event)
        var role=event.split("@");
        console.log("selected role",role)
        this.setState({RoleId:role[0]});
        this.setState({RoleName:role[1]});
           }
           else{
            this.setState({Erole:"Please select role for the user"})
            this.setState({efl:1})
           }
         }
         catch(err)
         {
           console.log(err);
         }
       }
handleOk =async () => {
    this.setState({
      ModalText: 'The modal will be closed after two seconds',
      confirmLoading: true,
    });
  
  

    try{
    //console.log("uparams",params);
    var eflag=0;
    if(this.state.FirstName=='')
    {
      this.setState({Nerror:"Please enter first name."})
            this.setState({efl2:1})
            eflag=1;
    }
     if(this.state.LastName=='')
    {
      this.setState({ELname:"Please enter last name."})
            this.setState({efl2:1})
            eflag=1;
    }
     if(this.state.EmailAddress=='')
    {
      this.setState({Eemail:" Please enter email address."})
            this.setState({efl2:1})
            eflag=1;
    }
    if(this.state.RoleId=='')
    {
      this.setState({Erole:"Please select a user role."})
            this.setState({efl2:1})
            eflag=1;
    }
    if(this.state.RoleId!=''&&this.state.LastName!=''&& this.state.FirstName!='' && this.state.EmailAddress!='')
    {
      //alert("here")
      this.setState({efl2:0})
      this.setState({efl:0})

      var bool=validator.validatelName(this.state.LastName)
      ///console.log("bool",bool);
      var bool2=validator.validatefName(this.state.FirstName)
      if(!bool)
      {
        this.setState({ELname:"Please review last name field"})
       // this.setState({efl:1})
        eflag=1;
      }
      else if(!bool2)
      {
        this.setState({Nerror:"Please review first name field"})
//this.setState({efl:1})
        eflag=1;
      }
      else
      {
        eflag=0;
      }

    //  alert("here "+this.state.efl+ '  '+eflag)
    }
   if(!this.state.editFlag)
  {
   


  var params = {
    "UserId": "",
    "CompanyId":localStorage.getItem('com'),
    "RoleId": this.state.RoleId,
    "FirstName": this.state.FirstName,
    "LastName": this.state.LastName,
    "MddleName": this.state.MiddleName,
    "EmailAddress": this.state.EmailAddress.toLocaleLowerCase(),
    "PhoneNum": this.state.PhoneNum,
    "IsEnabled": this.state.IsEnabled,
    "IsMFAEnabled":this.state.IsMFAEnabled,
    "Expires":this.state.Expires? this.state.Expires:moment(new Date().getFullYear()+'-12-31').format("YYYY-MM-DD"),
    "PrimaryCompanyId": "16e4eaaf-2701-42f2-ab7b-53ec9606fe94",
    "DefaultCompanyId": "16e4eaaf-2701-42f2-ab7b-53ec9606fe94",
    "LastCompanyId": "16e4eaaf-2701-42f2-ab7b-53ec9606fe94",
    "CreatedBy": localStorage.getItem('UserIdinfo'),
    "CreatedOn": new Date(),
    "RoleName":this.state.RoleName
}

console.log("data",params)

if(this.state.efl==0 && eflag==0)
{
var res=await apiCall.postApi('user', params);
console.log("res-u",res);

//alert(res.data.Item)

if(res.data.Item)
{
    setTimeout(() => {
        this.setState({
         dvisible: false,
          confirmLoading: false,
        });
       
      }, 2000);

      notification.open({
        message: 'Alert',
        description:
          `User ${this.state.FirstName} added successfully`,
        icon: <CheckCircleOutlined  style={{ color: '#228B22' }} />,
      });
      this.reloaddata();
  //  this.props.mvisible.rmodal(false);
}

else
{
   //alert("here");

      notification.open({
        message: 'Alert',
        description:
          `${res.message.message}`,
        icon: <CloseCircleOutlined  style={{ color: '#228B22' }} />,
      }); 
      setTimeout(() => {
        this.setState({
         dvisible: false,
          confirmLoading: false,
        });
       
      }, 2000);
}
}
else{
  message.error("Validation error please review.");
  setTimeout(() => {
    this.setState({
      confirmLoading: false,
    });
   
  }, 2000);
}
}
else
{
 

  var params = {
    "UserId": this.state.UserId,
    "CompanyId":localStorage.getItem('com'),
    "CompanyName":this.state.companyname,
    "RoleName":this.state.RoleName,
    "RoleDisplayName":this.state.RoleDisplayName,
    "RoleId": this.state.RoleId,
    "FirstName": this.state.FirstName,
    "LastName": this.state.LastName,
    "MiddleName": this.state.MiddleName,
    "EmailAddress": this.state.EmailAddress.toLocaleLowerCase(),
    "PhoneNum": this.state.PhoneNum,
    "IsEnabled": this.state.IsEnabled,
    "IsMFAEnabled":this.state.IsMFAEnabled,
    "Expires":this.state.Expires?this.state.Expires:moment(new Date().getFullYear()+'-12-31').format("YYYY-MM-DD"),
    "PrimaryCompanyId": "16e4eaaf-2701-42f2-ab7b-53ec9606fe94",
    "DefaultCompanyId": "16e4eaaf-2701-42f2-ab7b-53ec9606fe94",
    "LastCompanyId": "16e4eaaf-2701-42f2-ab7b-53ec9606fe94",
    "CreatedBy": localStorage.getItem('UserIdinfo'),
    "CreatedOn": new Date()
    
     //"RoleName":"WARD-Company-Users"
}
if(this.state.efl==0 && eflag==0)
{
var res=await apiCall.putApi('user',params);
console.log("UserUpdate",res);
if(this.state.IsEnabled=='Y')
{
  var params = {
    "LoggedInUser":localStorage.getItem('UserIdinfo')
  }
//  try{
  var res=await apiCall.putApi(`/user/enable/${this.state.UserId}`,params)

}
else
{
  var params = {
    "LoggedInUser":localStorage.getItem('UserIdinfo')
  }
//  try{
  var res=await apiCall.putApi(`/user/disable/${this.state.UserId}`,params)

}
if(this.state.IsMFAEnabled=='N')
{
  var mfa_params = {
    LoggedInUser: this.state.UserId
  };

 var res_mfa = await apiCall.putApi(
    "user/mfa/disable/" + this.state.UserId,
    mfa_params
  );
}
notification.open({
  message: 'Alert',
  description:
    `User ${this.state.FirstName.toLowerCase()} updated successfully`,
  icon: <CheckCircleOutlined  style={{ color: '#228B22' }} />,
});

setTimeout(() => {
  this.setState({
   dvisible: false,
    confirmLoading: false,
  });
 
}, 2000);

this.reloaddata();

}
else
{
  message.error("Validation error please review.");
  setTimeout(() => {
    this.setState({
 //    dvisible: false,
      confirmLoading: false,
    });
   
  }, 2000);
}




}

}
catch(error)
{
 
  this.setState({UserId:''})
    
  this.setState({FirstName:''})
  this.setState({LastName:''})
  this.setState({EmailAddress:''})
  this.setState({PhoneNum:''})
  this.setState({Expires:''})
  this.setState({IsEnabled:'Y'})
  this.setState({IsMFAEnabled:''})
  this.setState({RoleId:''})
  this.setState({RoleName:''})
  this.setState({RoleDisplayName:''})
  this.setState({CompanyId:''})
  this.setState({CompanyName:''})
  this.setState({ac:true})
  setTimeout(() => {
    this.setState({
  //   dvisible: false,
      confirmLoading: false,
    });
   
  }, 2000);
   console.log('errr',error)
      notification.open({
        message: 'Alert',
        description:"Operation Failed Please Try Again. User Already Exists",
        icon: <CloseCircleOutlined  style={{ color: 'red' }} />,
      }); 
}

  }


  handleChange = event => {
    this.setState({
          [event.target.id]: event.target.value
      });
};
handlesearch = event => {
  
  this.setState({
        [event.target.id]: event.target.value
    });
    var u=this.state.userinitdata;
    var x=event.target.value;
    console.log("usearch"+x)
    if(x!='')
    {
var res=this.filter(u, x)
    this.setState({userdata:res})
    }
    else{
      console.log("ucount"+u.length);
      this.setState({userdata:u})
    }

};

filter(source, nam){
  var results;
try{
  /*

  return x.filter(
      function(data){ return data.EmailAddress == code }
  );

  */
  //console.log("setcompcall",nam)  
  //console.log("res"+JSON.stringify(source));
 // nam = nam.toUpperCase();
  results = source.filter(function(entry) {
      return entry.EmailAddress.toUpperCase().includes(nam.toUpperCase());
  });

  console.log("wres"+JSON.stringify(results));
  if(results.length>0)
  {
  return results;
  }
  else
  {
    nam=nam.toUpperCase();
    results = source.filter(function(entry) {
      return entry.RoleDisplayName.toUpperCase().includes(nam);
  });
  if(results.length>0)
  {
    return results;
  }
  else{
    return source;
  }
}
}
catch(e)
{
  console.log('err');
}
}



  handleCancel = () => {
    console.log('Clicked cancel button');
    this.setState({
      dvisible: false,
    });
   // this.props.mvisible.rmodal(false);
  };
deleterec=async (comid)=>{
    //alert("to be deleted"+comid);
    
    var res=await apiCall.delete("user/"+comid);
    if(res.data)
    {
    console.log("deleted"+JSON.stringify(res));
    notification.open({
      message: 'Alert',
      description:
        'User deleted successfully',
      icon: <DeleteOutlined   style={{ color: '#0d4f8c' }} />,
    });
  
    this.reloaddata();
    }
    
  
    
    }
    reloaddata=async ()=>
    {
      var x=[];
      ///api/v1/user/company/{CompanyId}
      var CompanyId=localStorage.getItem('com');
      var res= await apiCall.getApi(`user/company/${CompanyId}`)
      //console.log("datafrom Api",res);
      res.data.Items.forEach(function(obj) {
        
        console.log("datafrom2 Api",obj);
        x.push(obj);
  
      //this.setState({userinfo:obj});
        
      
      });
  
  
      this.setState({
          userdata: x
        })
        
      this.setState({
        userinitdata: x
      })
  console.log("userinfo",this.state.userdata);
      
    }
    setCompany = async value => {
        console.log("setcomp",value)  
        //alert(value);
this.setState({companyname:value})
var searcharray=this.search(this.state.Admininfo,value);
console.log("search Array"+JSON.stringify(this.state.Admininfo)+"value"+value);
console.log("searcharray"+JSON.stringify(searcharray));
this.setState({
    searchinfo: searcharray
  })
/*this.setState(prevState => ({
    searchflag: !prevState.searchflag
  }));*/
  if(value=='ALL')
  {
     // alert("here");
      this.setState({searchflag:false})
  }
  else{
    this.setState({searchflag:true})
  }
    }

    
    search(source, nam){
        var results;
        //console.log("setcompcall",nam)  
        //console.log("res"+JSON.stringify(source));
        nam = nam.toUpperCase();
        results = source.filter(function(entry) {
            return entry.Name.toUpperCase().indexOf(nam) !== -1;
        });
    
        console.log("res"+JSON.stringify(results));
        return results;
    }

    validate=event=>{
    if(event.target.id=='FirstName')
    {
      if(event.target.value=='')
      {
        this.setState({Nerror:"First Name is required field"})
        this.setState({efl:1})
      }
      var bool=validator.validatefName(event.target.value)
///console.log("bool",bool);
if(!bool)
{
this.setState({Nerror:"Please review first name field"})
this.setState({efl:1})
    }
    else
    {
      this.setState({Nerror:""})
      this.setState({efl:0})
    }
  }
  else   if(event.target.id=='LastName')
  {
    if(event.target.value=='')
    {
      this.setState({ELname:"Last Name is required field"})
      this.setState({efl:1})
    }
    var bool=validator.validatelName(event.target.value)
///console.log("bool",bool);
if(!bool)
{
this.setState({ELname:"Please review last name field"})
this.setState({efl:1})
  }
  else
  {
    this.setState({ELname:""})
    this.setState({efl:0})
  }
}else   if(event.target.id=='EmailAddress')
{
  var bool=validator.email(event.target.value)
///console.log("bool",bool);
if(!bool)
{
this.setState({Eemail:"Please review email field"})
this.setState({efl:1})
}
else
{
  this.setState({Eemail:""})
  this.setState({efl:0})
}
}
else   if(event.target.id=='PhoneNum')
{
  var bool=validator.allnumeric(event.target.value)
///console.log("bool",bool);
if(!bool)
{
this.setState({Ephone:"Please review phone number field"})
this.setState({efl:1})
}
else
{
  this.setState({Ephone:""})
  this.setState({efl:0})
}
}
}
edit=(record)=>{
  //this.state.sh
  this.setState({ELname:""})
  this.setState({Nerror:""})
  this.setState({Eemail:""})
  this.setState({Erole:''})
  console.log("Record",record);
  this.setState({dFlag:false})
  this.setState({UserId:record.UserId})
  this.setState({eflag:true});
this.setState({FirstName:record.FirstName})
this.setState({LastName:record.LastName})
this.setState({EmailAddress:record.EmailAddress})
this.setState({PhoneNum:record.PhoneNum})
this.setState({Expires:record.Expires})
this.setState({IsEnabled:record.IsEnabled})
this.setState({IsMFAEnabled:record.IsMFAEnabled})
this.setState({RoleId:record.RoleId})
this.setState({RoleName:record.RoleName})
this.setState({RoleDisplayName:record.RoleDisplayName})
this.setState({CompanyId:record.CompanyId})
this.setState({CompanyName:record.CompanyName})
  this.setState({dvisible:true});
  this.setState({editFlag:true});
}

enable=()=>{
  const currentState = this.state.dFlag;
  this.setState({dFlag:!currentState});
}
repassword=async (rec)=>{
//  console.log("rpass",rec.EmailAddress)
try
{
 var res=await apiCall.putApi(`user/adminresetpassword/${rec.EmailAddress}`,'')
 console.log("pres",res)
 if(res.data.message)
 {
  notification.open({
    message: 'Alert',
    description:
     "Reset password request successful. A temporary password is send to registered Email.",
     icon: <CheckCircleOutlined  style={{ color: '#228B22' }} />,
  });  
 }
}
catch(err)
{
  console.log("error",err);
  notification.open({
    message: 'Alert',
    description:"Operation Failed Please Try Again.",
    icon: <CloseCircleOutlined  style={{ color: 'red' }} />,
  }); 
}

}
ractive=event=>{
  this.setState({ac:false})
}
checksel=(event)=>{

console.log("eeee",event);
}
render()
{
  try{
    const { TabPane } = Tabs;
    const { Option } = Select;
  
    const layout = {
        labelCol: { span: 8 },
        wrapperCol: { span: 16 },
        };
    
        let columns = [
            {
                // company: 'Company Name',
                // Adminname: 'company',
                key: 'action',
                title: '',
                render: (text, item) => (
                    <span>
                     <Avatar style={{backgroundColor:"#1890ff",boxShadow: "3px 6px 7px -1px #888888"}} >{item.FirstName.charAt(0).toUpperCase()}{item.LastName.charAt(0).toUpperCase()}</Avatar>
                      
                    </span>
                  ),
                },
            {
            // company: 'Company Name',
            // Adminname: 'company',
            key: 'FirstName',
            title: 'First Name',
            render: (text, item) => (
              <span>
              {item.FirstName.toUpperCase()}
              </span>
            ),
          },
            {
            // company: 'Company Name',
            // Adminname: 'company',
            key: 'LastName',
            title: 'Last Name',
            render: (text, item) => (
              <span>
             {item.LastName.toUpperCase()}
              </span>
            ),
            },
            {
            // company: 'Company Name',
            // Adminname: 'company',
            key: 'EmailAddress',
            title: 'Email Address',
            dataIndex: 'EmailAddress'
            },
            {
              // company: 'Company Name',
              // Adminname: 'company',
              key: 'CompanyId',
              title: 'Company Name',
              render: (text, item) => (
               <span> {item.CompanyName}</span>    
                              ),
              },
            {
            // company: 'Company Name',
            // Adminname: 'company',
            //<div><p style={{color: "rgba(0, 0, 0, 0.45)",fontSize:"14px"}}>{this.state.rolename}</p><Button style={{marginLeft: "4px"}} icon={<SettingOutlined />}></Button><Button  style={{marginLeft: "4px"}} type="primary" icon={<EditOutlined />}></Button><Button   style={{marginLeft: "4px"}} type="danger" icon={<DeleteOutlined />}></Button></div>
            key: 'RoleId',
            title: 'Role',
            render: (text, item) => (
              <span> {item.RoleDisplayName}</span>    
              ),
            },
            {
              // company: 'Company Name',
              // Adminname: 'company',
              //<div><p style={{color: "rgba(0, 0, 0, 0.45)",fontSize:"14px"}}>{this.state.rolename}</p><Button style={{marginLeft: "4px"}} icon={<SettingOutlined />}></Button><Button  style={{marginLeft: "4px"}} type="primary" icon={<EditOutlined />}></Button><Button   style={{marginLeft: "4px"}} type="danger" icon={<DeleteOutlined />}></Button></div>
              key: 'Expires',
              title: 'Expires',
              render: (text, item) => (
                <span> {moment(new Date(item.Expires)).format("DD MMM YYYY")} </span>    
                ),
              },
              {
                // company: 'Company Name',
                // Adminname: 'company',
                //<div><p style={{color: "rgba(0, 0, 0, 0.45)",fontSize:"14px"}}>{this.state.rolename}</p><Button style={{marginLeft: "4px"}} icon={<SettingOutlined />}></Button><Button  style={{marginLeft: "4px"}} type="primary" icon={<EditOutlined />}></Button><Button   style={{marginLeft: "4px"}} type="danger" icon={<DeleteOutlined />}></Button></div>
                key: 'CreatedOn',
                title: 'CreatedOn',
                render: (text, item) => (
                  <span>{moment(new Date(item.CreatedOn)).format("DD MMM YYYY")}  </span>  
                  ),
                },
            {
                // company: 'Company Name',
                // Adminname: 'company',
                key: 'IsEnabled',
                title: 'Status',
                render: (text, record) => (
                    <span>
                        
                    {record.IsEnabled=='Y'?
                    <Tooltip title="Active">
                    <CheckCircleOutlined  style={{fontSize: "23px",color: "#52c41a"}} />  </Tooltip>:<Tooltip title="In-Active">
                      <IssuesCloseOutlined  style={{fontSize: "23px",color: "#f5222d"}} />
                     </Tooltip>
                  
                      }
                        </span>
                  ),
                },
                
                    {
                      title: 'Action',
                      key: 'action',
                      render: (text, record) => (
                        <span>
                      
                          <Tooltip title="Edit User Details">
                        <FormOutlined  onClick={()=>this.edit(record)}/>
                         </Tooltip>
                         <Tooltip title="Reset User Password">
                         <UnlockOutlined  style={{color:"red"}} onClick={()=>this.repassword(record)}/>
                         </Tooltip>
                        </span>
                      
                      ),
                    }
                ];
                const content = (
                    <div>
                      
                      <h4>User access to portal will expire on this date.</h4>
                    </div>
                  );
  return (
    <div>
      <div>
      <Card style={{    borderRadius: "11px"}}>
      <h2 className="title gx-mb-4" style={{    color: "#1890ff"}}><IntlMessages id={"User > "+this.props.location.state.action}/></h2>

      <div className="gx-d-flex justify-content-center">
    
      </div>
      <div class="ant-row">
      <div class="ant-col ant-col-24" >




<Card title={<span style={{color:"#1890ff"}}>
<Form.Item label=""   {...formItemLayoutsec} style={{marginLeft:"10px"}}>
<Input type="text" id="usersearch" style={{ width: "226px"}} placeholder="Universal search for user" value={this.state.usersearch}  onChange={this.handlesearch}   prefix={<SearchOutlined/>}/>
</Form.Item>{(localStorage.getItem('user')=='WARD-Super-Admin'||localStorage.getItem('user')=='WARD-Company-Admin')&&localStorage.getItem('com')!='All'&&localStorage.getItem('com')&& this.props.location.state.action=='Create User'?<Button style={{float:"right",marginRight: "34px",marginTop: "-58px"}} onClick={()=>this.addmodal(this)}>+Add User</Button>:<p></p>}</span>}>
<Table className="gx-table-responsive" dataSource={this.state.userdata} columns={columns} style={{fontSize: "12px"}} pagination={{ defaultPageSize: 10, showSizeChanger: true, pageSizeOptions: ['10', '20', '30']}} size="small"/>

</Card>
</div>


  </div></Card></div>
  <Modal bodyStyle={{ padding: '0' }}
          title=""
          visible={this.state.dvisible}
          onOk={this.handleOk}
          confirmLoading={this.state.confirmLoading}
          onCancel={this.handleCancel}
        style={{marginLeft: "24%",marginTop:"-20px", height: "419px"}}  width={900}>
         
          <div>
         
          <Card className="gx-card" title={this.state.editFlag?'user Edit Form':'User Registration Form'}>
        <Form autocomplete="off" onSubmit={this.handleSubmit} style={{marginLeft: "3px"}}  >
     
        <Row>
<Col span={12} className="colcustom">
<Form.Item className="fcust" {...formItemLayout}  >
    <Input placeholder="First Name *" style={{ width: "360px"}} onBlur={this.validate} id="FirstName" value={this.state.FirstName} onChange={this.handleChange}  prefix={<UserOutlined/>}required/>
    <div style={{ fontSize: 12, color: "red" }}>
                {this.state.Nerror}
                </div>
</Form.Item>
</Col >

<Col span={12} className="colcustom">
<Form.Item className="fcust" {...formItemLayout}>
<Input placeholder="Last Name *" id="LastName"  onBlur={this.validate} style={{ width: "360px"}} onBlur={this.validate} value={this.state.LastName} onChange={this.handleChange} prefix={<UserOutlined/>} required/>
<div style={{ fontSize: 12, color: "red" }}>
                {this.state.ELname}
                </div>
</Form.Item>
</Col>
<Col span={12} className="colcustom">
<Form.Item className="fcust" {...formItemLayout}  >
    <Input  placeholder="Email Address *"  style={{ width: "360px"}}  onBlur={this.validate} id="EmailAddress" value={this.state.EmailAddress} onChange={this.handleChange} prefix={<MailOutlined />} required disabled={this.state.editFlag}/>
    <div style={{ fontSize: 12, color: "red" }}>
    {this.state.Eemail}
                  </div>
</Form.Item>
</Col>
<Col span={12} className="colcustom">
<Form.Item>

     
        <Input type="phone" placeholder="Phone Number"  onBlur={this.validate} id="PhoneNum" style={{ width: "378px",paddingLeft:"17px"}} value={this.state.PhoneNum}  onChange={this.handleChange} prefix={<PhoneOutlined style={{paddingLeft:"17px"}}/>}required/>
        <div style={{ fontSize: 12, color: "red" }}>
              
                </div>
   </Form.Item>

   </Col>

<Col span={12} className="colcustom">
<Form.Item {...formItemLayout}>
<Input type="Company" placeholder={this.state.companyname} id="PhoneNum" style={{ width: "358px",paddingLeft:"0px"}} value={this.state.companyname}  prefix={<i className="icon icon-company"/>}disabled/>

</Form.Item >
</Col>
<Col span={12} className="colcustom">
<Form.Item {...formItemLayout}>
{!this.state.editFlag?this.state.RoleId=='' && this.state.ac?<Input type="Company" placeholder="User Role *" style={{ width: "360px"}} id="PhoneNum" value="" onClick={this.ractive}/>:<Select
    showSearch
    style={{ width: "360px",marginLeft: "4px" }}
    placeholder="User Role *"
    optionFilterProp="children"

   
    
    onBlur={this.registerrole}
    onChange={this.checksel}
   
  >
    
  {this.state.roleid.map((x,y) => <option key={x.RoleId+"@"+x.Name} >{x.Name}</option>)}
  
  </Select>:<Input type="Company" placeholder={this.state.RoleName} style={{ width: "360px"}} id="PhoneNum" value={this.state.RoleName} disabled/>}
  <div style={{ fontSize: 12, color: "red" }}>
                {this.state.Erole}
                </div>
</Form.Item>
</Col>

<Col span={12} className="colcustom">

<Form.Item label="User Status" style={{marginLeft: "-42px"}} {...formItemLayout}>
 
<Switch
      checkedChildren={<CheckOutlined />}
      unCheckedChildren={<CloseOutlined />}
      checked={this.state.IsEnabled=='Y'?true:false}
      onChange={this.onChanges}
    />
</Form.Item>

</Col>
<Col span={12} className="colcustom">
<Form.Item className="fcust" label="MFA Setting"  {...formItemLayout}>
<Switch  checkedChildren="Enable"
                          unCheckedChildren="Disable"  checked={this.state.IsMFAEnabled=='Y'?true:false} disabled={this.state.IsMFAEnabled=='Y'?false:true}  onChange={this.onChanges2}/>
</Form.Item>
</Col>
<Col span={12} className="colcustom">  <Popover content={content} title="Expiry Date" trigger="hover">
<Form.Item label="Expires" {...formItemLayout} style={{marginLeft: "-83px"}} >
{this.state.dFlag?<DatePicker style={{marginLeft: "-30px",width: "292px"}}  onChange={this.seldate} onBlur={this.blur}    />:

<Input type="text" value={moment(new Date(this.state.Expires)).format("DD MMM YYYY")} onClick={this.enable}/>}
<div style={{ fontSize: 12, color: "red" }}>
                
                </div>
</Form.Item>
</Popover>
</Col>
</Row>


</Form>
          </Card>
        
          </div>
          </Modal>
    </div>
  );
}
catch(ex)
{
  localStorage.setItem("dkey",'1');
  localStorage.setItem("skey",'1122');
  window.location.href = '/dashboard';
}
}
};

export default UserPage;
