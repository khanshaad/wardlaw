import React from 'react';
import { Upload,Spin,Row,Col,Form,Input,Select,Switch,Modal,Popover} from 'antd';
import {
    MenuUnfoldOutlined,EditOutlined,DeleteOutlined,FileExcelTwoTone,EyeOutlined,CheckCircleOutlined,IssuesCloseOutlined,FunctionOutlined,
    MenuFoldOutlined,user,CheckCircleFilled,DownloadOutlined,WarningOutlined,
    UserOutlined,LogoutOutlined,PlusCircleOutlined,FormOutlined,
    VideoCameraOutlined,ProfileOutlined,SettingOutlined,RedoOutlined,HourglassOutlined,PhoneOutlined,LinkOutlined,CheckOutlined,CloseOutlined,
    UploadOutlined,HomeOutlined,FieldTimeOutlined,LineChartOutlined,FileSyncOutlined
  } from '@ant-design/icons';
  export default function Headline() {
    const greeting = [
        {
        
          key: 'error',
          title: '',
          render: (text, record) => (
            <span>{record.estatus ?<Popover content={record.emessage} title=""> <span><WarningOutlined style={{fontSize: "23px",
          color: "red"
          }}/></span></Popover>:<span><CheckCircleOutlined style={{fontSize: "23px",
          color: "green"
          }}/></span>}</span>
          ),
          },
        {
        
          key: 'ChannelType',
          title: 'Channel Type',
          dataIndex: 'ChannelType'
          },
          {
        
            key: 'Email',
            title: 'Email',
            dataIndex: 'Email'
            },
            {
        
              key: 'Phone',
              title: 'Phone',
              dataIndex: 'Phone'
              },
              {
        
                key: 'Name',
                title: 'Name',
                dataIndex: 'Name'
                },
               
                   {
        
                  key: 'IsUnsubscribed',
                  title: 'IsUnsubscribed',
                  render: (text, record) => (
                    <span>
                  {record.IsUnsubscribed=='FALSE'? <CheckCircleOutlined style={{fontSize: "23px",
        color: "green"
    }}/>:<IssuesCloseOutlined style={{fontSize: "23px",
    color: "red"
    }}/>
                   }
                    </span>
                    
                  ),
                  },
                 
                 
                    {
                      title: 'Action',
                      key: 'action',
                      fixed: 'right',
                      render: (text, record) => (
                      
                         <span>
            <a style={{ marginRight: 5 }} onClick={()=>this.showmodal(record.TransactionId,false)}>preview </a>
            <a style={{ marginRight: 5 }} onClick={()=>this.showmodal(record.TransactionId,true)}>edit </a>
            <a  style={{ marginRight: 5 }} onClick={()=>this.delete(record.TransactionId)}>delete</a>
          </span>
                        
                    
                        
                      ),
                    }
                    
    
      ];
    return greeting;
  }