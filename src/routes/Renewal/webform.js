import React, { Component, Fragment,useState, useEffect } from 'react';
import axios from 'axios';
import { Button} from 'antd';
import './user.css';
import {
    MenuUnfoldOutlined,EditOutlined,DeleteOutlined,
    MenuFoldOutlined,user,
    UserOutlined,LogoutOutlined,PlusCircleOutlined,
    VideoCameraOutlined,ProfileOutlined,SettingOutlined,RedoOutlined,HourglassOutlined,
    UploadOutlined,HomeOutlined,FieldTimeOutlined,LineChartOutlined,FileSyncOutlined
  } from '@ant-design/icons';
export default class webform extends Component {

 constructor(props) {
		super(props);
    this.state = {
      collapsed:true,
      logout: false,
      loading: false,
      rolename:''
      //userdata:[]
    };
  }
  async componentDidMount() {
    

}
handleChange = event => {
    this.setState({
          [event.target.id]: event.target.value
      });
  };


handleOk =async () => {
    this.setState({
      ModalText: 'The modal will be closed after two seconds',
      confirmLoading: true,
    });
    if(this.state.Name)
    {
      this.state.edata.Name=this.state.Name;
    }
    if(this.state.phone)
    {
      this.state.edata.Phone=this.state.phone;
      this.state.edata.ChannelAddress=this.state.phone;
    }
    if(this.state.email)
    {
     
      this.state.edata.Email=this.state.email;
      this.state.edata.ChannelAddress=this.state.email;
      //alert(this.state.edata.ChannelAddress)
    }
    if(this.state.ctype)
    {
      this.state.edata.ChannelType=this.state.ctype;
    }
    //this.setState({res:this.state.edata})
    console.log("ctype",this.state.edata.ChannelType)
  var params=
    {
      "Phone": this.state.edata.Phone,
      "ChannelAddress":  this.state.edata.ChannelAddress,
      "BatchId": this.state.edata.BatchId,
      "SubscriptionType": "RENEWALS",
      "TransactionId": this.state.edata.TransactionId,
      "CompanyName": this.state.edata.CompanyName,
      "ChannelType": this.state.edata.ChannelType,
      "Email":  this.state.edata.Email,
      "ChannelAdress": this.state.edata.ChannelAddress,
      "Company": this.state.edata.Company,
      "Name": this.state.edata.Name,
      "SecondaryColor1": this.state.edata.SecondaryColor1,
      "PrimaryColor1": this.state.edata.PrimaryColor1,
      "LanguageCode": this.state.edata.LanguageCode,
      "PrimaryColor2": this.state.edata.PrimaryColor2,
      "UploadedBy": this.state.edata.UploadedBy,
      "IsUnsubscribed": this.state.edata.IsUnsubscribed
     
  }
  try
  {
  var res=await apiCall.putApi('enduser', params);
  
  //console.log("put",res);
  var x=this.state.res;
  var y=[]
  //console.log(`https://yi70tn8ms6.execute-api.us-east-1.amazonaws.com/dev/api/v1/enduser/`+this.state.edata.TransactionId)
  //var res=await axios.get(`https://yi70tn8ms6.execute-api.us-east-1.amazonaws.com/dev/api/v1/enduser/`+this.state.edata.TransactionId)
  //console.log("gget",res);
  var tranc=res.data.Item.TransactionId;
  x.forEach(function(obj) {
    console.log("obj",obj)
    console.log("tranc",tranc)
  if(obj.TransactionId!=tranc)
  {
  
  
    
    y.push(obj);
    console.log("push",y)
  
  }
  
  
  
     });
     console.log("yval",y)
     y.push(res.data.Item)
    // this.setState({res:y});
     var estatus=[]
     var eflag=0;
     y.forEach(function(obj) {
      if((obj.ChannelType=="EMAIL")&&(validator.email(obj.Email) == false))
      {
      obj.estatus=1;
      eflag=1;
      obj.emessage="Email Error Please Review";
      
      }
      
      
      
      else if((obj.ChannelType=="SMS")&&(validator.allnumeric(obj.Phone) == false))
      {
      obj.estatus=1;
      eflag=1;
      obj.emessage="Error On Phone Number Please Review";
      
      }
      else {
        obj.estatus=0;
        eflag=0;
      }
      estatus.push(obj);
     });
  this.setState({errstatus:eflag})
  
  console.log("estatus",estatus);
     this.setState({res:estatus});
     message.success("End-User updated successfully")
    this.setState({phone:''})
    this.setState({Name:''})
    this.setState({email:''})
    }
    catch(err)
    {
      message.error("Unable to update End-User"+err)
    }
  
  
  setTimeout(() => {
    this.setState({
     dvisible: false,
      confirmLoading: false,
    });
   
  }, 2000);
  
   
  
  }

render()
{
    return <Card
    style={{ marginTop: 16 }}
    type="inner"
    title=""
     title="End-user Registration Form"
  >
<div style={{marginLeft: "109px"}}>
      <Form autocomplete="off">
   
      <Row>
<Col span={12} className="colcustom">
<Form.Item className="fcust" >
  <Input type="text" placeholder="Name *" style={{ width: "360px"}} id="Name"  value={this.state.Name} onChange={this.handleChange} prefix={<UserOutlined/> }required/>
</Form.Item>
</Col >
<Col span={12} className="colcustom">
<Form.Item className="fcust" >
  <Input placeholder="Email *" style={{ width: "360px"}}  id="email" value={this.state.email} onChange={this.handleChange}  prefix={<FormOutlined />} required/>
</Form.Item>
</Col >
<Col span={12} className="colcustom">
<Form.Item className="fcust" >
  <Input placeholder="Phone Number" style={{ width: "360px"}} id="phone" value={this.state.phone} onChange={this.handleChange}   prefix={<PhoneOutlined />}required/>
</Form.Item>
</Col >
<Col span={12} className="colcustom">
<Form.Item className="fcust" label="Language" >
<Select
  onChange={this.lang}
  style={{ width: 282 }}>
<option value="English">English</option>
<option value="Spanish">Spanish</option>
</Select>
</Form.Item>
</Col >
<Col span={12} className="colcustom">
<Form.Item label="Channel Type *" >
<Select
  onChange={this.channelType}
  style={{ width: 248 }}>
<option value="EMAIL">EMAIL</option>
<option value="SMS">SMS</option>
</Select>
</Form.Item></Col>
<Col span={12} className="colcustom">
<Form.Item label="Subscription Type">
<Input placeholder="RENEWALS" style={{ width: "231px"}}  id="email" value="RENEWALS"   disabled/>

</Form.Item></Col>

<Col span={12} className="colcustom">

<Form.Item label="" >
<Input placeholder="Field #1" style={{ width: "360px"}} id="FirstName"   prefix={<LinkOutlined />}required/>
</Form.Item>

</Col>
<Col span={12} className="colcustom">

<Form.Item label="" >
<Input placeholder="Field #2" style={{ width: "360px"}} id="FirstName"   prefix={<LinkOutlined />}required/>
</Form.Item>

</Col>
<Col span={12} className="colcustom">

<Form.Item label="" >
<Input placeholder="Field #3" style={{ width: "360px"}} id="FirstName"   prefix={<LinkOutlined />}required/>
</Form.Item>

</Col>
<Col span={12} className="colcustom">

<Form.Item label="" >
<Input placeholder="Field #4" style={{ width: "360px"}} id="FirstName"   prefix={<LinkOutlined />}required/>
</Form.Item>

</Col>

</Row>
</Form>



</div>
  </Card>
}

}