import React, { Component, Fragment,useState, useEffect } from 'react';
import axios from 'axios';
import { CSVLink, CSVDownload } from "react-csv";
import IntlMessages from "util/IntlMessages";
import { Upload,Spin,Row,Col,Form,Input,Select,Switch,Modal,Popover,Tooltip} from 'antd';
import { InboxOutlined } from '@ant-design/icons';
import CSVReader from "react-csv-reader";
import validator from '../../validators/validator';
import moment from "moment";
import Iframe from 'react-iframe'
import search from '../../util/Search';
import "./renewal.css";
import apiCall from '../../apiutil/apicall';
import Widget from "components/Widget/index";
import column from './renewal';
import InputTextField from '../../dynamicForm/InputTextField'
import Datepicker from "../../dynamicForm/Datepicker";
import { Steps, Button, message,Card,Alert,Table,Badge } from 'antd';
import * as _JFORM from "./Form.json";
import Dropdown from '../../dynamicForm/Dropdown';
import {
    MenuUnfoldOutlined,EditOutlined,DeleteOutlined,FileExcelTwoTone,EyeOutlined,CheckCircleOutlined,IssuesCloseOutlined,FunctionOutlined,
    MenuFoldOutlined,user,CheckCircleFilled,DownloadOutlined,WarningOutlined,FilterOutlined,
    UserOutlined,LogoutOutlined,PlusCircleOutlined,FormOutlined,
    VideoCameraOutlined,ProfileOutlined,SettingOutlined,RedoOutlined,HourglassOutlined,PhoneOutlined,LinkOutlined,CheckOutlined,CloseOutlined,
    UploadOutlined,HomeOutlined,FieldTimeOutlined,LineChartOutlined,FileSyncOutlined
  } from '@ant-design/icons';
  const { Step } = Steps;
  const { Dragger } = Upload;
  const papaparseOptions = {
    header: true,
    dynamicTyping: true,
    skipEmptyLines: true,
    transformHeader: header => header.toLowerCase().replace(/\W/g, "_")
  };
  
  const props = {
    name: 'file',
    multiple: false,
    action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
    onChange(info) {
      const { status } = info.file;
      console.log("fstatus",info)
      if (status !== 'uploading') {
        console.log("here",info.file, info.fileList);

      }
      if (status === 'done') {
        message.success(`${info.file.name} file uploaded successfully.`);
        let reader = new FileReader(); 

      reader.readAsText(info.file[0]); 

  

      reader.onload = () => { 

        let csvData = reader.result; 

        let csvRecordsArray = (csvData).split(/\r\n|\n/); 

  

       
        console.log(csvRecordsArray)
      }

      } 
      else if (status === 'error') {
        message.error(`${info.file.name} file upload failed.`);
      }
    },
  };
export default class Renewal extends Component {

 constructor(props) {
		super(props);
    this.state = {
      collapsed:false,
      logout: false,
      loading: false,
      rolename:'',
      current: 0,
      fname:'',
      substatus:'',
      domain:'',
      enduser:[],
      initenduser:[],
      errstatus:0,
      batchid:'',
      showprevious:true,
      res:[],
      html:'',
      unsubcount:0,
      erflag:true,
      subcount:0,
      //userdata:[],
      dvisible:false,
      Name:'',
      name:localStorage.getItem('name'),
      email:localStorage.getItem('email'),
      ctype:localStorage.getItem('ctype')?localStorage.getItem('ctype'):"EMAIL",
      mlender:localStorage.getItem('mlender'),
      policyage:localStorage.getItem('policyage'),
      rdate:localStorage.getItem('rdate'),
      edate:localStorage.getItem('edate'),
      atype:localStorage.getItem('atype'),
      add1:localStorage.getItem('add1'),
      add2:localStorage.getItem('add2'),
     //pstreet:localStorage.getItem('pstreet'),
      sfinsur:localStorage.getItem('sfinsur')?localStorage.getItem('sfinsur'):"N",
      slinsur:localStorage.getItem('slinsur')?localStorage.getItem('slinsur'):"N",
      spinsur:localStorage.getItem('spinsur')?localStorage.getItem('spinsur'):"N",
      CDAmount:localStorage.getItem('CDAmount'),
      CLossAmount:localStorage.getItem('CLossAmount'),
      COSAmount:localStorage.getItem('COSAmount'),
      CPPAmount:localStorage.getItem('CPPAmount'),
      CustomerState:localStorage.getItem('CustomerState'),
      FIPType:localStorage.getItem('FIPType'),
      HDAmount:localStorage.getItem('HDAmount'),
      IRType:localStorage.getItem('IRType'),
      InsuredStreet:localStorage.getItem('InsuredStreet'),
      LOIPType:localStorage.getItem('LOIPType'),
      MPAmount:localStorage.getItem('MPAmount'),
      PLAmount:localStorage.getItem('PLAmount'),
      PTotal:localStorage.getItem('PTotal'),
      SDAmount:localStorage.getItem('SDAmount'),
      changeTotal:localStorage.getItem('changeTotal'),
      creditsTotal:localStorage.getItem('creditsTotal'),
      lcoverage:localStorage.getItem('lcoverage'),

      stype:'RENEWALS',
      phone:localStorage.getItem('phone'),
      lang:'',
      form:false,
      eid:'',
      edata:[],
      unflag:'',
      emessage:'',
      efilter:[],
      substate:'',
      bdisabled:false,
      initbuttondisabled:true,
      fields:[],
      header:[],
      countrycode:'',
      nextflag:false,
      subserr:'',
      hideNav:false

    };
  }
  async componentDidMount() {
    
    window.addEventListener("resize", this.resize.bind(this));
    this.resize();
  try
  {
var x= this.props.location.state.subtype;
  }
  catch(ex)
  {
    window.location.href = '/dashboard';
  }
  // this.setState({countrycode:x.toString()})

   //console.log("countrycode"+"---x----"+x+"----x----"+this.state.countrycode+' ----'+process.env.REACT_APP_COUNTRYCODE)
    this.setState({erflag:true})
    if(this.props.location.state.form)
    {
      this.setState({initbuttondisabled:false})
    }
 // let {subtype}=this.props.location.state;

   /* if(this.props.location.state.subtype=="Renewal")
    {
      var x=[];
      this.setState({fields:_JFORM[0].Renewal})
      _JFORM[0].Renewal.forEach((data)=>{
      x.push({label:data.header,key:data.name})
      });
      console.log("1header",x)
      this.setState({header:x});
    }
    else if(this.props.location.state.subtype=="Onboarding")
    {
      var x=[];
      this.setState({fields:_JFORM[1].Onboarding})
      _JFORM[1].Onboarding.forEach((data)=>{
        x.push({label:data.header,key:data.name})
        });
        console.log("1header",x)
        this.setState({header:x});
    }
    else
    {
      this.setState({fields:_JFORM[2].Claims})
      var x=[];
      try
      {
      this.setState({fields:_JFORM[2].Onboarding})
      _JFORM[1].Claims.forEach((data)=>{
        x.push({label:data.header,key:data.name})
        });
        console.log("1header",x)
        this.setState({header:x});
      }
      catch (err)
      {
        
      }
    }*/
try{
 // this.setState({name:localStorage.getItem('name')})
  //alert(localStorage.getItem('name'));
  //alert(this.state.name);
var res=await apiCall.getApi(`subscriptionsetup/type/${this.props.location.state.subtype.toUpperCase()}`)

console.log("subs",res.data.Items[0].FieldList);
this.setState({fields:res.data.Items[0].FieldList})

console.log("sdsd",this.state.fields)
var x=[];
res.data.Items[0].FieldList.forEach((data)=>{
  x.push({label:data.header,key:data.name})
 console.log("sdata",data);
  });
  this.setState({header:x});
var com=localStorage.getItem('com')
console.log("qwqwq",com)
  var res=await apiCall.getApi(`company/${com}/subscription/${this.props.location.state.subtype.toUpperCase()}`)
//console.log("rtrtrtr",res);
this.setState({subserr:res.data});
console.log("rtrtrtr",this.state.subserr);
}
catch(err)
{
  console.log(err)
}

  //  console.log('sum',await apiCall.getApi('api/v1/role',''))
  console.log("fname"+localStorage.getItem('firstname'))
 //console.log("getdata",this.props.location.state.subType);
 this.setState({substate:this.props.location.state.subtype})
  console.log("lname"+localStorage.getItem('lastname'))

}

resize() {
  let currentHideNav = (window.innerWidth <= 760);
  if (currentHideNav !== this.state.hideNav) {
      this.setState({hideNav: currentHideNav});
      console.log('1resize',this.state.hideNav);
  }
}


onChange=(checked) =>{
  //var x=this.state.res;
  //console.log(`switch to ${checked}`);
  if(checked)
  {
    var x=this.state.res;
var y=[]
x.forEach(function(obj) {
 
if(obj.IsUnsubscribed=="TRUE")
{
  y.push(obj);
  console.log("push",y)

}

   });
   this.setState({res:y});
  // console.log("yval",y)

  }
  else
  {
    console.log("endsds",this.state.initenduser);
    this.setState({res:this.state.initenduser});
  }
}
handleOk =async () => {
  this.setState({
    ModalText: 'The modal will be closed after two seconds',
    confirmLoading: true,
  });
  if(this.state.form)
  {
  if(this.state.Name)
  {
    this.state.edata.Name=this.state.Name;
  }
  //if(this.state.phone)
  //{
    this.state.edata.Phone=this.state.phone?this.state.phone:"Blank Phone Field"
   // this.state.edata.ChannelAddress=this.state.phone;
 // }
  //if(this.state.email)
  //{
   
    this.state.edata.Email=this.state.email?this.state.email:"Blank Email Field";
   // this.state.edata.ChannelAddress=this.state.email;
    //alert(this.state.edata.ChannelAddress)
  //}
  if(this.state.ctype)
  {
    this.state.edata.ChannelType=this.state.ctype;
  }
  if(this.state.ctype=="SMS")
  {
    this.state.edata.ChannelAddress=this.state.phone?this.state.phone:"Blank Phone Field"
    //alert("here"+ this.state.edata.ChannelAddress)
  }
  if(this.state.ctype=="EMAIL")
  {
    this.state.edata.ChannelAddress=this.state.email?this.state.email:"Blank Email Field";
  }
  //this.setState({res:this.state.edata})
  console.log("ctype"+this.state.ctype+"--------"+this.state.edata.ChannelAddress)
var params=
  {
    "Phone": this.state.edata.Phone,
    "ChannelAddress":  this.state.edata.ChannelAddress,
    "BatchId": this.state.edata.BatchId,
    "SubscriptionType": this.props.location.state.subtype.toUpperCase(),
    "TransactionId": this.state.edata.TransactionId,
    "CompanyName": this.state.edata.CompanyName,
    "ChannelType": this.state.edata.ChannelType,
    "Email":  this.state.edata.Email,
    "Company": this.state.edata.Company,
    "Name": this.state.edata.Name,
    "FieldList":this.state.FieldList,
    "SecondaryColor1": this.state.edata.SecondaryColor1,
    "PrimaryColor1": this.state.edata.PrimaryColor1,
    "LanguageCode": this.state.edata.LanguageCode,
    "PrimaryColor2": this.state.edata.PrimaryColor2,
    "UploadedBy": this.state.edata.UploadedBy,
    "IsUnsubscribed": this.state.edata.IsUnsubscribed
   
}
console.log("ppp",params);
try
{
var res=await apiCall.putApi('enduser', params);

//console.log("put",res);
var x=this.state.res;
var y=[]
var unsubcount=0;
var subcount=0;
var unflag='';
var tranc=res.data.Item.TransactionId;
x.forEach(function(obj) {
  console.log("obj",obj)
  console.log("tranc",tranc)
if(obj.TransactionId!=tranc)
{
 

  
  y.push(obj);
  console.log("push",y)

}



   });
   console.log("yval",y)
   
   y.push(res.data.Item)
  // this.setState({res:y});
   var estatus=[]
   var eflag=0;
   var substatus=this.state.substatus;
   var domain=this.state.domain;
   y.forEach(function(obj) {
    if(obj.IsUnsubscribed=='TRUE')
    {
    
      unsubcount=unsubcount+1;
        unflag=true;
        }
        else{
          subcount=subcount+1;
        }
    if((obj.ChannelType=="EMAIL")&&(validator.email(obj.Email) == false))
    {
    obj.estatus=1;
    eflag=1;
    obj.emessage="Invalid Email Address";
    
    }
    else if((obj.ChannelType=="EMAIL")&&(validator.email(obj.Email) == true)&&(substatus=="InActive"))
    {
      var e=obj.Email.split("@");
      if(e[1]!=domain)
      {
        obj.estatus=1;
        obj.emessage="Cross Domain Email Address Not Allowed Please Delete";
        eflag=1;
      }   
    

    }
    else if((obj.ChannelType=="SMS")&&(validator.allnumeric(obj.Phone) == true)&&(substatus=="InActive"))
    {
      obj.estatus=1;
      obj.emessage="No SMS Channel Type Is Allowed";
      eflag=1;
    }
    else if((obj.ChannelType=="SMS")&&(validator.allnumeric(obj.Phone) == false))
    {
    obj.estatus=1;
    eflag=1;
    obj.emessage="Invalid Phone Number";
    
    }
    else {
      obj.estatus=0;
      eflag=0;
    }
    estatus.push(obj);
   });
this.setState({errstatus:eflag})
this.setState({unsubcount});
   this.setState({subcount});
   this.setState({unflag});
console.log("estatus",estatus);
   this.setState({res:estatus});
   var update=this.state.initenduser;
   var up=[];
  
   var i=0;
   console.log("update",update);
   estatus.forEach((obj)=>
   {

     if(obj.TransactionId!=update[i].TransactionId)
     {
      up.push(obj);
  
     }
   
    i++
   });
   console.log("up",up)
  
   message.success("End-User updated successfully")
  this.setState({phone:''})
  this.setState({Name:''})
  this.setState({email:''})
  var s=await search.searchValue(this.state.res,1,"estatus");
  console.log("search",s);
  if(eflag==1)
  {
  this.setState({erflag:true});
  }
  else
  {
    this.setState({erflag:false});
  }
  this.setState({res:estatus});
  }
  catch(err)
  {
    message.error("Unable to update End-User"+err)
  }


setTimeout(() => {
  this.setState({
   dvisible: false,
    confirmLoading: false,
  });
 
}, 2000);

}
else
{
  setTimeout(() => {
    this.setState({
     dvisible: false,
      confirmLoading: false,
    });
   
  }, 2000);
}

}
onChangeHandler=event=>{

  console.log("event",event.target.files[0])
  console.log("fevent",event.target.files[0].name)
  var vaildcsv=this.isValidCSVFile(event.target.files[0])
  //console.log("valid",x);
  
  if(localStorage.getItem('com')!='All'&&localStorage.getItem('com'))
  {
    if(vaildcsv)
  {
  var fname=event.target.files[0].name;
  let reader = new FileReader(); 
console.log('Filename',fname);
      reader.readAsText(event.target.files[0]); 
      this.setState({loading:true});
      
  var substype=this.state.substate;
  console.log("getdata",substype);
      reader.onload = async () => { 

        let csvData = reader.result; 

        let csvRecordsArray = (csvData).split(/\r\n|\n/); 

      
       let headersRow = this.getHeaderArray(csvRecordsArray); 
      
  console.log('header',headersRow);
       var uploadGLRecords = this.getDataRecordsArrayFromCSVFile(csvRecordsArray, headersRow.length,headersRow,substype); 
       if(uploadGLRecords!="error")
       {
       console.log("uploadGLRecords"+JSON.stringify(uploadGLRecords))
       this.setState({enduser:uploadGLRecords})
    console.log("uid"+localStorage.getItem("userId"));
      // var x=[];
       var params = {
        "CompanyId": localStorage.getItem('com'),
        "LoggedInUser": localStorage.getItem('UserIdinfo'),
        "SubscriptionType":this.props.location.state.subtype.toUpperCase(),
        "LoggedInUserName":localStorage.getItem('UserInitialsinfo'),
        "EndUsers" :uploadGLRecords
       }
      console.log("params"+JSON.stringify(params))
     
     this.setState({loading:true})
    // console.log("params"+JSON.stringify(params));
    var x=[];
    var unflag='';
    var unsubcount=0;
    var subcount=0;
    var eflag=0;
    try{
    var res=await apiCall.postApi('enduser/upload', params);
    console.log("response2",res);
    
    // /BatchId: "ee9a22a4-f4cd-4026-af77-2fac5a8342b8"
    if(res.data.BatchId)
    {
      this.setState({loading:false})
   this.setState({batchid:res.data.BatchId})
   var substatus=res.data.SubscriptionStatus;
   var domain=res.data.Domain;
   this.setState({substatus})
   this.setState({domain})
   res.data.Items.forEach(function(obj) {
     obj.errstatus=0
if(obj.IsUnsubscribed=='TRUE')
{

  unsubcount=unsubcount+1;
    unflag=true;
    }
    else{
      subcount=subcount+1;
    }
    console.log("2phone",validator.allnumeric(obj.Phone))
    if((obj.ChannelType=="EMAIL")&&(validator.email(obj.Email) == false))
    {
    obj.estatus=1;
    obj.emessage="Invalid Email Address";
    eflag=1;
    }
    else if((obj.ChannelType=="EMAIL")&&(validator.email(obj.Email) == true)&&(substatus=="InActive"))
    {
      var e=obj.Email.split("@");
      if(e[1]!=domain)
      {
        obj.estatus=1;
        obj.emessage="Cross Domain Email Address Not Allowed please delete";
        eflag=1;
      }   
    

    }
    else if((obj.ChannelType=="SMS")&&(validator.allnumeric(obj.Phone) == true)&&(substatus=="InActive"))
    {
      obj.estatus=1;
      obj.emessage="No SMS Channel Type Is Allowed";
      eflag=1;
    }
    
    else if((obj.ChannelType=="SMS")&&(validator.allnumeric(obj.Phone) == false))
    {
      eflag=1;
    obj.estatus=1;
    obj.emessage="Invalid Phone Number";
    
    }
    else
    {
      obj.estatus=0;
      eflag=0;
    }
    
    
  x.push(obj);
   });
   this.setState({errstatus:eflag})
   console.log("errstatus",eflag);
   this.setState({unsubcount});
   this.setState({subcount});
   this.setState({unflag});
   var efilter=[]
 /*  x.forEach((obj)=>{
     if(obj.estatus==1)
     {
       efilter.push(obj);
     }
   });*/
   var s=await search.searchValue(x,1,"estatus");
   console.log("search",s);
   this.setState({res:s});

   console.log("efilter",efilter);
   this.setState({efilter:s});
   this.setState({res:s});
  // this.setState({res:x});
   this.setState({initenduser:x});
   console.log("reserror",this.state.res);
    }

       /*test*/

  var s=await search.searchValue(this.state.initenduser,1,"estatus");
  console.log("search",s);
  this.setState({res:s});
  this.setState({initbuttondisabled:false})
  this.setState({fname})

  if(eflag==1)
  {
    this.setState({erflag:true})
  
  }
  else
  {
    this.setState({res:x});
    this.setState({erflag:false})
  }
  
  setTimeout(()=>{
this.setState({loading:false});
message.success('File Processed')
  },2000)
}
  catch (err)
  {
    this.setState({fname})
    setTimeout(()=>{
  this.setState({loading:false});
  message.error('Unable to process file please try again')
    },2000)
  }
  }
  else
  {
    setTimeout(()=>{
      this.setState({loading:false});
      message.error('File processing error invalid columns found in CSV')
        },2000)
        
  }

  /*test*/
        }




}




      
      else
      {
        message.error("Invalid file extension please upload a CSV file");
      }
    }
      else
      {
        message.error('Select Company')
      }
    



}

getHeaderArray(csvRecordsArr) { 

  let headers = (csvRecordsArr[0]).split(','); 

  let headerArray = []; 

  for (let j = 0; j < headers.length; j++) { 

    headerArray.push(headers[j]); 

  } 

  return headerArray; 

} 
handleCancel = () => {
  console.log('Clicked cancel button');
  this.setState({
    dvisible: false,
  });
 // this.props.mvisible.rmodal(false);
};
getDataRecords(curruntRecord,substype)
{ 

    let csvArr = []; 
  
    try{
    for (let i = 0; i < curruntRecord.length; i++) { 
  
   //   let curruntRecord = (curruntRecord[i]).split(','); 
  
 
  
   let csvRecord
 
   if(substype=='Renewals' && Object.keys(curruntRecord[i]).includes('policy_age'))
   {
     var pnum;
    // alert("push here"+ substype +'   ' +Object.keys(curruntRecord[i]).includes('policy_age')+curruntRecord[i].channel_type__email_)
   var ch;
   if(curruntRecord[i].channel_type__email_.trim())
   {
   ch='EMAIL'
   }
   else if(pnum)
   {
     ch='SMS'
   }
   else{
     ch='EMAIL'
   }
   // console.log("pnum",curruntRecord[7])
         csvRecord = {
   
   
           ChannelType: curruntRecord[i].channel_type__email_?curruntRecord[i].channel_type__email_.trim():ch,
   
           Email: curruntRecord[i].email?curruntRecord[i].email.trim():'Blank Email Field',
   
           Phone: pnum?pnum:'Blank Phone Field',
   
           Name: curruntRecord[i].name?curruntRecord[i].name.trim():'Blank Name Field',
           "FieldList":{
             "mlender":curruntRecord[i].mortgage_lender?curruntRecord[i].mortgage_lender.trim():null,
             "policyage":curruntRecord[i].policy_age?curruntRecord[i].policy_age:null,
             "rdate": curruntRecord[i].policy_renewal_start_date?curruntRecord[i].policy_renewal_start_date.trim():null,
             "edate":curruntRecord[i].policy_renewal_end_date?curruntRecord[i].policy_renewal_end_date.trim():null,
             "add1":curruntRecord[i].address_line_1?curruntRecord[i].address_line_1.trim():null,
             "add2":curruntRecord[i].address_line_2?curruntRecord[i].address_line_2.trim():null,
            
             "sfinsur":curruntRecord[i].show_flood_insurance_scene_y_n_?curruntRecord[i].show_flood_insurance_scene_y_n_.trim():null,
             "slinsur":curruntRecord[i].show_law_ordinance_scene_y_n_?curruntRecord[i].show_law_ordinance_scene_y_n_.trim():null,
             "spinsur":curruntRecord[i].show_portal_scene_y_n_?curruntRecord[i].show_portal_scene_y_n_.trim():null,
           }      
   
   
         }; 
       }
       else if(substype=='Onboarding' && Object.keys(curruntRecord[i]).includes('insured_street'))
       {
    
   var pnum='';
    var ch;
   if(curruntRecord[i].channel_type__email_.trim())
   {
   ch='EMAIL'
   }
   else if(pnum)
   {
     ch='SMS'
   }
   else{
     ch='EMAIL'
   }
   console.log("pnum",pnum)
         csvRecord = {
   
   
           ChannelType: curruntRecord[i].channel_type__email_?curruntRecord[i].channel_type__email_.trim().toUpperCase():ch,
   
           Email: curruntRecord[i].email?curruntRecord[i].email.trim():'Blank Email Field',
   
           Phone: pnum?pnum:'Blank Phone Field',
   
           Name:  curruntRecord[i].name?curruntRecord[i].name.trim():'Blank Name Field',
   
          // SubscriptionType : 'curruntRecord[4].trim()',
        
          "FieldList":{
        
          'PolicyEndDate':curruntRecord[i].policy_end_date?curruntRecord[i].policy_end_date.trim():null,
          'InsuredStreet':curruntRecord[i].insured_street?curruntRecord[i].insured_street.trim():null,
          'CDAmount':curruntRecord[i].coverage_dwellings_amount?curruntRecord[i].coverage_dwellings_amount:null,
          'COSAmount':curruntRecord[i].coverage_other_structures_amount?curruntRecord[i].coverage_other_structures_amount:null,
          'CPPAmount':curruntRecord[i].coverage_personal_property_amount?curruntRecord[i].coverage_personal_property_amount:null,
          'CLUAmount':curruntRecord[i].coverage_loss_of_use_amount?curruntRecord[i].coverage_loss_of_use_amount:null,
          'LCoverage':curruntRecord[i].liability_coverage__y_n_?curruntRecord[i].liability_coverage__y_n_.trim():null,
          'PLAmount':curruntRecord[i].personal_liability_amount?curruntRecord[i].personal_liability_amount:null,
          'MPAmount':curruntRecord[i].medical_payments_amount?curruntRecord[i].medical_payments_amount:null,
          'ChangesTotal':curruntRecord[i].changes_total?curruntRecord[i].changes_total:null,
          'CreditsTotal':curruntRecord[i].credits_total?curruntRecord[i].credits_total:null,
          'PTotal':curruntRecord[i].premium_total?curruntRecord[i].premium_total:null,
          'SDAmount':curruntRecord[i].standard_deductible_amount?curruntRecord[i].standard_deductible_amount:null,
          'HDAmount':curruntRecord[i].hurricane_deductible_amount?curruntRecord[i].hurricane_deductible_amount:null,
          'CState':curruntRecord[i].customer_state?curruntRecord[i].customer_state.trim():null,
          'LOIP':curruntRecord[i].law_and_ordinance_insurance_purchased?curruntRecord[i].law_and_ordinance_insurance_purchased:null,
          'FIP':curruntRecord[i].flood_insurance_purchased__y_n_?curruntRecord[i].flood_insurance_purchased__y_n_.trim():null,
          'IR':curruntRecord[i].inspection_required__y_n_?curruntRecord[i].inspection_required__y_n_.trim():null
       
         
          
          
        
          }
   
         }; 
   
       }
       else
       {
         return "error"
       }
     
   
         
   
         csvArr.push(csvRecord); 
   
      
   
     
   
    
  } 
}
  catch (error)
  {
  console.log("e11",error)
  return "error"; 
  }
  
    return csvArr; 
  
  } 

getDataRecordsArrayFromCSVFile(csvRecordsArray, headerLength,headersRow,substype) { 

  let csvArr = []; 
console.log("header",headersRow)
  try{
  for (let i = 1; i < csvRecordsArray.length; i++) { 

    let curruntRecord = (csvRecordsArray[i]).split(','); 
//if(curruntRecord[3])

//{

    if (curruntRecord.length == headerLength) { 
/*if( curruntRecord[1].trim()=='')
{
  message.error('One of the CSV has blank email')
}*/

let csvRecord

if(substype=='Renewals' && headersRow[4]=="Policy Age")
{
  var pnum;
/*  var code=process.env.REACT_APP_COUNTRYCODE;
var x=curruntRecord[3]?curruntRecord[3].trim():'';
var pnum;
if(x.length==10)
{
  pnum=code.toString()+x;
}
else if(x.length==11)
{
  pnum="+"+x;
}
else if(x.length==12)
{
  pnum="+"+x;
}
else{
  pnum=x;
}*/
var ch;
if(curruntRecord[2].trim())
{
ch='EMAIL'
}
else if(pnum)
{
  ch='SMS'
}
else{
  ch='EMAIL'
}
console.log("pnum",curruntRecord[7])
      csvRecord = {


        ChannelType: curruntRecord[1]?curruntRecord[1].trim():ch,

        Email: curruntRecord[2]?curruntRecord[2].trim():'Blank Email Field',

        Phone: pnum?pnum:'Blank Phone Field',

        Name: curruntRecord[0].trim(),
        "FieldList":{
          //"policyRenewalDate":  curruntRecord[4].trim()?curruntRecord[4].trim():null,
          //"PremiumAmount":  curruntRecord[5].trim()?curruntRecord[5].trim():null
          "mlender":curruntRecord[3]?curruntRecord[3].trim():null,
          "policyage":curruntRecord[4]?curruntRecord[4].trim():null,
          "rdate": curruntRecord[5]?curruntRecord[5].trim():null,
          "edate":curruntRecord[6]?curruntRecord[6].trim():null,
          "add1":curruntRecord[7]?curruntRecord[7].trim():null,
          "add2":curruntRecord[8]?curruntRecord[8].trim():null,
         
          "sfinsur":curruntRecord[9]?curruntRecord[9].trim():null,
          "slinsur":curruntRecord[10]?curruntRecord[10].trim():null,
          "spinsur":curruntRecord[11]?curruntRecord[11].trim():null,
        }
      // "Policy Renewal Date":  curruntRecord[4].trim(),
        //"Premium Amount":  curruntRecord[5].trim(),

    
       // SubscriptionType : 'curruntRecord[4].trim()',

     


      }; 
    }
    else if(substype=='Onboarding' && headersRow[4]=="Policy End Date")
    {
    /*  var code=process.env.REACT_APP_COUNTRYCODE;
var x=curruntRecord[3].trim();
var pnum;
if(x.length==10)
{
  pnum=code.toString()+x;
}
else if(x.length==11)
{
  pnum="+"+x;
}
else if(x.length==12)
{
  pnum="+"+x;
}
else{
  pnum=x;
}
*/
var pnum='';
 var ch;
if(curruntRecord[2].trim())
{
ch='EMAIL'
}
else if(pnum)
{
  ch='SMS'
}
else{
  ch='EMAIL'
}
console.log("pnum",pnum)
      csvRecord = {


        ChannelType: curruntRecord[1].trim().toUpperCase()?curruntRecord[1].trim():ch,

        Email: curruntRecord[2].trim()?curruntRecord[2].trim():'Blank Email Field',

        Phone: pnum?pnum:'Blank Phone Field',

        Name: curruntRecord[0].trim(),

       // SubscriptionType : 'curruntRecord[4].trim()',
     
       "FieldList":{
     
       'PolicyEndDate':curruntRecord[4].trim()?curruntRecord[4].trim():null,
       'InsuredStreet':curruntRecord[5].trim()?curruntRecord[5].trim():null,
       'CDAmount':curruntRecord[6].trim()?curruntRecord[6].trim():null,
       'COSAmount':curruntRecord[7].trim()?curruntRecord[7].trim():null,
       'CPPAmount':curruntRecord[8].trim()?curruntRecord[8].trim():null,
       'CLUAmount':curruntRecord[9].trim()?curruntRecord[9].trim():null,
       'LCoverage':curruntRecord[10].trim()?curruntRecord[10].trim():null,
       'PLAmount':curruntRecord[11].trim()?curruntRecord[11].trim():null,
       'MPAmount':curruntRecord[12].trim()?curruntRecord[12].trim():null,
       'ChangesTotal':curruntRecord[13].trim()?curruntRecord[13].trim():null,
       'CreditsTotal':curruntRecord[14].trim()?curruntRecord[14].trim():null,
       'PTotal':curruntRecord[15].trim()?curruntRecord[15].trim():null,
       'SDAmount':curruntRecord[16].trim()?curruntRecord[16].trim():null,
       'HDAmount':curruntRecord[17].trim()?curruntRecord[17].trim():null,
       'CState':curruntRecord[18].trim()?curruntRecord[18].trim():null,
       'LOIP':curruntRecord[19].trim()?curruntRecord[19].trim():null,
       'FIP':curruntRecord[20].trim()?curruntRecord[20].trim():null,
       'IR':curruntRecord[21].trim()?curruntRecord[21].trim():null
    
      
       
       
     
       }

      }; 

    }
    else
    {
      return "error"
    }
  

      

      csvArr.push(csvRecord); 

   
  }
  

  } 
} 
catch (error)
{
console.log("e11",error)
return "error"; 
}

  return csvArr; 

} 



isValidCSVFile(file) { 

  return file.name.endsWith(".csv"); 

}
async next() {
  const current = this.state.current + 1;
  console.log("next()",current);
  if(current==1)
  {
    if(this.props.location.state.form)
    {
      //this.setState({enduser:})
      this.submit();

      
    var params = {
      "CompanyId": localStorage.getItem('com'),
      "LoggedInUser": localStorage.getItem('UserIdinfo'),
      "SubscriptionType":this.props.location.state.subtype.toUpperCase(),
      "LoggedInUserName":localStorage.getItem('UserInitialsinfo'),
      "EndUsers" :this.state.enduser
     }
     this.setState({loading:true})
    console.log("params"+JSON.stringify(params));
    var x=[];
   var unflag='';

  // params.EndUsers[0].ChannelType=this.state.ctype;
    var res=await apiCall.postApi('enduser/upload', params);
    console.log("response",res.data.BatchId);
    // /BatchId: "ee9a22a4-f4cd-4026-af77-2fac5a8342b8"
    console.log("rdata",res.data.Items.length);

    if(res.data.Items.length==0)
    {
      //this.setState({initbuttondisabled:false})
     // message.error("Few of the required fields are missing please check");
    }
    else
    {
      this.setState({initbuttondisabled:false})
    var substatus=res.data.SubscriptionStatus;
    var domain=res.data.Domain;
    this.setState({substatus});
    this.setState({domain});
    var unsubcount=0;
    var eflag=0;
    var subcount=0;
    if(res.data.BatchId)
    {
      this.setState({loading:false})
   this.setState({batchid:res.data.BatchId})
   res.data.Items.forEach(function(obj) {
    if(obj.IsUnsubscribed=='TRUE')
    {
      unsubcount=unsubcount+1;
    unflag=true;
    }
    else{
      subcount=subcount+1;
    }
    console.log("phone",validator.allnumeric(obj.Phone))
    if((obj.ChannelType=="EMAIL")&&(validator.email(obj.Email) == false))
    {
      console.log('indise Email')
    obj.estatus=1;
    obj.emessage="Invalid Email Address";
    eflag=1
    }
    else if((obj.ChannelType=="EMAIL")&&(validator.email(obj.Email) == true)&&(substatus=="InActive"))
    {
      var e=obj.Email.split("@");
      if(e[1]!=domain)
      {
        obj.estatus=1;
        obj.emessage="Cross Domain Email Address Not Allowed please delete";
        eflag=1;
      }   
    

    }
    else if((obj.ChannelType=="SMS")&&(validator.allnumeric(obj.Phone) == true)&&(substatus=="InActive"))
    {
      obj.estatus=1;
      obj.emessage="No SMS Channel Type Is Allowed";
      eflag=1;
    }
    else if((obj.ChannelType=="SMS")&&(validator.allnumeric(obj.Phone) == false))
    {
    obj.estatus=1;
    console.log('indise phone')
    obj.emessage="Invalid Phone Number";
    eflag=1;
    }
    else{
      obj.estatus=0;
      eflag=0;
    }
    
    x.push(obj);
     });
     this.setState({errstatus:eflag})
     this.setState({unsubcount});
     this.setState({subcount});
     this.setState({unflag});

     var efilter=[]
     x.forEach((obj)=>{
       if(obj.estatus==1)
       {
         efilter.push(obj);
       }
     });
     console.log("efilter",efilter);
     this.setState({efilter:efilter});
     this.setState({res:efilter});
     this.setState({initenduser:x});
     //this.setState({enduser:''});
     this.setState({showprevious:false})
     this.setState({nextflag:false});
     console.log("eeef",eflag);
if(eflag==1)
{
  this.setState({erflag:true})

}
else
{
  if(x.length>1)
  {
 // var y=x.splice(0, 1);
  //x=x.splice(0, 1)
  console.log("value of x",x);
 delete  x[0];
 this.setState({res:x});
  }
  else{
  console.log("value of x",x);
  this.setState({res:x});
  this.setState({erflag:false})
  }
  
}


      }
      message.success("End User Added Successfully");
   
    }
    
  }
  this.setState({ current });

  }
  else if(this.state.nextflag==true)
  {
    message.error("No data found");
   // this.setState({showprevious:true})
  }
 else if(this.state.errstatus==0 && current==2) 
  {
  const current = this.state.current + 1;
  this.setState({loading:false})
 
 
  
  this.setState({ current });
}

else{
  message.error("There are errors in records please review");
  
}

}

prev() {
  const current = this.state.current - 1;
  this.setState({ current });
console.log("current",current);
/*if(current==0)
{
  this.setState({ctype:''})
  this.setState({rdate:''})
  this.setState({InsuredStreet:''})
  this.setState({CDAmount:''})
  this.setState({COSAmount:''})
  this.setState({CPPAmount:''})
  this.setState({CLossAmount:''})
  this.setState({lcoverage:''})
  this.setState({PLAmount:''})
  this.setState({MPAmount:''})
  this.setState({changeTotal:''})
  this.setState({creditsTotal:''})
  this.setState({PTotal:''})
  this.setState({SDAmount:''})
  this.setState({HDAmount:''})
  this.setState({CustomerState:''})
  this.setState({LOIPType:''})
  this.setState({FIPType:''})
  this.setState({IRType:''})
  this.setState({Name:''})
  this.setState({Email:''})



}*/

}
showmodal= async (x,flag)=>{
 console.log("ssd",x)
 if(!flag)
 {
   this.setState({form:false})
 var params = {
  "TransactionId": x,
  "TemplateName": "Wardlaw_Email_Template_v1"
  
 }
 //var res=await axios.post('https://yi70tn8ms6.execute-api.us-east-1.amazonaws.com/dev/api/v1/enduser/EmailPreview', params);
//var h=res.data
var res=await apiCall.postApi('enduser/EmailPreview',params);
//var h=res.data
//console.log("12aa",res2);
var dataURI = 'data:text/html,' + encodeURIComponent(res.data);
var parser = new DOMParser();
		var doc = parser.parseFromString(res.data, 'text/html');
 this.setState({html:dataURI});
 console.log("HTML",doc.body);
  this.setState({dvisible:true});
}
else
{
  
 
 
  this.setState({eid:x})

  this.setState({form:true})
  this.setState({dvisible:true});
  var editd='';

  var s=await search.searchText(this.state.initenduser,x);
  console.log("2search",s);
  this.setState({edata:s[0]});


//this.setState({edata:editd});
this.setState({Name:s[0].Name})
this.setState({email:s[0].Email})
this.setState({phone:s[0].Phone})
this.setState({ctype:s[0].ChannelType})

this.setState({FieldList:s[0].FieldList})
console.log("editd",s)
console.log("edata",this.state.edata.name)
}
}

async sendnoti()
{
  
  this.setState({loading:true})
  var params2 = {
    "CompanyId": localStorage.getItem('com'),
    "BatchId":this.state.batchid
   }
  // console.log("params"+JSON.stringify(params));
  try{
  //var res=await axios.post('https://yi70tn8ms6.execute-api.us-east-1.amazonaws.com/dev/api/v1/enduser/sendnotifications', params2);
  var res=await apiCall.postApi('enduser/sendnotifications',params2);
  message.success('Notification sent')
  this.setState({loading:false})
  this.setState({collapsed:true});
  this.setState({showprevious:false})
  this.setState({bdisabled:true});
  if(res)
  {

localStorage.setItem('name','');
localStorage.setItem('ctype','');
localStorage.setItem('atype','');
localStorage.setItem('email','');
localStorage.setItem('phone','');
localStorage.setItem('rdate','');
localStorage.setItem('CDAmount','');
localStorage.setItem('CLossAmount','');
localStorage.setItem('COSAmount','');
localStorage.setItem('CPPAmount','');
localStorage.setItem('CustomerState','');
localStorage.setItem('FIPType','');
localStorage.setItem('HDAmount','');
localStorage.setItem('IRType','');
localStorage.setItem('InsuredStreet','');
localStorage.setItem('LOIPType','');
localStorage.setItem('MPAmount','');
localStorage.setItem('PLAmount','');
localStorage.setItem('PTotal','');
localStorage.setItem('SDAmount','');
localStorage.setItem('changeTotal','');
localStorage.setItem('creditsTotal','');
localStorage.setItem('lcoverage','');
localStorage.setItem('mlender','');
localStorage.setItem('policyage','');
localStorage.setItem('rdate','');
localStorage.setItem('edate','');
localStorage.setItem('add1','');
localStorage.setItem('add2','');
//localStorage.setItem('pstreet','');
localStorage.setItem('sfinsur','');
localStorage.setItem('slinsur','');
localStorage.setItem('spinsur','');



  var encrypt =await apiCall.putApi(`enduser/encrypt/company/${localStorage.getItem('com')}/batch/${this.state.batchid}`);
  }
 
  }
  catch(err)
  {
  console.log("err",err);
message.success('Notification sent')
this.setState({loading:false})
this.setState({collapsed:true});
this.setState({bdisabled:true});
  }
}
handleChange = event => {
  this.setState({
        [event.target.id]: event.target.value
    });


};

handleForce = async (data, fileInfo) =>{
  //alert("here");
    console.log("1csvdata",data);
    console.log('fd',fileInfo);
    var vaildcsv=this.isValidCSVFile(fileInfo)
    console.log("valid",vaildcsv);
    
    if(localStorage.getItem('com')!='All'&&localStorage.getItem('com'))
    {
      if(vaildcsv)
    {
    var fname=fileInfo.name;
   // let reader = new FileReader(); 
//  console.log('Filename',fname);
   //    reader.readAsText(event.target.files[0]); 
        this.setState({loading:true});
        
    var substype=this.state.substate;
    console.log("getdata",substype);
    //    reader.onload = async () => { 
  
      //    let csvData = reader.result; 
  
      //    let csvRecordsArray = (csvData).split(/\r\n|\n/); 
  
        
        // let headersRow = this.getHeaderArray(csvRecordsArray); 
        
 //   console.log('header',headersRow);
         var uploadGLRecords = this.getDataRecords(data,substype); 
         console.log("p1",uploadGLRecords)
         if(uploadGLRecords!="error")
         {
         console.log("uploadGLRecords"+JSON.stringify(uploadGLRecords))
         this.setState({enduser:uploadGLRecords})
      console.log("uid"+localStorage.getItem("userId"));
        // var x=[];
         var params = {
          "CompanyId": localStorage.getItem('com'),
          "LoggedInUser": localStorage.getItem('UserIdinfo'),
          "SubscriptionType":this.props.location.state.subtype.toUpperCase(),
          "LoggedInUserName":localStorage.getItem('UserInitialsinfo'),
          "EndUsers" :uploadGLRecords
         }
        console.log("params"+JSON.stringify(params))
       
       this.setState({loading:true})
      // console.log("params"+JSON.stringify(params));
      var x=[];
      var unflag='';
      var unsubcount=0;
      var subcount=0;
      var eflag=0;
      try{
      var res=await apiCall.postApi('enduser/upload', params);
      console.log("response2",res);
      
      // /BatchId: "ee9a22a4-f4cd-4026-af77-2fac5a8342b8"
      if(res.data.BatchId)
      {
        this.setState({loading:false})
     this.setState({batchid:res.data.BatchId})
     var substatus=res.data.SubscriptionStatus;
     var domain=res.data.Domain;
     this.setState({substatus})
     this.setState({domain})
     res.data.Items.forEach(function(obj) {
       obj.errstatus=0
  if(obj.IsUnsubscribed=='TRUE')
  {
  
    unsubcount=unsubcount+1;
      unflag=true;
      }
      else{
        subcount=subcount+1;
      }
      console.log("2phone",validator.allnumeric(obj.Phone))
      if((obj.ChannelType=="EMAIL")&&(validator.email(obj.Email) == false))
      {
      obj.estatus=1;
      obj.emessage="Invalid Email Address";
      eflag=1;
      }
      else if((obj.ChannelType=="EMAIL")&&(validator.email(obj.Email) == true)&&(substatus=="InActive"))
      {
        var e=obj.Email.split("@");
        if(e[1]!=domain)
        {
          obj.estatus=1;
          obj.emessage="Cross Domain Email Address Not Allowed please delete";
          eflag=1;
        }   
      
  
      }
      else if((obj.ChannelType=="SMS")&&(validator.allnumeric(obj.Phone) == true)&&(substatus=="InActive"))
      {
        obj.estatus=1;
        obj.emessage="No SMS Channel Type Is Allowed";
        eflag=1;
      }
      
      else if((obj.ChannelType=="SMS")&&(validator.allnumeric(obj.Phone) == false))
      {
        eflag=1;
      obj.estatus=1;
      obj.emessage="Invalid Phone Number";
      
      }
      else
      {
        obj.estatus=0;
        eflag=0;
      }
      
      
    x.push(obj);
     });
     this.setState({errstatus:eflag})
     console.log("errstatus",eflag);
     this.setState({unsubcount});
     this.setState({subcount});
     this.setState({unflag});
     var efilter=[]
   /*  x.forEach((obj)=>{
       if(obj.estatus==1)
       {
         efilter.push(obj);
       }
     });*/
     var s=await search.searchValue(x,1,"estatus");
     console.log("search",s);
     this.setState({res:s});
  
     console.log("efilter",efilter);
     this.setState({efilter:s});
     this.setState({res:s});
    // this.setState({res:x});
     this.setState({initenduser:x});
     console.log("reserror",this.state.res);
      }
  
         /*test*/
  
    var s=await search.searchValue(this.state.initenduser,1,"estatus");
    console.log("search",s);
    this.setState({res:s});
    this.setState({initbuttondisabled:false})
    this.setState({fname})
  
    if(eflag==1)
    {
      this.setState({erflag:true})
    
    }
    else
    {
      this.setState({res:x});
      this.setState({erflag:false})
    }
    
    setTimeout(()=>{
  this.setState({loading:false});
  message.success('File Processed')
    },2000)
  }
    catch (err)
    {
      this.setState({fname})
      setTimeout(()=>{
    this.setState({loading:false});
    message.error('Unable to process file please try again')
      },2000)
    }
    }
    else
    {
      setTimeout(()=>{
        this.setState({loading:false});
        message.error('File processing error invalid columns found in CSV')
          },2000)
          
    }
  
    /*test*/
        //  }
  
  
  
  
  }
  
  
  
  
        
        else
        {
          message.error("Invalid file extension please upload a CSV file");
        }
      }
        else
        {
          message.error('Select Company')
        }
      
  
  
  
  }

submit=()=>{
 
  if(localStorage.getItem('com')&&localStorage.getItem('com')!=='All')
  {
var x=[]
x=this.state.enduser;
var eflag=0;
if(this.state.ctype!='')
{
//x=this.state.enduser;
var params
/*var params= {
  ChannelType: this.state.ctype.trim(),

Email: this.state.email.trim(),

Phone: this.state.phone.trim(),

Name: this.state.name.trim(),

"FieldList":{
  "policyRenewalDate": this.state.rdate,
  "PremiumAmount":  this.state.atype

}

}*/
var flag=0
if(this.state.ctype=='EMAIL'&& this.state.email)
{
  flag=1
}
else if(this.state.ctype=='SMS'&& this.state.phone)

{
flag=1;
}

if(flag==1)
{
if(this.props.location.state.subtype=='Onboarding'&&  this.state.name&& this.state.edate &&this.state.CustomerState&&this.state.PTotal&&this.state.InsuredStreet)
{
 // alert('here');
 if(this.state.phone)
 {
  var pinit=this.state.phone?this.state.phone.trim():this.state.phone;
  var pnum;
  var code=process.env.REACT_APP_COUNTRYCODE;
  if(pinit.length==10)
  {
    pnum=code+pinit;
  }
  else if(pinit.length==11)
{
  pnum="+"+pinit;
}
else if(pinit.length==12)
{
  pnum="+"+x;
}
  else{
    pnum=pinit;
  }

 }
  params= {



    ChannelType: this.state.ctype.trim(),
  
  Email: this.state.email?this.state.email.trim():"Blank Email Field",
  
  Phone: pnum?pnum:'Blank Phone Field',
  
  Name: this.state.name.trim(),
  
  "FieldList":{
    'PolicyEndDate':this.state.edate,
    'InsuredStreet':this.state.InsuredStreet.trim(),
    'CDAmount':this.state.CDAmount,
    'COSAmount':this.state.COSAmount,
    'CPPAmount':this.state.CPPAmount,
    'CLUAmount':this.state.CLossAmount,
    'LCoverage':this.state.lcoverage,
    'PLAmount':this.state.PLAmount,
    'MPAmount':this.state.MPAmount,
    'ChangesTotal':this.state.changeTotal,
    'CreditsTotal':this.state.creditsTotal,
    'PTotal':this.state.PTotal,
    'SDAmount':this.state.SDAmount,
    'HDAmount':this.state.HDAmount,
    'CState':this.state.CustomerState,
    'LOIP':this.state.LOIPType,
    'FIP':this.state.FIPType,
    'IR':this.state.IRType
  
  }
}
//this.setState({initbuttondisabled:})
}
else if(this.props.location.state.subtype=='Renewals' &&  this.state.name&& this.state.policyage&& this.state.rdate&&this.state.edate&&this.state.add1&&this.state.sfinsur&&this.state.slinsur&&this.state.spinsur)
//else if(this.props.location.state.subtype=='Renewals'&&this.state.rdate&&this.state.atype)
{
  var pinit=this.state.phone?this.state.phone.trim():'';
var pnum;
var code=process.env.REACT_APP_COUNTRYCODE;
if(pinit.length==10)
{
  pnum=code+pinit;
}
else if(pinit.length==11)
{
  pnum="+"+pinit;
}
else if(pinit.length==12)
{
  pnum="+"+x;
}
else{
  pnum=pinit;
}
  params= {
    ChannelType: this.state.ctype.trim(),
  
    Email: this.state.email?this.state.email.trim():"Blank Email Field",
  
    Phone: pnum?pnum:'Blank Phone Field',
  
  Name: this.state.name.trim(),
  
  "FieldList":{
    "mlender":this.state.mlender,
    "policyage":this.state.policyage,
    "rdate": this.state.rdate,
    "edate":this.state.edate,
    "add1":this.state.add1,
    "add2":this.state.add2,
    //"pstreet":this.state.pstreet,
    "sfinsur":this.state.sfinsur,
    "slinsur":this.state.slinsur,
    "spinsur":this.state.spinsur
  
  }
}
}
else
{
  eflag=1;
 // message.error("Few of the required fields are missing please review.")
}

//message.success("End User Added Successfully");
//this.setState({phone:''})
//this.setState({email:''})
//this.setState({Name:''})
///this.setState({ctype:''})
//this.setState({stype:''})
//console.log("edata",this.state.enduser)
if(eflag==0)
{
  x.push(params)
console.log("params",x)
this.setState({enduser:x})
this.setState({initbuttondisabled:true})
}
else
{
  this.setState({initbuttondisabled:false})
  message.error("Few of the required fields are missing please review.")
}


/*----*/
}
else
{
  if(!this.state.phone && this.state.ctype=='SMS')
  {
    message.error("Phone is a require field as Channel Type is SMS")
  }
  if(!this.state.email && this.state.ctype=='EMAIL')
  {
    message.error("Email is a require field as Channel Type is EMAIL")
  }
}

  }

else
{
  this.setState({initbuttondisabled:false})
message.error("Few of the required fields are missing please review.")
}
}
  else
  {
    message.error("Please Select A Company");
  }

}
lang=event=>{
  this.setState({lang:event})
}
channelType=event=>{
  this.setState({ctype:event})
}
subType=event=>{
  this.setState({stype:event})
}

delete=async (tid)=>{
var x=this.state.initenduser;
  var res=await apiCall.delete('enduser',tid);
  console.log('delete res',res);
if(res.data.message)
{
 // console.log('delete res',res);


var y=[]
x.forEach(function(obj) {
 console.log("dobj",obj)
if(obj.TransactionId!=tid)
{
  y.push(obj);
  console.log("push",y)

}

   });
var efilter=[];
var nofilter=[];
var unsubcount=0;
var unflag;
var erflag=true;
var subcount=0
   y.forEach((obj)=>{
    if(obj.estatus==1)
    {
      
      efilter.push(obj);
    }
    else
    {
   
      nofilter.push(obj);
    }
    if(obj.IsUnsubscribed=='TRUE')
{

  unsubcount=unsubcount+1;
    unflag=true;
    }
    else{
      subcount=subcount+1;
    }
  });

   console.log("initdelete",y);
   if(efilter.length>0)
   {
    this.setState({erflag:true})
   this.setState({res:efilter});
   }
   else
   {
    this.setState({errstatus:0})
    this.setState({erflag:false})
    this.setState({res:nofilter});
   }
   this.setState({initenduser:y});
   this.setState({unsubcount});
   this.setState({subcount});
   this.setState({unflag})
console.log("lencount",y.length);
   console.log("erflag",this.state.erflag);
   message.success("User deleted successfully");
if(y.length==0)
{
this.setState({nextflag:true});
}
console.log("nextflag",this.state.nextflag)
}
else{
  message.error('unable to delete user');
}


}
initErr=async (checked)=>{
  this.setState({erflag:checked})
  console.log("inituser",this.state.initenduser)
 // this.setState({res:this.state.initenduser});
 console.log("here",this.state.errstatus)
 var eflag=0;
if(!checked)
{
var up=this.state.initenduser;
var x=[]
var substatus=this.state.substatus;
var domain=this.state.domain;
up.forEach((obj)=>{
  if((obj.ChannelType=="EMAIL")&&(validator.email(obj.Email) == false))
    {
    obj.estatus=1;
    obj.emessage="Invalid Email Address";
    eflag=1
    }
    else if((obj.ChannelType=="EMAIL")&&(validator.email(obj.Email) == true)&&(substatus=="InActive"))
    {
      var e=obj.Email.split("@");
      if(e[1]!=domain)
      {
        obj.estatus=1;
        obj.emessage="Cross Domain Email Address Not Allowed please delete";
        eflag=1;
      }   
    

    }
    else if((obj.ChannelType=="SMS")&&(validator.allnumeric(obj.Phone) == true)&&(substatus=="InActive"))
    {
      obj.estatus=1;
      obj.emessage="No SMS Channel Type Is Allowed";
      eflag=1;
    }
    
    else if((obj.ChannelType=="SMS")&&(validator.allnumeric(obj.Phone) == false))
    {
    obj.estatus=1;
    obj.emessage="Invalid Phone Number";
  eflag=1;
    }
    else{
      obj.estatus=0;
      eflag=0;
    }
    
    x.push(obj);
})
this.setState({errstatus:eflag})
  this.setState({res:x});
}
else
{
 
var s=await search.searchValue(this.state.initenduser,1,"estatus");
  console.log("search",s);
  this.setState({res:s});
}
}

submitForm=event=>{
  const{fields,...inputFields}=this.state;
  console.log("input data",inputFields);
  console.log("einput data",fields);
event.preventDefault();
}
_handleChange=event=>{
 // console.log("einput",JSON.stringify(event))
this.setState({
[event.currentTarget.name]:event.currentTarget.value

});
localStorage.setItem(event.currentTarget.name,event.currentTarget.value)
//console.log("qqqq",localStorage.getItem(event.target.id));
}
_handleDateChange=event=>{
  console.log("22date",event);
  this.setState({rdate:event})
  localStorage.setItem('rdate',event)

}
_handleDateChange2=event=>{
  console.log("33date",event);
this.setState({edate:event})
localStorage.setItem('edate',event)

}
_handleDropdownChange=event=>{
 if(event!=="YES"||event!=="NO") 
 {
  this.setState({ctype:event})
 }
 else{
   this.setState({atype:event})
 }
}



render()
{
  /*const headers = [
    { label: "First Name", key: "firstname" },
    { label: "Last Name", key: "lastname" },
    { label: "Email", key: "email" }
  ];*/
try
{
  const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 16 },
  };
  let {header}=this.state;
  let {erflag}=this.state;
  console.log("2header",header)
  const data = [
 
  ];
 const{fields}=this.state;
  let styleScroll
  //if()
  let {subtype}=this.props.location.state;
  if( this.props.location.state.subtype=='Renewals')
  {
   // styleScroll={x: 0, y: 300 }
   if(this.state.hideNav)
   {
    styleScroll={x: 2300, y: 300 }
   }
   else{
    styleScroll={x: 2300, y: 300 }
   }
  }
  else
  {
    styleScroll={x: 3800, y: 300 }
  }
  let columns;
  if(this.props.location.state.subtype=='Renewals')
  {
  columns = [
          {
    
            key: 'Name',
            title: 'Name',
            dataIndex: 'Name'
            },
           
           
              {
                key: 'ChannelAddress',
                title: 'To',
                dataIndex: 'ChannelAddress'
                },
                {
                  key: 'ChannelType',
                  title: 'Channel Type',
                  dataIndex: 'ChannelType'
                  },
                  {
                    key: 'mlender',
                    title: 'Mortgage Lender',
                    render: (text, item) => (
                      <span>
                      {item.FieldList.mlender} 
                      </span>
                    ),
                    },
                    {
                      key: 'policyage',
                      title: 'Policy Age',
                      render: (text, item) => (
                        <span>
                        {item.FieldList.policyage} 
                        </span>
                      ),
                      },
                  {
                  // company: 'Company Name',
                  // Adminname: 'company',
                  key: 'rdate',
                  title: 'Policy Renewal Start Date',
                  render: (text, item) => (
                    <span>
                   {moment(new Date(item.FieldList.rdate)).format("DD MMM YYYY")}
                    </span>
                  ),
                  },
                  {
                    // company: 'Company Name',
                    // Adminname: 'company',
                    key: 'edate',
                    title: 'Policy Renewal End Date',
                    render: (text, item) => (
                      <span>
                     {moment(new Date(item.FieldList.edate)).format("DD MMM YYYY")}
                      </span>
                    ),
                    },
                  
                  
                  
                  {
                    // company: 'Company Name',
                    // Adminname: 'company',
                    key: 'add1',
                    title: 'Adress Line 1',
                    render: (text, item) => (
                      <span>
                      {item.FieldList.add1}
                      </span>
                    ),
                    },
                    {
                      // company: 'Company Name',
                      // Adminname: 'company',
                      key: 'add2',
                      title: 'Adress Line 2',
                      render: (text, item) => (
                        <span>
                        {item.FieldList.add2}
                        </span>
                      ),
                      },
                      {
                        // company: 'Company Name',
                        // Adminname: 'company',
                        key: 'sfinsur',
                        title: 'Show Flood InsuranceScene',
                        render: (text, item) => (
                          <span>
                          {item.FieldList.sfinsur} 
                          </span>
                        ),
                        },
                        {
                          // company: 'Company Name',
                          // Adminname: 'company',
                          key: 'slinsur',
                          title: 'Show Law Ordinance Scene',
                          render: (text, item) => (
                            <span>
                            {item.FieldList.slinsur} 
                            </span>
                          ),
                          },
                          {
                            // company: 'Company Name',
                            // Adminname: 'company',
                            key: 'spinsur',
                            title: 'Show Portal Scene',
                            render: (text, item) => (
                              <span>
                              {item.FieldList.spinsur} 
                              </span>
                            ),
                            },

                  {
    
                    key: 'IsUnsubscribed',
                    title: 'Subscribed',
                    render: (text, record) => (
                      <span>
                    {record.IsUnsubscribed=='FALSE'? <CheckCircleOutlined style={{fontSize: "23px",
          color: "green"
      }}/>:<IssuesCloseOutlined style={{fontSize: "23px",
      color: "red"
      }}/>
                     }
                      </span>
                      
                    ),
                    },
              {
              key: 'error',
      title: 'Error Status',
      render: (text, record) => (
        <span style={{float:""}}>{record.estatus ?<Popover content={record.emessage} title=""> <span><WarningOutlined style={{fontSize: "23px",
      color: "red"
      }}/></span></Popover>:<span><CheckCircleOutlined style={{fontSize: "23px",
      color: "green"
      }}/></span>}</span>
      ),
      },
             
             
                {
                  title: 'Action',
                  key: 'action',
                  fixed: 'right',
                  render: (text, record) => (
                  
                     <span>
                        <Tooltip title="Preview Template">
                  <EyeOutlined
                    style={{ fontSize: "15px", color: "#1890ff" }}
                    onClick={()=>this.showmodal(record.TransactionId,false)}
                  />
                </Tooltip>
                <Tooltip title="Edit Enduser Details">
                  <EditOutlined
                    style={{ fontSize: "15px", color: "#1890ff" }}
                    onClick={()=>this.showmodal(record.TransactionId,true)}
                  />
                </Tooltip>
            
                <Tooltip title="Delete Enduser">
                  <DeleteOutlined
                    style={{ fontSize: "15px", color: "red" }}
                    onClick={()=>this.delete(record.TransactionId)}
                  />
                </Tooltip>
      
      </span>
                    
                
                    
                  ),
                }
                

  ];
}
else if(this.props.location.state.subtype=='Onboarding')
{
  columns = [
    {

      key: 'Name',
      title: 'Name',
      dataIndex: 'Name'
      },
     
     
        {
          key: 'ChannelAddress',
          title: 'To',
          dataIndex: 'ChannelAddress'
          },
          {
            key: 'ChannelType',
            title: 'Channel Type',
            dataIndex: 'ChannelType'
            },
            {
              // company: 'Company Name',
              // Adminname: 'company',
              key: 'PolicyEndDate',
              title: 'Policy End Date',
              render: (text, item) => (
                <span>
                {moment(new Date(item.FieldList.PolicyEndDate)).format("DD MMM YYYY")}
                </span>
              ),
              },{
                // company: 'Company Name',
                // Adminname: 'company',
                key: 'InsuredStreet',
                title: 'Insured Street',
                render: (text, item) => (
                  <span>
                  {item.FieldList.InsuredStreet} 
                  </span>
                ),
                },{
                  key: 'CDAmount',
                  title: 'Coverage Dwellings Amount',
                  render: (text, item) => (
                    <span>
                    {item.FieldList.CDAmount} 
                    </span>
                  ),
                  },
                  {
                    key: 'COSAmount',
                    title: 'Coverage Other Structures Amount',
                    render: (text, item) => (
                      <span>
                      {item.FieldList.COSAmount} 
                      </span>
                    ),
                    },{
                      key: 'CPPAmount',
                      title: 'Coverage Personal Property Amount',
                      render: (text, item) => (
                        <span>
                        {item.FieldList.CPPAmount} 
                        </span>
                      ),
                      },{
                        key: 'CLUAmount',
                        title: 'Coverage Loss Of Use Amount',
                        render: (text, item) => (
                          <span>
                          {item.FieldList.CLUAmount} 
                          </span>
                        ),
                        },{
                          key: 'LCoverage',
                          title: 'Liability Coverage (Y/N)',
                          render: (text, item) => (
                            <span>
                            {item.FieldList.LCoverage} 
                            </span>
                          ),
                          },{
                            key: 'PLAmount',
                            title: 'Personal Liability Amount',
                            render: (text, item) => (
                              <span>
                              {item.FieldList.PLAmount} 
                              </span>
                            ),
                            },{
                              key: 'MPAmount',
                              title: 'Medical Payments Amount',
                              render: (text, item) => (
                                <span>
                                {item.FieldList.MPAmount} 
                                </span>
                              ),
                              },{
                                key: 'ChangesTotal',
                                title: 'Changes Total',
                                render: (text, item) => (
                                  <span>
                                  {item.FieldList.ChangesTotal} 
                                  </span>
                                ),
                                },{
                                  key: 'CreditsTotal',
                                  title: 'Credits Total',
                                  render: (text, item) => (
                                    <span>
                                    {item.FieldList.CreditsTotal} 
                                    </span>
                                  ),
                                  },{
                                    key: 'PTotal',
                                    title: 'Premium Total',
                                    render: (text, item) => (
                                      <span>
                                      {item.FieldList.PTotal} 
                                      </span>
                                    ),
                                    },{
                                      key: 'SDAmount',
                                      title: 'Standard Deductible Amount',
                                      render: (text, item) => (
                                        <span>
                                        {item.FieldList.SDAmount} 
                                        </span>
                                      ),
                                      },{
                                        key: 'HDAmount',
                                        title: 'Hurricane Deductible Amount',
                                        render: (text, item) => (
                                          <span>
                                          {item.FieldList.HDAmount} 
                                          </span>
                                        ),
                                        },{
                                          key: 'CState',
                                          title: 'Customer State',
                                          render: (text, item) => (
                                            <span>
                                            {item.FieldList.CState} 
                                            </span>
                                          ),
                                          },{
                                            key: 'LOIP',
                                            title: 'Law and Ordinance Insurance Purchased',
                                            render: (text, item) => (
                                              <span>
                                              {item.FieldList.LOIP} 
                                              </span>
                                            ),
                                            },{
                                              key: 'FIP',
                                              title: 'Flood Insurance Purchased',
                                              render: (text, item) => (
                                                <span>
                                                {item.FieldList.FIP} 
                                                </span>
                                              ),
                                              },{
                                                key: 'IR',
                                                title: 'Inspection Required',
                                                render: (text, item) => (
                                                  <span>
                                                  {item.FieldList.IR} 
                                                  </span>
                                                ),
                                                },
  


            {

              key: 'IsUnsubscribed',
              title: 'Subscribed',
              render: (text, record) => (
                <span>
              {record.IsUnsubscribed=='FALSE'? <CheckCircleOutlined style={{fontSize: "23px",
    color: "green"
}}/>:<IssuesCloseOutlined style={{fontSize: "23px",
color: "red"
}}/>
               }
                </span>
                
              ),
              },
        {
        key: 'error',
title: 'Error Status',
render: (text, record) => (
  <span style={{float:""}}>{record.estatus ?<Popover content={record.emessage} title=""> <span><WarningOutlined style={{fontSize: "23px",
color: "red"
}}/></span></Popover>:<span><CheckCircleOutlined style={{fontSize: "23px",
color: "green"
}}/></span>}</span>
),
},
       
       
          {
            title: 'Action',
            key: 'action',
            fixed: 'right',
            render: (text, record) => (
            
               <span>
                  <Tooltip title="Preview Template">
            <EyeOutlined
              style={{ fontSize: "15px", color: "#1890ff" }}
              onClick={()=>this.showmodal(record.TransactionId,false)}
            />
          </Tooltip>
          <Tooltip title="Edit Enduser Details">
            <EditOutlined
              style={{ fontSize: "15px", color: "#1890ff" }}
              onClick={()=>this.showmodal(record.TransactionId,true)}
            />
          </Tooltip>
      
          <Tooltip title="Delete Enduser">
            <DeleteOutlined
              style={{ fontSize: "15px", color: "red" }}
              onClick={()=>this.delete(record.TransactionId)}
            />
          </Tooltip>

</span>
              
          
              
            ),
          }
          

];
}
 
  const steps = [
    {
      title: this.props.location.state.form ?'Step 1 - Enter End-User Details':'Step 1 - Upload',
      content: this.props.location.state.form?<Card    
      style={{ marginTop: 12}}
      type="inner"
      title=""
       title={<div style={{    marginTop: "-11px",
        paddingBottom: "9px"}}>End-user Registration Form</div>}
    >
<div style={{marginLeft: "4px"}}>
        <Form   {...layout} autocomplete="off" onSubmit={this.submitForm}> 
     {fields.map(form=>{
       console.log("form",form);
       if(form.input_type=='text')
     {
      var v=form.name
      if(this.state[v])
      {
       return(
      
      
        <InputTextField class="fcust2"
        name={form.name}
        ltext={form.required?<span>{form.header}<span style={{color:"red"}}> *</span></span>:form.header}
        require={form.required}
        placeholder=''
        key={form.placeholder}
        values={localStorage.getItem(v)}
        wide={this.state.hideNav==true?145:560}
        _handleChange={this._handleChange}
        />
      
       



       )
      }
      else
      {
        return(
      
        
          <InputTextField
          name={form.name}
          ltext={form.required?<span>{form.header}<span style={{color:"red"}}> *</span></span>:form.header}
          require={form.required}
          placeholder=''
          key={form.placeholder}
          values={localStorage.getItem(form.name)}
          wide={this.state.hideNav==true?145:560}
          _handleChange={this._handleChange}
          />
       
        )
      }
     }
     if(form.input_type=='dropdown')
     {
      
       return(
       
    
        <Dropdown
        name={form.name}
        ltext={form.required?<span>{form.header}<span style={{color:"red"}}> *</span></span>:form.header}
        require={form.required}
        placeholder=''
        val={form.values}
        key={form.placeholder}
        wide={this.state.hideNav==true?145:560}
        defaultval={localStorage.getItem(form.name)?localStorage.getItem(form.name):form.default}
        _handleChange={this._handleChange}
        />
    



       )
     }
     if(form.input_type=='date')
     {
       return(
       
     
        <Datepicker
        name={form.name}
        ltext={form.required?<span>{form.header}<span style={{color:"red"}}> *</span></span>:form.header}
        require={form.required}
        placeholder=''
        key={form.placeholder}
        wide={this.state.hideNav==true?145:560}
        defaultval={localStorage.getItem(form.name)}
        _handleChange={form.name=='rdate'?this._handleDateChange:this._handleDateChange2}
        />
      


       )
     }

     })}
  
    
</Form>



</div>
    </Card>:<Card     
      style={{ marginTop: "16",
     
        border: "1px",
       borderStyle:"dashed",
        backgroundColor: "#fafafa"
      
      }}
      type="inner"
      title=""
   
    ><div style={{marginLeft: "473px",
      fontSize: "52px"}}><FileExcelTwoTone /></div><div class="upload-btn-wrapper" style={{marginLeft: "432px"}}>
    <button class="btn"  >  <CSVReader
      cssClass="react-csv-input"
      onFileLoaded={this.handleForce}
      parserOptions={papaparseOptions}
    /><UploadOutlined />  Upload CSV</button>
    {/*<input type="file" name="file" id="file" onChange={this.onChangeHandler}/>*/ }
    {this.state.fname ?(<p style={{marginLeft: "21px"}}><CheckCircleFilled  style={{color:"green"}}/>  {this.state.fname}</p>):<p></p>}
    {this.state.loading?(<p><div className="example">
    <Spin />
  </div></p>):<p></p>}
    </div></Card>,
    },
    {
      title: 'Step 2 - Review',
      content: 
     
      <Card  style={{ marginTop: "16",
     
      border: "0px",
     
      backgroundColor: "#fff"
    
    }}>{this.state.hideNav==true?<p></p>:<div style={{    marginTop: "-13px",
        marginBottom: "10px",float:"right"}}><div style={{marginLeft:"-158px"}}><FilterOutlined /> Error Filter <Switch checked={this.state.erflag}  onChange={this.initErr} /> </div><div style={{marginTop:"-22px"}}><SettingOutlined  /> Unsubscribe Filter <Switch defaultunChecked onChange={this.onChange} /></div></div>}<div style={{marginLeft: "-8px"}}>
<Table className="gx-table-responsive" dataSource={this.state.res} columns={columns} style={{fontSize: "12px"}}  scroll={styleScroll} pagination={{ defaultPageSize: 10, showSizeChanger: true, pageSizeOptions: ['10', '20', '30']}} size="small"/>
{this.state.loading?(<p><div className="example">
    <Spin />
  </div></p>):<p></p>}
    </div>

</Card>,
    },
    {
      title: 'Step 3 - Submit',
      content:<div>{this.state.collapsed?(<Alert
        message="Informational Notes"
        description="Notification sent"
        type="success"
        showIcon
      /> ):<Alert
      message="Informational Notes : This action will send an email alert to all subscribed users"
      description={<p style={{paddingTop: "17px"}}><Badge count={this.state.subcount} style={{ backgroundColor: '#52c41a' }}>
      <div> <Button>Count of subscribed user</Button></div>
      </Badge><Badge count={this.state.unsubcount} >
      <div style={{paddingLeft:this.state.hideNav==true?"0px":"23px"}}><Button> Count of unsubscribed user</Button></div>
      </Badge></p>}
      type="info"
      showIcon
      />  }
      {this.state.loading?<p><div className="example" style={{paddingTop:"2px"}}>
    <Spin />
  </div></p>:<p>  </p>}
      </div>,
    },
  ];
 
const { current } = this.state;
    return  <div>
      <div style={{
width: "578px",
marginTop: "-23px",
marginBottom: "9px",
marginLeft: this.state.hideNav==true?"-16px":"223px"

      }}>  <Alert
      message=""
      description="You are currently in the DEV environment"
      type="warning"
      showIcon
      closable
    /></div>
      <Card  className="gx-card" style={{    borderRadius: "11px"}}>
      
    <h2 className="title gx-mb-4" style={{    color: "#1890ff"}}><IntlMessages id={<span>{this.props.location.state.subtype} > {this.props.location.state.nav}</span>}/></h2>

    <div className="gx-d-flex justify-content-center">
   
    </div>
    <div class="ant-row">
    <div class="ant-col ant-col-24" >
    
    <div style={{marginLeft:"0px",paddingRight:"0px",marginTop: "-18px",marginBottom: "7px"}}>{this.state.subserr.Status=="InActive"||this.state.subserr.Status=="Trial"?<Alert
      message={this.state.subserr.Status=="InActive"?<span>Subscription Status: Inactive</span>:<span>Subscription Status: Trial</span>}
      description={this.state.subserr.Status=="InActive"?<span>Your subscription is currently inactive. You will not be able to send notification outside your company domain nor use SMS channel type.
    </span>:<span>Your subscription is currently in trial period. It will expire in {moment(this.state.subserr.ValidUntil).diff(moment(new Date),'days')} days. Please contact your company admin to activate</span>}
      type="info"
      showIcon
/>:<p></p>}</div>
    <div>
        <Steps current={current}>
          {steps.map(item => (
            <Step key={item.title} title={item.title} />
          ))}
        </Steps>
      
        <div className="steps-content" style={{ marginTop: "21px"}}>{steps[current].content}</div>
        <div className="steps-action" style={{ marginTop: "10px"}}>
        {current > 0 && (
            this.state.showprevious?<Button style={{ margin: 8,marginTop: "-7px"}} onClick={() => this.prev()}  disabled={this.state.bdisabled}>
              Previous
            </Button>:<p></p>
          )}
          {current < steps.length - 1 && (
            <Button type="primary" onClick={() => this.next()} disabled={this.state.initbuttondisabled}>
              Next
            </Button>
          )}
          {current === steps.length - 1 && (
            <Button type="primary" onClick={() => this.sendnoti(this)} disabled={this.state.bdisabled}>
              Submit
            </Button>
          )}
         
        {this.state.current==0 && !this.props.location.state.form?<div style={{    float: "right",
    
}}><Button type="primary">
      <CSVLink  headers={header} data={data}  filename={subtype+'.csv'} >  <DownloadOutlined /> Download CSV Template</CSVLink>
            </Button></div>:<p></p>}
        </div>
      </div>
      </div>
     
      
      
      </div></Card>
      <Modal bodyStyle={{ padding: '0' }}
       title=""
       visible={this.state.dvisible}
       onOk={this.handleOk}
       confirmLoading={this.state.confirmLoading}
       onCancel={this.handleCancel}
     style={{marginLeft: this.state.hideNav==true?"8%":"24%",marginTop:this.state.hideNav==true?"30%":"-20px", 
     height:this.state.hideNav==true?"466px": "419px"}}  width={this.state.hideNav==true?1000:900}>
      
       <div>
      
       {!this.state.form?  <Card className="gx-card" title="Email Preview" >
{this.state.hideNav?<div style={{marginLeft: "-15px"}}><Iframe src={this.state.html}
       style={{
        display: "initial",
        border: "0px",
        position: "absolute",
        top: "0",
        left: "0",
        width: "60%",
        height: "398%",
        marginLeft: "-18px"


       }}/></div>:
       <Iframe src={this.state.html}
        width="850px"
        height="450px"
        id="myId"
        frameBorder="0"
        className="myClassname"
        display="initial"
        position="relative"/>}
         </Card>:<Card className="gx-card" title="Edit End User">
         <div style={{marginLeft: "57px"}}>
        <Form autocomplete="off">
     
        <Row>
<Col span={12} className="colcustom">
<Form.Item className="fcust" style={{ marginLeft: this.state.hideNav?"-62px":'0px'}}>
    <Input type="text" placeholder={this.state.edata.Name} style={{ width: this.state.hideNav?"130px":"360px"}} id="Name"  value={this.state.Name} onChange={this.handleChange} prefix={<UserOutlined/> }required/>
</Form.Item>
</Col >
<Col span={12} className="colcustom" style={{ marginLeft: this.state.hideNav?"-31px":'0px'}}>
<Form.Item className="fcust" >
    <Input placeholder={this.state.edata.Email} style={{width: this.state.hideNav?"130px":"360px"}}  id="email" value={this.state.email} onChange={this.handleChange}  prefix={<FormOutlined />} required/>
</Form.Item>
</Col >
<Col span={12} className="colcustom">
<Form.Item className="fcust" style={{ marginLeft: this.state.hideNav?"-62px":'0px'}}>
    <Input placeholder="Phone Number NA" style={{ width: this.state.hideNav?"130px":"360px"}} id="phone" value="NA" onChange={this.handleChange}   prefix={<PhoneOutlined />}disabled/>
</Form.Item>
</Col >
<Col span={12} className="colcustom">
<Form.Item  style={{ marginLeft: this.state.hideNav?"-45px":'-11px'}}>
<Input placeholder={this.props.location.state.subtype.toUpperCase()} style={{ width: this.state.hideNav?"130px":"360px"}}  id="email" value={this.props.location.state.subtype.toUpperCase()}   disabled/>

</Form.Item></Col>
<Col span={12} className="colcustom">
<Form.Item className="fcust" label="Language" style={{ marginLeft: this.state.hideNav?"-60px":'0px'}}>
<Select
placeholder="English"
    onChange={this.lang}
    style={{ width: this.state.hideNav?130:282 }}>
  <option value="en_US">English</option>
  <option value="es-UY">Spanish</option>
</Select>
</Form.Item>
</Col >
<Col span={12} className="colcustom">
<Form.Item label="Channel Type *"   style={{ marginLeft: this.state.hideNav?"-45px":'-11px'}}>
<Select
placeholder={this.state.edata.ChannelType}
    onChange={this.channelType}
    style={{ width: this.state.hideNav?130:248 }}>
  <option value="EMAIL">EMAIL</option>
  
</Select>
</Form.Item></Col>


</Row>
</Form>



</div>
           
           
           
           
           
           
           
           </Card>}
         </div>
         </Modal>
      
      </div>
   
}
catch(ex)
{
  localStorage.setItem("dkey",'1');
  localStorage.setItem("skey",'1122');
  window.location.href = '/dashboard';
}

}

}