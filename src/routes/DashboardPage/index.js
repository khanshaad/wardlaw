import React, { Component } from "react";
import { Card, Table } from "antd";
import IntlMessages from "util/IntlMessages";
import axios from "axios";
import "./dashboard.css";
import apiCall from "../../apiutil/apicall";
import moment from "moment";
import { AreaChartOutlined } from "@ant-design/icons";
import {
  Bar,
  BarChart,
  CartesianGrid,
  Legend,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis
} from "recharts";

class CompanyPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      collapsed: false,
      comid: "",
      userdata: [],
      dailySentEmailsDetails: []
    };
  }
  async componentDidMount() {
    // var company=this.state.company;
    // var ainfo=[];
    var comid = localStorage.getItem("com");
    if (comid == "All") {
      comid = "";
    }
    this.setState({ comid });

    var companyId = comid ? comid : 'All';
    //var response = await axios.get(`http://localhost:3001/api/v1/analytics/sentemailscount/company/${companyId}/days/90`);
    var response = await apiCall.getApi(`analytics/sentemailscount/days/90`);
    response.data.forEach(element => {
      element.date = moment(new Date(element.date)).format("DD MMM YYYY")
      element['Emails Sent'] = element.email
    });
    this.setState({ dailySentEmailsDetails: response.data });

    var ainfo = [];
    var params = {
      companyid: comid,
      userid: localStorage.getItem("UserIdinfo"),
      rolename: "WARD-Super-Admin"
    };
    var res = await apiCall.postApi(`analytics/`, params);
    var y = res.data.Items.sort(
      (a, b) => new Date(b.UploadedOn) - new Date(a.UploadedOn)
    );
    console.log("y", y);
    y.forEach(function(obj) {
      console.log("datafrom Api", obj);
      ainfo.push(obj);
    });
    this.setState({ userdata: ainfo });
  }

  custom_sort(a, b) {
    return new Date(a.UploadedOn).getTime() - new Date(b.UploadedOn).getTime();
  }

  ///console.log(props);
  render() {
    let data = this.state.dailySentEmailsDetails;
    console.log('90 days data: '+ JSON.stringify(data, null, 2))
    let columns = [
      {
        // company: 'Company Name',
        // Adminname: 'company',
        key: "CompanyName",
        title: "Company Name",
        dataIndex: "CompanyName"
      },

      {
        // company: 'Company Name',
        // Adminname: 'company',
        key: "UploadedBy",
        title: "Uploaded By",
        dataIndex: "UploadedBy"
      },
      {
        // company: 'Company Name',
        // Adminname: 'company',
        key: "UploadedOn",
        title: "Uploaded On",
        render: (text, item) => (
          <span>
            {moment(new Date(item.UploadedOn)).format("DD MMM YYYY")}{" "}
          </span>
        )
      },
      {
        // company: 'Company Name',
        // Adminname: 'company',
        key: "SubscriptionType",
        title: "Subscription Type",
        dataIndex: "SubscriptionType"
      },
      {
        // company: 'Company Name',
        // Adminname: 'company',
        key: "TotalEndpoints",
        title: "Sent #",
        dataIndex: "TotalEndpoints"
      },
      {
        // company: 'Company Name',
        // Adminname: 'company',
        key: "EndPointDeliveries",
        title: "Delivered #",
        //dataIndex: "EndPointDeliveries"
        render: (text, item) => (
          <span>
            {Number(item.EndPointDeliveries)}
          </span>
        )
      },
      {
        // company: 'Company Name',
        // Adminname: 'company',
        key: "EmailDeliveryMetric",
        title: "Delivery Metric (%)",
        dataIndex: "EmailDeliveryMetric"
      },

      {
        // company: 'Company Name',
        // Adminname: 'company',
        key: "EmailBounceMetric",
        title: "Bounce Metric (%)",
        dataIndex: "EmailBounceMetric"
      }
    ];
    return (
      <div>
        <Card style={{ borderRadius: "11px" }}>
          <h2 className="title gx-mb-4" style={{ color: "#1890ff" }}>
            <IntlMessages id="Dashboard" />
          </h2>
          <div class="ant-row">
            <div class="ant-col ant-col-24">
              <Card
                title={
                  <span style={{ paddingLeft: "23px" }}>
                    <AreaChartOutlined /> Graph
                  </span>
                }
                style={{ fontSize: "12px" }}
              >
                <ResponsiveContainer width="100%" height={300}>
                  <BarChart
                    data={data}
                    margin={{ top: 10, right: 0, left: -15, bottom: 0 }}
                  >
                    <XAxis dataKey="date" />
                    <YAxis />
                    <CartesianGrid strokeDasharray="3 3" />
                    <Tooltip />
                    <Legend />
                    <Bar dataKey="Emails Sent" fill="#4EAFE8" />
                  </BarChart>
                </ResponsiveContainer>
              </Card>
            </div>
          </div>

          <div className="gx-d-flex justify-content-center"></div>
          <div class="ant-row">
            <div class="ant-col ant-col-24">
              <Card
                title={
                  <span style={{ paddingLeft: "23px" }}>
                    <AreaChartOutlined /> Outbound Email Analytics Data
                  </span>
                }
                style={{ fontSize: "12px" }}
              >
                <Table
                  dataSource={this.state.userdata}
                  columns={columns}
                  pagination={{
                    defaultPageSize: 10,
                    showSizeChanger: true,
                    pageSizeOptions: ["10", "20", "30"]
                  }}
                  size="small"
                />
              </Card>
            </div>
          </div>

         </Card>
      </div>
    );
  }
}

export default CompanyPage;
