import React,{Component} from "react";
import { Layout, Menu,Dropdown,Card,Tabs,Row,Col,Divider,Input,Badge,Modal,Table } from 'antd';
import IntlMessages from "util/IntlMessages";
import { AreaChartOutlined,UsrOutlined, LaptopOutlined, NotificationOutlined,CheckCircleOutlined,IssuesCloseOutlined } from '@ant-design/icons';
import ApiCall from '../../apiutil/apicall';
class Report extends Component {
    constructor(props) {
		super(props);
    this.state = {
      collapsed: false,
      comid:'',
      userdata:[]
    }
}
    async componentDidMount() {
       // var company=this.state.company;
       var ainfo=[];
        var comid=localStorage.getItem('com');
        
        if(comid!='All'&&comid)
        {
       var res=await ApiCall.getApi(`enduser/company/${comid}`);
     //  console.log("newapi",res);
       res.data.Items.forEach(element => {
           if(element.IsUnsubscribed=='TRUE')
           {
           ainfo.push(element);
           }
       });
       console.log('enew',ainfo);
      // ainfo=this.DistinctRecords(ainfo,"ChannelAddress");
       ainfo=this.DistinctRecords(ainfo,"Email");
        this.setState({userdata:ainfo})
        }
  }
  DistinctRecords(MYJSON,prop) {
    return MYJSON.filter((obj, pos, arr) => {
      return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
   })
  }
  render()
  {
    try{
    let columns = [
        {
            // company: 'Company Name',
            // Adminname: 'company',
            key: 'ChannelAddress',
            title: 'Channel Address',
            dataIndex: 'ChannelAddress'
            },{
                // company: 'Company Name',
                // Adminname: 'company',
                key: 'Name',
                title: 'Name',
                dataIndex: 'Name'
                },{
                    // company: 'Company Name',
                    // Adminname: 'company',
                    key: 'CompanyName',
                    title: 'Company Name',
                    dataIndex: 'CompanyName'
                    },
        
            {
                // company: 'Company Name',
                // Adminname: 'company',
                key: 'ChannelType',
                title: 'Channel Type',
                dataIndex: 'ChannelType'
                },
        {
        // company: 'Company Name',
        // Adminname: 'company',
        key: 'SubscriptionType',
        title: 'Subscription Type',
        dataIndex: 'SubscriptionType'
        },
        {
            // company: 'Company Name',
            // Adminname: 'company',
            key: 'IsUnsubscribed',
            title: 'Subscription Status',
            render: (text, record) => (
                <span>
                 
                {record.IsUnsubscribed=='FALSE'?<CheckCircleOutlined  style={{fontSize: "23px",color: "#52c41a"}} />:<IssuesCloseOutlined  style={{fontSize: "23px",color: "#f5222d"}} />}
                </span>
              ),
            }
        
          
              
                    
     
            ];

  return (
    <div>
      
      <Card style={{    borderRadius: "11px"}}>
      
      <h2 className="title gx-mb-4" style={{color: "#1890ff"}}><IntlMessages id='Unsubscribed'/></h2>

     
      <div class="ant-row">
      <div class="ant-col ant-col-24" >



      <Card title={<span style={{    paddingLeft: "23px"}}><AreaChartOutlined /> End Users Unsubscription Report</span>} style={{fontSize: "12px"}}>

      <Table className="gx-table-responsive" dataSource={this.state.userdata} columns={columns}  pagination={{ defaultPageSize: 10, showSizeChanger: true, pageSizeOptions: ['10', '20', '30']}} size="small"/>



</Card>
    </div>
    </div></Card>
    </div>
  );
    }
    catch(ex)
{
  window.location.href = '/dashboard';
}
};
}

export default Report;
