import React,{Component} from "react";
import {Route, Switch} from "react-router-dom";
import IdleTimer from 'react-idle-timer';
import asyncComponent from "util/asyncComponent";
import { notification,Modal  } from 'antd';
import { UsrOutlined, LaptopOutlined, CheckCircleOutlined,ExclamationCircleOutlined} from '@ant-design/icons';
const { confirm } = Modal;
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      timeout:1700000,//1000 * 180 * 1,
      showModal: false,
      userLoggedIn: false,
      isTimedOut: true,
      activity:false,
      hello2:true,
      visible: false,
    confirmLoading: false
  }
  
  this.idleTimer = null
  this.onAction = this._onAction.bind(this)
  this.onActive = this._onActive.bind(this)
  this.onIdle = this._onIdle.bind(this)
  }

  _onAction(e) {
    console.log('ddd',e);
    try{
    if(e.type=='mousedown' && e.target.className.trim()=='ant-modal-wrap')
    {
      this.setState({isTimedOut: false})
    }
    console.log('*** user is doing something', this.state.timeout)
    console.log('** user is doing something', e)

  }
  catch(e)
  {

  }
   // this.setState({visible:false});

  // this.setState({isTimedOut: false})
   // notification.destroy();
  }
 
  _onActive(e) {
    //notification.destroy();
    console.log('ddd',e);
   this.setState({isTimedOut: false})
    console.log('*** user is doing something', this.state.timeout)
    console.log('** user is active', e)
  //  this.setState({isTimedOut: false})
  }
 
  _onIdle(e) {
    console.log("asasas",e);
    this.setState({visible:true});
    console.log("1ms",this.state.isTimedOut);
if(this.state.isTimedOut==true)
{
    setTimeout(()=>{

     /* notification.open({
        message: 'Alert',
        description:
          `One minute remaining for logout`,
          duration: 0,
        icon: <CheckCircleOutlined  style={{ color: '#228B22' }} />,
      });*/
   //   this.setState({visible:true});
      setTimeout(()=>{
        this.setState({visible:false});

        if(this.state.isTimedOut==true)
        {
        localStorage.clear();

        window.location.href = "/signin";
        }
      },60000);
    },60000)
    console.log('*** user is  idle', this.state.timeout)
    console.log('** user is idle page will redirect to sign in')
 // localStorage.clear();
  //  window.location.href = "/signin";
  }
  }
  handleOk = () => {
    this.setState({
      ModalText: 'The modal will be closed after two seconds',
      confirmLoading: true,
    });

 //   alert("here");
    setTimeout(() => {
    /*  this.setState({
        visible: false,
        confirmLoading: false,
      });*/
      localStorage.clear();
      window.location.href = "/signin"; 
    }, 2000);
  };

  handleCancel = () => {
    console.log('Clicked cancel button');
    this.setState({
      visible: false,
    });
   this.setState({isTimedOut:false});
  };
  render()
  {
  
    const { match } = this.props
return(
  <>
  <IdleTimer
            ref={ref => { this.idleTimer = ref }}
            element={document}
            onActive={this.onActive}
            onIdle={this.onIdle}
            onAction={this.onAction}
            debounce={250}
            timeout={this.state.timeout} />
  <div className="gx-main-content-wrapper">
    <Switch>
      <Route path={`${match.url}dashboard`} component={asyncComponent(() => import('./DashboardPage'))}/>
    
      <Route path={`${match.url}user`} component={asyncComponent(() => import('./UserPage'))}/>
      <Route path={`${match.url}renewal`} component={asyncComponent(() => import('./Renewal'))}/>
      <Route path={`${match.url}report`} component={asyncComponent(() => import('./Reports'))}/>
      <Route path={`${match.url}create`}  component={asyncComponent(() => import('./create'))}/>
    <Route path={`${match.url}manage`}  component={asyncComponent(() => import('./manage'))}/>
    <Route path={`${match.url}CTA`}  component={asyncComponent(() => import('./CTAPage'))}/>
    <Route path={`${match.url}claims`}  component={asyncComponent(() => import('./Claims'))}/>
    <Route path={`${match.url}manageheader`}  component={asyncComponent(() => import('./SubscriptionHeaders'))}/>
    </Switch>
    <Modal
          title=""
          visible={this.state.visible}
          onOk={this.handleCancel}
          okText={"Continue Session"}
          cancelText={"Log Out"}
          onCancel={this.handleOk}
          id="modal12"
        >
          <p><ExclamationCircleOutlined style={{fontSize:"20px",color:'#fa8c16'}}/><span style={{paddingLeft: "7px",fontSize:"20px"}}>Your session is about to expire.</span><p style={{paddingLeft: "31px"}}>You will be logged out in 60 seconds.</p></p>
        </Modal>
  </div>
  </>
);

  }
}

export default App;
