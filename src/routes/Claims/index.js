import React, {Component} from "react";
import { Card,Table,Steps,Button } from 'antd';
import IntlMessages from "util/IntlMessages";
import axios from 'axios';

import ReactPlayer from 'react-player'
import moment from "moment";
import { AreaChartOutlined, InfoCircleFilled } from '@ant-design/icons';
class Claims  extends Component {
    constructor(props) {
		super(props);
    this.state = {
      collapsed: false,
      comid:'',
      userdata:[],
      step1:'Step-1',
      step2:'Step-2',
      step3:'Step-3',
      count:0,
      control:true,
      url1:'',
      url2:'',
      url3:''
    }
}
    async componentDidMount() {
      this.setState({url1:'https://youtu.be/YKhYXUvmbwQ'})
      const script1 = document.createElement("script");
      const script2 = document.createElement("script");
  
      script1.src = "https://fast.wistia.com/embed/medias/yw3lmvxqa6.jsonp";
      script1.async = true;
  
      script2.src = "https://fast.wistia.com/assets/external/E-v1.js";
      script2.async = true;
  
      document.body.appendChild(script1);
      document.body.appendChild(script2);
    
    }

    handleProgress =async state => {
        console.log(state)
        this.setState({step1:"In-progress"})
        if(state.played===1)
        {
            this.setState({step1:"Finished"})
            this.setState({count:1});
            this.setState({url1:''})
            this.setState({url2:'https://youtu.be/DKhSRPULnsI'})  
        }
    }
        handleProgress2 =async state => {
            console.log(state)
            this.setState({step2:"In-progress"})
            if(state.played===1)
            {
                this.setState({step2:"Finished"})
                this.setState({count:2});
                this.setState({url2:''})  
                this.setState({url3:'https://https://youtu.be/8SxCCLk9nqE'})   

            }
        }
            handleProgress3 =async state => {
                console.log(state)
                this.setState({step3:"In-progress"})
                if(state.played===1)
                {
                    this.setState({step3:"Finished"})
                    this.setState({url3:''})   
                }
            }

            play=x=>
            {
                if(x==1)
                {
                    this.setState({url1:'https://youtu.be/YKhYXUvmbwQ'})
                }
                else if(x==2)
                {
                    this.setState({url2:'https://youtu.be/DKhSRPULnsI'})  
                }
                else if(x==3)
                {
                    this.setState({url3:'https://https://youtu.be/8SxCCLk9nqE'})   

                }
            }
  render()
  {
      try{
    const { Step } = Steps;
  
  return (
    <div>
       <Card style={{    borderRadius: "11px"}}>
    <h2 className="title gx-mb-4" style={{color: "#1890ff"}}><IntlMessages id={this.props.location.state.action}/></h2>

    <div className="gx-d-flex justify-content-center">
  
    </div>
    <div class="ant-row">
    <div class="ant-col ant-col-24" >




<Card title="">
    <h4>When a policyholder experience a loss, helping them understand the next steps is a crucial component to the claims experience.

Sending a Smart Claims Video helps customers understand the process, set expectation, and improves customer satisfaction.</h4>
<br/>
<Steps direction="vertical" current={this.state.count}>
    <Step title="Step-1" description={
        <div>
            <div class="wistia_responsive_padding" style={{padding:"56.25% 0 0 0",position:"relative"}}>
            <div class="wistia_responsive_wrapper" style={{height:"100%",left:"0",position:"absolute",top:"0",width:"100%"}}>
            <div class="wistia_embed wistia_async_72kflzcvdr videoFoam=true" style={{height:"100%",position:"relative",width:"100%"}}>
            <div class="wistia_swatch" style={{height:"100%",left:"0",opacity:"0",overflow:"hidden",position:"absolute",top:"0",transition:"opacity 200ms",width:"100%"}}>
            <img src="https://fast.wistia.com/embed/medias/72kflzcvdr/swatch" style={{filter:"blur(5px)",height:"100%",objectFit:"contain",width:"100%"}} alt="" aria-hidden="true" onload="this.parentNode.style.opacity=1;" /></div></div></div></div>

        </div>

    } >

    
    </Step>
    <Step title="Step-2" description={
        
        <div>
        <div class="wistia_responsive_padding" style={{padding:"56.25% 0 0 0",position:"relative"}}>
        <div class="wistia_responsive_wrapper" style={{height:"100%",left:"0",position:"absolute",top:"0",width:"100%"}}>
        <div class="wistia_embed wistia_async_yw3lmvxqa6 videoFoam=true" style={{height:"100%",position:"relative",width:"100%"}}>
        <div class="wistia_swatch" style={{height:"100%",left:"0",opacity:"0",overflow:"hidden",position:"absolute",top:"0",transition:"opacity 200ms",width:"100%"}}>
        <img src="https://fast.wistia.com/embed/medias/yw3lmvxqa6/swatch" style={{filter:"blur(5px)",height:"100%",objectFit:"contain",width:"100%"}} alt="" aria-hidden="true" onload="this.parentNode.style.opacity=1;" /></div></div></div></div>

    </div>
        
        } />
   
    <Step title="Step-3" description={

<div>
<div class="wistia_responsive_padding" style={{padding:"56.25% 0 0 0",position:"relative"}}>
<div class="wistia_responsive_wrapper" style={{height:"100%",left:"0",position:"absolute",top:"0",width:"100%"}}>
<div class="wistia_embed wistia_async_azmaulc70d videoFoam=true" style={{height:"100%",position:"relative",width:"100%"}}>
<div class="wistia_swatch" style={{height:"100%",left:"0",opacity:"0",overflow:"hidden",position:"absolute",top:"0",transition:"opacity 200ms",width:"100%"}}>
<img src="https://fast.wistia.com/embed/medias/azmaulc70d/swatch" style={{filter:"blur(5px)",height:"100%",objectFit:"contain",width:"100%"}} alt="" aria-hidden="true" onload="this.parentNode.style.opacity=1;" /></div></div></div></div>

</div>

    } />
   
  </Steps>

  <Button type="primary">Click Here To Learn More</Button> 
</Card>
</div>


</div></Card>
</div>
  

  
   
  );
      }
      catch(ex)
{
  localStorage.setItem("dkey",'1');
  localStorage.setItem("skey",'1122');
  window.location.href = '/dashboard';
}
};
}


export default Claims;
